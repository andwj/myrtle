// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "io"
import "os"
import "fmt"
import "strings"
import "strconv"
import "unicode"
import "unicode/utf8"

// Node represents an element of a parsed line or file.
type Node struct {
	kind     NodeKind
	children []*Node
	str      string
	pos      Position

	val      *Value     // NX_Value, NX_Const, NX_Lambda
	tdef     *TypeDef   // ND_Map
	scope    *Scope     // NG_Block
	lvar     *LocalVar  // NX_Local, NS_Let
	flags    int
}

type NodeKind int

const (
	/* low level nodes */

	NL_ERROR NodeKind = iota    // str is the message
	NL_EOF

	NL_Name     // an identifier
	NL_DirSym   // a redirect symbol like `>>`
	NL_Char     // a char literal, encoded as integer: DDDDD

	/* grouping nodes */

	NG_Line     // a parsed line of tokens
	NG_Word     // a complex word
	NG_Double   // a double-quoted string

	NG_Expr     // a group of elements in `()` parentheses
	NG_Data     // a group of elements in `[]` square brackets
	NG_Block    // a group of elements in `{}` curly brackets

	/* word components */

	NW_Plain    // a sequence of normal (never magic) chars
	NW_Single   // a sequence of chars in single quotes
	NW_Tilde    // a sequence beginning with `~`
	NW_Param    // a numbered parameter (for $1 $2 etc...)
	NW_Var      // a variable name (restricted to letters, digits, `_`)
	NW_Glob     // a glob pattern sequence: `*` `?` `[...]`
)

type Position struct {
	line int  // 0 if unknown
	file int  // 0 if unknown, index into all_filenames (+ 1)
}

type Lexer struct {
	reader  io.Reader
	pos     Position

	line  []rune   // original line, only NIL at start
	r     []rune   // current parse position in line, NIL when done

	pending  *Node  // a pending error from SkipAhead
	hit_eof  bool   // we have reached the end-of-file
}

// NewLexer creates a Lexer from a Reader (e.g. a file).
func NewLexer(reader io.Reader, file int) *Lexer {
	lexer := new(Lexer)

	lexer.reader   = reader
	lexer.pos.file = file
	lexer.pending  = nil
	lexer.hit_eof  = false

	return lexer
}

// Scan reads and parses the current file and returns the next logical
// line (a NG_Line token).  It returns a NL_ERROR if there was a parsing
// error, or NL_EOF when the end of the file is reached.
func (lex *Lexer) Scan() *Node {
	if lex.pending != nil {
		res := lex.pending
		lex.pending = nil
		return res
	}

	if lex.hit_eof  {
		return NewNode(NL_EOF, "", lex.pos)
	}

	return lex.scanGroup(NG_Line, "", "", false)
}

// MoreLines returns true if the next call to Scan will return a line
// *without* fetching another raw line from the io.Reader.  Note that
// the future line could be empty.
func (lex *Lexer) MoreLines() bool {
	if lex.pending != nil {
		return false
	}
	if lex.line == nil || lex.r == nil {
		return false
	}
	return true
}

// SkipAhead may be called after an error is returned from Scan(), and
// will reposition the token stream at the next top-level directive.
// If any error occurs during this process, it will be the returned at
// the next call to Scan().
func (lex *Lexer) SkipAhead() {
	lex.pending = nil

	for {
		err := lex.fetchNonCommentLine()

		if err != nil {
			if err.kind == NL_ERROR {
				lex.pending = err
			}
			return
		}

		s := ""
		r := lex.r

		for len(r) > 0 {
			ch := r[0]

			if ! LEX_IdentifierChar(ch) {
				break
			}

			s = s + string(ch)
			r = r[1:]
		}

		if LEX_CommonDirective(s) {
			// ok, stop here
			return
		}
	}
}

//----------------------------------------------------------------------

func (lex *Lexer) scanGroup(kind NodeKind, opener, closer string, in_block bool) *Node {
	group := NewNode(kind, "", Position{})

	var err *Node

	for {
		// need a fresh line?
		if lex.line == nil || lex.r == nil {
			err = lex.fetchNonCommentLine()

			if err == nil {
				// ok
			} else if err.kind == NL_EOF {
				break
			} else {
				return err
			}
		} else {
			lex.skipWhitespace()
		}

		// ensure group gets correct position (esp. after first fetch)
		if group.pos.line == 0 {
			group.pos = lex.pos
		}

		// reached end of the line?
		is_end := false
		if len(lex.r) == 0 {
			is_end = true
		} else if lex.r[0] == ';' {
			is_end = true
		}

		if is_end {
			lex.r = nil

			if kind == NG_Line {
				return group
			}

			// group is not closed yet...
			continue
		}

		ch := lex.r[0]

		if in_block && ch == '}' {
			// leave lex.r alone, scanBlock depends on it
			return group
		}

		// a comma splits a physical line into two logical lines
		if ch == ',' {
			if kind != NG_Line || group.Len() == 0 {
				return NewNode(NL_ERROR, "stray , found", lex.pos)
			}

			lex.r = lex.r[1:]
			return group
		}

		if closer != "" && string(ch) == closer {
			lex.r = lex.r[1:]
			return group
		}

		// handle an opening parenthesis or bracket
		is_glob := (ch == '[' && LEX_DisambiguateGlob(lex.r))

		if (ch == '(' || ch == '[' || ch == '{') && !is_glob {
			var child *Node

			lex.r = lex.r[1:]

			switch ch {
			case '(': child = lex.scanGroup(NG_Expr, "(", ")", false)
			case '[': child = lex.scanGroup(NG_Data, "[", "]", false)
			case '{': child = lex.scanBlock()
			}

			// the child should never be EOF here
			if child.kind == NL_EOF {
				panic("unexpected EOF")
			}
			if child.kind == NL_ERROR {
				return child
			}

			group.Add(child)
			continue
		}

		// detect a stray closing parenthesis or bracket
		if ch == ')' || ch == ']' || ch == '}' {
			msg := "stray " + string(ch) + " found"
			return NewNode(NL_ERROR, msg, lex.pos)
		}

		// a `\` backslash extends a logical line over two physical lines
		if ch == '\\' {
			if LEX_AllSpace(lex.r[1:]) {
				if kind != NG_Line {
					return NewNode(NL_ERROR, "stray \\ found", lex.pos)
				}

				// grab another line
				lex.r = nil
				continue
			}

			if len(lex.r) >= 2 {
				if LEX_Whitespace(lex.r[1]) {
					if len(lex.r) > 0 {
						return NewNode(NL_ERROR, "can only use \\ at end of a line", lex.pos)
					}
				}
			}
		}

		var tok *Node

		// detect pipes and redirect symbols, also `!=` and `=`
		if LEX_RedirectChar(ch) {
			tok = lex.scanRedirect()
			if tok.kind == NL_ERROR {
				return tok
			}
			group.Add(tok)

			// a pipe symbol at end of line is an unfinished pipeline
			if tok.str[0] == '|' && kind == NG_Line && LEX_AllSpace(lex.r) {
				// grab another line
				lex.r = nil
			}
			continue
		}

		// a character literal?
		if ch == '#' && len(lex.r) >= 2 {
			var tok *Node

			switch lex.r[1] {
			case '\'':
				tok = lex.scanCharLiteral()
			case '`':
				tok = lex.scanCharEscape()
			}

			if tok != nil {
				if tok.kind == NL_ERROR {
					return tok
				}
				group.Add(tok)
				continue
			}
		}

		// everything else is a "word" here
		tok = lex.scanWord(kind)
		if tok.kind == NL_EOF {
			panic("unexpected EOF")
		}
		if tok.kind == NL_ERROR {
			return tok
		}

		group.Add(tok)
	}

	// reached end-of-file

	if opener != "" {
		// use the starting line for this error message
		msg := "unterminated " + opener + closer + " group"
		return NewNode(NL_ERROR, msg, group.pos)
	}

	if group.Len() > 0 {
		return group
	}

	lex.r = nil
	return err
}

func (lex *Lexer) scanBlock() *Node {
	group := NewNode(NG_Block, "", lex.pos)

	for {
		line := lex.scanGroup(NG_Line, "", "", true /* in_block */)

		if line.kind == NL_EOF {
			// use the starting line for this error message
			return NewNode(NL_ERROR, "unterminated {} block", group.pos)
		}
		if line.kind == NL_ERROR {
			return line
		}

		// was the line terminated by a `}` bracket?
		is_end := false

		if lex.r != nil {
			if len(lex.r) > 0 {
				if lex.r[0] == '}' {
					is_end = true
					lex.r = lex.r[1:]
				}
			}
		}

		// do not add empty lines to a block
		if line.Len() > 0 {
			group.Add(line)
		}

		if is_end {
			return group
		}
	}
}

func (lex *Lexer) skipWhitespace() {
	for len(lex.r) > 0 {
		ch := lex.r[0]

		if LEX_Whitespace(ch) {
			lex.r = lex.r[1:]
		} else {
			break
		}
	}
}

//----------------------------------------------------------------------

func (lex *Lexer) scanWord(context NodeKind) *Node {
	word := NewNode(NG_Word, "", lex.pos)

	for len(lex.r) > 0 {
		ch := lex.r[0]

		if LEX_Whitespace(ch) {
			break
		}

		if ch == ')' || ch == ']' || ch == '}' {
			break
		}

		// checking '=' here is critical for stuff like `VARNAME=value`
		if ch == ',' || ch == ';' || ch == '=' {
			break
		}

		var tok *Node

		switch ch {
		case '`':
			tok = lex.scanEscape()

		case '\'':
			tok = lex.scanSingleQuotes(false)

		case '"':
			tok = lex.scanDoubleQuotes()

		case '~':
			if context == NG_Line && word.Len() == 0 {
				tok = lex.scanTilde()
			}

		case '$':
			tok = lex.scanDollar()

		case '*', '?', '[':
			if context == NG_Line {
				tok = lex.scanGlobPattern()
			}

		case '(', '{':
			msg := "cannot use '" + string(ch) + "' in middle of a word"
			return NewNode(NL_ERROR, msg, lex.pos)
		}

		if tok == nil {
			word.AppendChar(ch)
			lex.r = lex.r[1:]
		} else if tok.kind == NW_Plain {
			word.AppendString(tok.str)
		} else if tok.kind == NL_ERROR {
			return tok
		} else {
			word.Add(tok)
		}
	}

	if word.Len() == 0 {
		panic("empty word")
	}

	// convert a plain word into an identifier, etc...
	if word.Len() == 1 {
		child := word.children[0]

		switch child.kind {
		case NW_Plain:
			if LEX_Identifier(child.str) {
				child.kind = NL_Name
				return child
			}

		case NW_Var:
			child.kind = NX_EnvVar
			return child

		case NW_Param:
			child.kind = NX_ArgParm
			return child
		}
	}

	return word
}

func (lex *Lexer) scanRedirect() *Node {
	pos := 1

	for pos < len(lex.r) {
		ch := lex.r[pos]

		if ! LEX_RedirectChar(ch) {
			break
		}

		// ok
		pos += 1
	}

	s   := string(lex.r[0:pos])
	tok := NewNode(NL_DirSym, s, lex.pos)

	lex.r = lex.r[pos:]
	return tok
}

func (lex *Lexer) scanSingleQuotes(allow_escape bool) *Node {
	// skip the leading quote
	lex.r = lex.r[1:]

	quote := NewNode(NW_Single, "", lex.pos)
	start := lex.pos

	for {
		// an unterminated string continues on the next line
		if len(lex.r) == 0 {
			err := lex.fetchRawLine()

			if err == nil {
				// ok
			} else if err.kind == NL_EOF {
				return NewNode(NL_ERROR, "unterminated string in ''", start)
			} else {
				return err
			}
			quote.str += LEX_Newline()
			continue
		}

		ch   := lex.r[0]

		// found the trailing quote?
		if ch == '\'' {
			lex.r = lex.r[1:]
			return quote
		}

		if allow_escape && ch == '`' {
			tok := lex.scanEscape()
			if tok.kind == NL_ERROR {
				return tok
			}
			quote.str = quote.str + tok.str
			continue
		}

		quote.str = quote.str + string(ch)
		lex.r = lex.r[1:]
	}
}

func (lex *Lexer) scanDoubleQuotes() *Node {
	// skip the leading quote
	lex.r = lex.r[1:]

	quote := NewNode(NG_Double, "", lex.pos)
	start := lex.pos

	for {
		// an unterminated string continues on the next line
		if len(lex.r) == 0 {
			err := lex.fetchRawLine()

			if err == nil {
				// ok
			} else if err.kind == NL_EOF {
				return NewNode(NL_ERROR, "unterminated string in \"\"", start)
			} else {
				return err
			}
			quote.AppendString(LEX_Newline())
			continue
		}

		ch := lex.r[0]

		// found the trailing quote?
		if ch == '"' {
			lex.r = lex.r[1:]
			return quote
		}

		var tok *Node

		switch ch {
		case '`':
			tok = lex.scanEscape()
		case '$':
			tok = lex.scanDollar()
		case '*', '?', '[':
			tok = lex.scanGlobPattern()
		}

		if tok == nil {
			quote.AppendChar(ch)
			lex.r = lex.r[1:]
		} else if tok.kind == NW_Plain {
			quote.AppendString(tok.str)
		} else if tok.kind == NL_ERROR {
			return tok
		} else {
			quote.Add(tok)
		}
	}
}

func (lex *Lexer) scanTilde() *Node {
	// skip leading tilde
	pos := 1

	for pos < len(lex.r) {
		ch := lex.r[pos]

		is_term := LEX_Whitespace(ch)

		switch ch {
		case '/', '\\', ',', ';':
			is_term = true

		case '(', ')', '[', ']', '{', '}', '\'', '"':
			is_term = true
		}

		if is_term {
			break
		}

		pos += 1
	}

	s   := string(lex.r[0:pos])
	tok := NewNode(NW_Tilde, s, lex.pos)

	lex.r = lex.r[pos:]
	return tok
}

func (lex *Lexer) scanDollar() *Node {
	// skip the dollar sign
	lex.r = lex.r[1:]

	if len(lex.r) == 0 {
		return NewNode(NW_Plain, "$", lex.pos)
	}

	ch := lex.r[0]

	// look for a natural break after it
	if ! LEX_Printable(ch) {
		return NewNode(NW_Plain, "$", lex.pos)
	}
	switch ch {
	case ',', ';', ')', ']', '}':
		return NewNode(NW_Plain, "$", lex.pos)
	}

	// a variable?
	if ch == '_' || unicode.IsLetter(ch) {
		return lex.scanDollarVar()
	}

	// a parameter number?
	if '0' <= ch && ch <= '9' {
		return lex.scanDollarParam()
	}

	// an expression in parentheses?
	if ch == '(' {
		lex.r = lex.r[1:]
		return lex.scanGroup(NG_Expr, "(", ")", false)
	}

	msg := "unknown dollar form: $" + string(ch)
	return NewNode(NL_ERROR, msg, lex.pos)
}

func (lex *Lexer) scanDollarParam() *Node {
	pos := 1

	for pos < len(lex.r) {
		ch := lex.r[pos]

		if ! ('0' <= ch && ch <= '9') {
			break
		}
		pos += 1
	}

	param := NewNode(NW_Param, string(lex.r[0:pos]), lex.pos)
	lex.r = lex.r[pos:]
	return param
}

func (lex *Lexer) scanDollarVar() *Node {
	pos := 1

	for pos < len(lex.r) {
		ch := lex.r[pos]

		if unicode.IsDigit(ch) || unicode.IsLetter(ch) {
			// ok
		} else if ch == '_' || ch == '-' {
			// ok
		} else {
			break
		}
		pos += 1
	}

	dvar := NewNode(NW_Var, string(lex.r[0:pos]), lex.pos)
	lex.r = lex.r[pos:]
	return dvar
}

func (lex *Lexer) scanGlobPattern() *Node {
	if lex.r[0] != '[' {
		s   := string(lex.r[0])
		tok := NewNode(NW_Glob, s, lex.pos)

		lex.r = lex.r[1:]
		return tok
	}

	pos := 1

	for {
		if pos >= len(lex.r) {
			return NewNode(NL_ERROR, "unclosed glob pattern in []", lex.pos)
		}

		ch := lex.r[pos]

		if ch == ']' {
			if pos == 1 {
				pos += 1
				continue
			}

			// found the closer of the [] glob pattern
			pos += 1

			s   := string(lex.r[0:pos])
			tok := NewNode(NW_Glob, s, lex.pos)

			lex.r = lex.r[pos:]
			return tok
		}

		// check for negation of the character class
		if ch == '!' && pos == 1 {
			pos += 1
			if pos < len(lex.r) && lex.r[pos] == ']' {
				pos += 1
			}
			continue
		}

		// check for a range of characters
		if pos+2 < len(lex.r) {
			if lex.r[pos+1] == '-' && lex.r[pos+2] != ']' {
				pos += 3
				continue
			}
		}

		// a single character
		pos += 1
	}
}

func (lex *Lexer) scanCharLiteral() *Node {
	lex.r = lex.r[2:]  // skip the `#` and quote

	if len(lex.r) == 0 {
		return NewNode(NL_ERROR, "bad char literal", lex.pos)
	}
	if len(lex.r) < 2 {
		return NewNode(NL_ERROR, "unterminated char literal", lex.pos)
	}
	if lex.r[1] != '\'' {
		return NewNode(NL_ERROR, "unterminated char literal", lex.pos)
	}

	ch   := lex.r[0]
	lex.r = lex.r[2:]

	if LEX_Whitespace(ch) {
		// any whitespace becomes a normal space
		ch = ' '
	}

	int_str := strconv.Itoa(int(ch))
	return NewNode(NL_Char, int_str, lex.pos)
}

func (lex *Lexer) scanCharEscape() *Node {
	lex.r = lex.r[1:]  // skip the `#`

	tok := lex.scanEscape()
	if tok.kind == NL_ERROR {
		return tok
	}

	if !utf8.ValidString(tok.str) {
		return NewNode(NL_ERROR, "invalid unicode char literal", lex.pos)
	}

	r := []rune(tok.str)

	int_str := strconv.Itoa(int(r[0]))
	return NewNode(NL_Char, int_str, lex.pos)
}

func (lex *Lexer) scanEscape() *Node {
	// skip the backquote
	lex.r = lex.r[1:]

	if len(lex.r) == 0 {
		return NewNode(NL_ERROR, "missing char after ` escape", lex.pos)
	}

	ch := lex.r[0]

	if LEX_Whitespace(ch) {
		// any whitespace becomes a single space
		lex.r = lex.r[1:]
		return NewNode(NW_Plain, " ", lex.pos)
	}

	// any non-alphanumeric character is escaped as itself
	if ! (unicode.IsLetter(ch) || unicode.IsDigit(ch)) {
		tok  := NewNode(NW_Plain, string(ch), lex.pos)
		lex.r = lex.r[1:]
		return tok
	}

	// octal escape?
	if LEX_OctalDigit(ch) {
		return lex.scanOctalEscape()
	}

	ch = unicode.ToLower(ch)
	lex.r = lex.r[1:]

	switch ch {
	case 'n':
		return NewNode(NW_Plain, LEX_Newline(), lex.pos)
	case 't':
		return NewNode(NW_Plain, "\t", lex.pos)
	case 'x':
		return lex.scanHexEscape()
	case 'u':
		return lex.scanUnicodeEscape()
	}

	msg := "unknown escape `" + string(ch)
	return NewNode(NL_ERROR, msg, lex.pos)
}

func (lex *Lexer) scanOctalEscape() *Node {
	value := int(lex.r[0] - '0')
	lex.r  = lex.r[1:]

	if len(lex.r) > 0 && LEX_OctalDigit(lex.r[0]) {
		value = value * 8 + int(lex.r[0] - '0')
		lex.r = lex.r[1:]
	}
	if len(lex.r) > 0 && LEX_OctalDigit(lex.r[0]) {
		value = value * 8 + int(lex.r[0] - '0')
		lex.r = lex.r[1:]
	}

	if value > 0xFF {
		return NewNode(NL_ERROR, "illegal octal escape", lex.pos)
	}

	// convert to a string.
	// NOTE: this may be produce invalid UTF-8 !
	var b [1]byte
	b[0] = byte(value)

	return NewNode(NW_Plain, string(b[:]), lex.pos)
}

func (lex *Lexer) scanHexEscape() *Node {
	s := ""

	if len(lex.r) > 0 && LEX_HexDigit(lex.r[0]) {
		s = s + string(lex.r[0])
		lex.r = lex.r[1:]
	}
	if len(lex.r) > 0 && LEX_HexDigit(lex.r[0]) {
		s = s + string(lex.r[0])
		lex.r = lex.r[1:]
	}

	if s == "" {
		return NewNode(NL_ERROR, "missing digits in hex escape", lex.pos)
	}

	value, err := strconv.ParseUint(s, 16, 64)
	if err != nil {
		return NewNode(NL_ERROR, "illegal hex escape", lex.pos)
	}

	// convert to a string.
	// NOTE: this may be produce invalid UTF-8 !
	var b [1]byte
	b[0] = byte(value)

	return NewNode(NW_Plain, string(b[:]), lex.pos)
}

func (lex *Lexer) scanUnicodeEscape() *Node {
	if len(lex.r) == 0 {
		return NewNode(NL_ERROR, "missing { in unicode escape", lex.pos)
	}
	if lex.r[0] != '{' {
		return NewNode(NL_ERROR, "missing { in unicode escape", lex.pos)
	}

	lex.r = lex.r[1:]

	s := ""

	for {
		if len(lex.r) == 0 {
			return NewNode(NL_ERROR, "missing } in unicode escape", lex.pos)
		}

		ch := lex.r[0]

		if ch == '}' {
			lex.r = lex.r[1:]
			break
		}

		if LEX_HexDigit(ch) {
			// ok
		} else if unicode.IsLetter(ch) {
			return NewNode(NL_ERROR, "bad hex digit in unicode escape", lex.pos)
		} else {
			return NewNode(NL_ERROR, "missing } in unicode escape", lex.pos)
		}

		s = s + string(ch)
		lex.r = lex.r[1:]
	}

	if s == "" {
		return NewNode(NL_ERROR, "missing digits in unicode escape", lex.pos)
	}

	value, err := strconv.ParseUint(s, 16, 64)
	if err != nil || value > 0x10FFFF {
		return NewNode(NL_ERROR, "illegal unicode code point", lex.pos)
	}

	return NewNode(NW_Plain, string(rune(value)), lex.pos)
}

//----------------------------------------------------------------------

func (lex *Lexer) fetchNonCommentLine() *Node {
	for {
		err := lex.fetchNormalLine()

		if err != nil {
			return err
		}

		// ignore `#!` to allow using as a Unix script interpreter
		if len(lex.r) >= 2 {
			if lex.r[0] == '#' && lex.r[1] == '!' {
				continue
			}
		}

		// ignore line comments
		if len(lex.r) >= 1 {
			if lex.r[0] == ';' {
				continue
			}
		}

		return nil  // ok
	}
}

func (lex *Lexer) fetchNormalLine() *Node {
	// this handles multi-line comments, like: `#[[` and `#]]`

	cur_depth  := 0
	start_line := 0

	for {
		err := lex.fetchRawLine()

		if err == nil {
			// ok
		} else {
			if err.kind == NL_EOF && cur_depth > 0 {
				pos := lex.pos
				pos.line = start_line
				return NewNode(NL_ERROR, "unterminated comments", pos)
			}
			return err
		}

		lex.skipWhitespace()

		opener := LEX_MultilineComment(lex.r, '[')
		closer := LEX_MultilineComment(lex.r, ']')

		if opener > 0 {
			if cur_depth == 0 {
				cur_depth  = opener
				start_line = lex.pos.line
			} else if opener < cur_depth {
				// ignore a lower depth
			} else {
				return NewNode(NL_ERROR, "bad nesting of multiline comments", lex.pos)
			}
			continue
		}

		if closer > 0 {
			if closer == cur_depth {
				cur_depth = 0
			} else if closer < cur_depth {
				// ignore a lower depth
			} else {
				return NewNode(NL_ERROR, "stray #] or #]] found", lex.pos)
			}
			continue
		}

		if cur_depth == 0 {
			// a normal line, outside of any comments
			return nil
		}
	}
}

func (lex *Lexer) fetchRawLine() *Node {
	// once stream is finished, keep returning EOF
	// [ this simplifies some logic in the calling code ]
	if lex.hit_eof  {
		return NewNode(NL_EOF, "", lex.pos)
	}

	lex.pos.line += 1

	// we assume the Reader is buffered
	var buf [1]byte

	s := ""

	for {
		n, err := lex.reader.Read(buf[:])

		if n > 0 {
			if buf[0] == '\r' {
				// ignore CR
			} else if buf[0] == '\n' {
				// found the LF
				break
			} else {
				s = s + string(buf[:])
			}
		}

		if err == io.EOF {
			lex.hit_eof = true
			if s == "" {
				return NewNode(NL_EOF, "", lex.pos)
			} else {
				break
			}
		} else if err != nil {
			lex.hit_eof = true
			return NewNode(NL_ERROR, err.Error(), lex.pos)
		}
	}

	// convert to runes
	lex.line = []rune(s)
	lex.r    = lex.line

	// check for UTF-8 problems
	for _, ch := range lex.line {
		if ch == utf8.RuneError {
			return NewNode(NL_ERROR, "bad UTF-8 found", lex.pos)
		}
	}

	return nil  // ok
}

//----------------------------------------------------------------------

func LEX_Whitespace(ch rune) bool {
	return unicode.Is(unicode.White_Space, ch) ||
		   unicode.IsControl(ch)
}

func LEX_AllSpace(r []rune) bool {
	for _, ch := range r {
		if ! LEX_Whitespace(ch) {
			return false
		}
	}
	return true
}

func LEX_Identifier(s string) bool {
	if s == "" {
		return false
	}

	// disallow something which looks like a filename
	if s == "/" {
		return true
	}
	if s[0] == '/' || s[0] == '\\' {
		return false
	}

	for _, ch := range s {
		if ! LEX_IdentifierChar(ch) {
			return false
		}
	}
	return true
}

func LEX_IdentifierChar(ch rune) bool {
	const ID_CHARS = "@_.-+*/%<=>!?#$&|'~\\"

	return false ||
		unicode.IsLetter(ch) ||
		unicode.IsDigit(ch) ||
		strings.ContainsRune(ID_CHARS, ch)
}

func LEX_RedirectChar(ch rune) bool {
	switch ch {
	case '<', '>', '|', '&', '!', '=':
		return true
	}
	return false
}

func LEX_EnvironmentVar(s string) bool {
	if s == "" {
		return false
	}

	r  := []rune(s)
	ch := r[0]

	if LEX_AsciiLetter(ch) || ch == '_' {
		// ok
	} else {
		return false
	}

	for _, ch := range r[1:] {
		if LEX_AsciiLetter(ch) || ch == '_' || ch == '-' {
			// ok
		} else if '0' <= ch && ch <= '9' {
			// ok
		} else {
			return false
		}
	}
	return true
}

func LEX_Printable(ch rune) bool {
	return (
		unicode.IsLetter(ch) ||
		unicode.IsNumber(ch) ||
		unicode.IsSymbol(ch) ||
		unicode.IsPunct(ch) )
}

func LEX_AsciiLetter(ch rune) bool {
	if 'A' <= ch && ch <= 'Z' {
		return true
	}
	if 'a' <= ch && ch <= 'z' {
		return true
	}
	return false
}

func LEX_OctalDigit(ch rune) bool {
	return '0' <= ch && ch <= '7'
}

func LEX_HexDigit(ch rune) bool {
	ch = unicode.ToLower(ch)
	if 'a' <= ch && ch <= 'f' {
		return true
	}
	if '0' <= ch && ch <= '9' {
		return true
	}
	return false
}

func LEX_Number(s string) bool {
	if s == "" {
		return false
	}

	r := []rune(s)

	if unicode.IsDigit(r[0]) {
		return true
	}
	if len(r) < 2 {
		return false
	}
	if r[0] == '-' || r[0] == '+' {
		if unicode.IsDigit(r[1]) {
			return true
		}
	}
	return false
}

func LEX_MultilineComment(r []rune, bracket rune) int {
	if len(r) < 2 {
		return 0
	}
	if r[0] != '#' {
		return 0
	}
	count := 0
	r = r[1:]
	for len(r) > 0 {
		if r[0] != bracket {
			break
		}
		count += 1
		r = r[1:]
	}
	return count
}

func LEX_Newline() string {
	if os.PathSeparator == '\\' {
		return "\r\n"  // Windows uses CR/LF
	} else {
		return "\n"    // Unix uses just LF
	}
}

func LEX_CommonDirective(s string) bool {
	switch s {
	case "type", "macro", "const", "var", "fun", "do", "command":
		return true
	}
	return false
}

func LEX_DisambiguateGlob(r []rune) bool {
	// look for the closing square bracket.
	// this logic DOES fail for some valid glob patterns...

	for i := 1; i < len(r); i++ {
		if r[i] == ']' {
			// nothing after it --> data / access
			if i+1 >= len(r) {
				return false
			}

			// whitespace after it --> data access
			ch := r[i+1]

			if LEX_Whitespace(ch) {
				return false
			}

			// closing paren/bracket after it --> data access
			if ch == ')' || ch == ']' || ch == '}' {
				return false
			}

			// something else after it --> glob
			return true
		}
	}

	// no closing ']' on line --> data access
	return false
}

func LEX_ModuleName(s string) string {
	pos := strings.IndexRune(s, '/')
	if pos <= 0 || pos+1 >= len(s) {
		return ""
	}
	return s[0:pos]
}

func LEX_BareName(s string) string {
	m := LEX_ModuleName(s)
	if m == "" {
		return s
	}
	return s[len(m)+1:]
}

//----------------------------------------------------------------------

func NewNode(kind NodeKind, str string, pos Position) *Node {
	t := new(Node)
	t.kind = kind
	t.str  = str
	t.pos  = pos
	t.children = make([]*Node, 0)
	return t
}

func (t *Node) Len() int {
	return len(t.children)
}

func (t *Node) Last() *Node {
	if len(t.children) == 0 {
		return nil
	}
	return t.children[len(t.children)-1]
}

func (t *Node) Add(child *Node) {
	if child == nil {
		panic("Add with nil node")
	}
	t.children = append(t.children, child)
}

func (t *Node) AddFront(child *Node) {
	// resize the array
	t.children = append(t.children, nil)

	// shift elements up
	copy(t.children[1:], t.children[0:])

	t.children[0] = child
}

func (t *Node) PopHead() *Node {
	if t.Len() == 0 {
		panic("PopHead with no tokens")
	}
	head := t.children[0]
	t.children = t.children[1:]
	return head
}

func (t *Node) PopTail() *Node {
	if t.Len() == 0 {
		panic("PopTail with no tokens")
	}
	num  := len(t.children)
	tail := t.children[num-1]
	t.children = t.children[0:num-1]
	return tail
}

func (t *Node) Remove(idx int) {
	if idx >= t.Len() {
		panic("Remove with bad index")
	}
	for k := idx+1; k < t.Len(); k++ {
		t.children[k-1] = t.children[k]
	}
	t.children = t.children[0:t.Len()-1]
}

func (t *Node) AppendChar(ch rune) {
	if t.Len() >= 1 {
		last := t.children[t.Len()-1]

		if last.kind == NW_Plain {
			last.str = last.str + string(ch)
			return
		}
	}

	t.Add(NewNode(NW_Plain, string(ch), t.pos))
}

func (t *Node) AppendString(s string) {
	if len(s) == 0 {
		return
	}

	if t.Len() >= 1 {
		last := t.children[t.Len()-1]

		if last.kind == NW_Plain {
			last.str = last.str + s
			return
		}
	}

	t.Add(NewNode(NW_Plain, s, t.pos))
}

func (t *Node) Match(name string) bool {
	if t.kind == NL_Name || t.kind == NL_DirSym {
		if t.Len() == 0 {
			return t.str == name
		}
	}
	return false
}

func (t *Node) Find(name string) int {
	for i, elem := range t.children {
		if elem.Match(name) {
			return i
		}
	}
	return -1
}

func (t *Node) IsField() bool {
	if t.Match("...") || t.Match("..") {
		return false
	}
	return t.kind == NL_Name && len(t.str) >= 2 && t.str[0] == '.'
}

func (t *Node) IsMethod() bool {
	if t.kind == NL_Name {
		r := []rune(t.str)

		if r[0] == '.' {
			return false
		}

		// require last char (only) to be a hash
		if len(r) >= 2 && r[len(r)-1] == '#' {
			for i := len(r)-2; i >= 0; i-- {
				if r[i] == '#' { return false }
			}
			return true
		}
	}
	return false
}

func (t *Node) IsValue() bool {
	return t.val != nil
}

func (t *Node) HasGlob() bool {
	if t.kind == NW_Glob {
		return true
	}
	for _, child := range t.children {
		if child.HasGlob() {
			return true
		}
	}
	return false
}

// Unglob removes any glob patterns from a complex word, including any
// existing in double-quoted strings.  The node will become a NL_Name
// when possible (nothing but plain chars and globs).  Returns true if
// the given node was modified, false otherwise.
func (t *Node) Unglob() bool {
	changed := false

	for _, child := range(t.children) {
		if child.kind == NG_Double {
			child.Unglob()
		}
		if child.kind == NW_Glob {
			child.kind = NW_Plain
			changed = true
		}
	}

	if !changed {
		return false
	}

	// merge any consecutive pairs of NW_Plain nodes
	old_children := t.children
	t.children = make([]*Node, 0)

	for _, child := range old_children {
		if child.kind == NW_Plain {
			t.AppendString(child.str)
		} else {
			t.Add(child)
		}
	}

	// convert to NL_Name when possible
	if t.kind == NG_Word && t.Len() == 1 {
		child := t.children[0]

		if child.kind == NW_Plain {
			t.kind = NL_Name
			t.str  = child.str
			t.children = t.children[0:0]
		}
	}

	return true
}

func (t *Node) IsDynamic() bool {
	// includes NC_GlobMatch and any node kind which may produce an
	// arbitrary value (which could be an array or map).

	switch t.kind {
	case NX_Value, NX_Const, NX_EnvVar, NX_ArgParm, NX_UserDir:
		return false
	case ND_StrBuild, NC_Ok, NC_Failed, NC_ArgNum:
		return false
	default:
		return true
	}
}

// String returns something usable in error messages,
// especially ones of the form "expected xxx, got: yyy".
func (t *Node) String() string {
	switch t.kind {
	case NL_Name, NL_DirSym:
		return t.str

	case NL_EOF:     return "EOF"
	case NL_Char:    return "a char literal"

	case NG_Line:    return "a raw line"
	case NG_Word:    return "a raw word"
	case NG_Double:  return "a double-quoted string"
	case NG_Expr:    return "stuff in ()"
	case NG_Data:    return "stuff in []"
	case NG_Block:   return "stuff in {}"

	case NX_Value:   return "a literal value"
	case NX_Const:   return "a global constant"
	case NX_Global:  return "a global var"
	case NX_Local:   return "a local var"
	case NX_EnvVar:  return "an env var"
	case NX_ArgParm: return "an arg ref"

	default:         return "something odd"
	}
}

func (t *Node) FullString() string {
	if t == nil {
		return "nil"
	}

	s := t.str
	if len(s) > 50 {
		s = s[0:50] + "..."
	}
	s = fmt.Sprintf("%q", s)

	len_str := "(" + strconv.Itoa(t.Len()) + " elem)"

	switch t.kind {
	/* low level nodes */

	case NL_ERROR:   return "ERROR " + s
	case NL_EOF:     return "EOF"

	case NL_Name:    return "NL_Name " + s
	case NL_DirSym:  return "NL_DirSym " + s
	case NL_Char:    return "NL_Char " + s

	/* grouping nodes */

	case NG_Line:    return "NG_Line " + len_str
	case NG_Word:    return "NG_Word " + len_str
	case NG_Double:  return "NG_Double " + len_str

	case NG_Expr:    return "NG_Expr " + len_str
	case NG_Data:    return "NG_Data " + len_str
	case NG_Block:   return "NG_Block " + len_str

	/* word components */

	case NW_Plain:   return "NW_Plain " + s
	case NW_Single:  return "NW_Single " + s
	case NW_Tilde:   return "NW_Tilde " + s
	case NW_Param:   return "NW_Param " + s
	case NW_Var:     return "NW_Var " + s
	case NW_Glob:    return "NW_Glob " + s

	/* expressions */

	case NX_Value:    return "NX_Value"
	case NX_Const:    return "NX_Const " + s
	case NX_Global:   return "NX_Global " + s
	case NX_Local:    return "NX_Local " + s

	case NX_EnvVar:   return "NX_EnvVar " + s
	case NX_ArgParm:  return "NX_ArgParm " + s
	case NX_UserDir:  return "NX_UserDir " + s

	case NX_Unary:    return "NX_Unary " + s
	case NX_Binary:   return "NX_Binary " + s
	case NX_Call:     return "NX_Call " + len_str
	case NX_Method:   return "NX_Method " + len_str
	case NX_Access:   return "NX_Access " + len_str

	case NX_Lambda:   return "NX_Lambda"
	case NX_Matches:  return "NX_Matches"
	case NX_Supports: return "NX_Supports"
	case NX_Min:      return "NX_Min"
	case NX_Max:      return "NX_Max"

	/* statements */

	case NS_Return:   return "NS_Return"
	case NS_Skip:     return "NS_Skip "  + s
	case NS_Label:    return "NS_Label " + s
	case NS_Continue: return "NS_Continue "
	case NS_Break:    return "NS_Break "

	case NS_Let:      return "NS_Let "
	case NS_If:       return "NS_If"
	case NS_Loop:     return "NS_Loop"
	case NS_For:      return "NS_For"
	case NS_ForEach:  return "NS_ForEach"
	case NS_Assign:   return "NS_Assign"

	/* data constructors */

	case ND_Array:     return "ND_Array " + len_str
	case ND_Set:       return "ND_Set " + len_str
	case ND_Map:       return "ND_Map " + len_str
	case ND_MapElem:   return "ND_MapElem"
	case ND_StrBuild:  return "ND_StrBuild " + len_str

	/* commands */

	case NC_Command:   return "NC_Command " + len_str
	case NC_Backgnd:   return "NC_Backgnd"
	case NC_Pipe:      return "NC_Pipe " + s

	case NC_GlobMatch: return "NC_GlobMatch" + len_str
	case NC_ArgGroup:  return "NC_ArgGroup" + len_str
	case NC_Redir:     return "NC_Redir " + s

	case NC_Ok:        return "NC_Ok"
	case NC_Failed:    return "NC_Failed"
	case NC_ArgNum:    return "NC_ArgNum"

	default:
		return "??UNKNOWN??"
	}
}

func Dump(msg string, t *Node, level int) {
	if msg != "" {
		println(msg)
	}

	s := fmt.Sprintf("%*s%s", level, "", t.FullString())
	println(s)

	if t.children != nil {
		for _, child := range t.children {
			Dump("", child, level + 3)
		}
	}
}
