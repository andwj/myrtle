// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "io/fs"
import "path/filepath"
import "strings"
import "unicode"

// GlobPattern represents a pattern to match.  Matching can be done
// case-sensitively or case-insensitively.  There is always at least
// one section, and there is no requirement to contain actual globs
// (i.e. the whole pattern can be plain text).
type GlobPattern struct {
	sections    []*GlobSection
	ignore_case bool
}

type GlobKind int
const (
	GLOB_INVALID GlobKind = iota

	GLOB_Text    // a sequence of normal characters
	GLOB_One     // for `?`  : matches any single character
	GLOB_Many    // for `*`  : matches any sequence of characters
	GLOB_Set     // for `[]` : match a single char in the set
)

type GlobSection struct {
	kind    GlobKind
	text    []rune
	negate  bool
	ranges  []GlobRange
}

type GlobRange struct {
	low   rune
	high  rune
}

//----------------------------------------------------------------------

func NewGlobPattern(ignore_case bool) *GlobPattern {
	pat := new(GlobPattern)
	pat.sections = make([]*GlobSection, 0)
	pat.ignore_case = ignore_case
	return pat
}

func (pat *GlobPattern) rawAdd(sec *GlobSection) {
	if len(pat.sections) > 0 {
		last := pat.sections[len(pat.sections)-1]

		// an optimization: merge two `*` or more in a row
		if sec.kind == GLOB_Many && last.kind == GLOB_Many {
			return
		}
	}

	pat.sections = append(pat.sections, sec)
}

func (sec *GlobSection) AddRange(low, high rune) {
	sec.ranges = append(sec.ranges, GlobRange{ low, high })
}

func (pat *GlobPattern) AddText(s string) {
	if s == "" {
		return
	}

	sec := new(GlobSection)
	sec.kind = GLOB_Text
	sec.text = []rune(s)

	pat.rawAdd(sec)
}

func (pat *GlobPattern) AddGlob(s string) {
	if s == "" {
		return
	}

	sec := new(GlobSection)

	if s[0] == '?' {
		sec.kind = GLOB_One
	} else if s[0] == '*' {
		sec.kind = GLOB_Many
	} else if s[0] == '[' {
		sec.decodeSet(s)
	} else {
		panic("bad glob part: " + s)
	}

	pat.rawAdd(sec)
}

func (sec *GlobSection) decodeSet(s string) {
	sec.kind = GLOB_Set

	sec.text   = make([]rune, 0, len(s))
	sec.ranges = make([]GlobRange, 0)

	r   := []rune(s)
	pos := 1

	for pos < len(r) {
		ch := r[pos]

		if ch == ']' {
			if pos == 1 {
				sec.text = append(sec.text, ch)
				pos += 1
				continue
			}

			// found the closer of the [] glob pattern
			return
		}

		// check for negation of the character class
		if ch == '!' && pos == 1 {
			sec.negate = true
			pos += 1
			if pos < len(r) && r[pos] == ']' {
				sec.text = append(sec.text, ']')
				pos += 1
			}
			continue
		}

		// check for a range of characters
		if pos+2 < len(r) {
			if r[pos+1] == '-' && r[pos+2] != ']' {
				sec.AddRange(r[pos], r[pos+2])
				pos += 3
				continue
			}
		}

		// a single character
		sec.text = append(sec.text, ch)
		pos += 1
	}
}

//----------------------------------------------------------------------

func (pat *GlobPattern) Match(r []rune) bool {
	return pat.rawMatch(pat.sections, r)
}

func (pat *GlobPattern) rawMatch(s []*GlobSection, r []rune) bool {
	for {
		if len(r) == 0 {
			if len(s) == 0 {
				return true
			}
			if s[0].kind == GLOB_Many {
				return true
			}
			// more sections remain, but cannot be matched
			return false
		}

		if len(s) == 0 {
			// more runes remain, but there is nothing to match them
			return false
		}

		sec := s[0]
		s    = s[1:]

		if sec.kind == GLOB_One {
			r = r[1:]

		} else if sec.kind == GLOB_Set {
			if pat.matchSet(r[0], sec) == sec.negate {
				return false
			}
			r = r[1:]

		} else if sec.kind == GLOB_Text {
			if len(r) < len(sec.text) {
				return false
			}
			for k, ch := range sec.text {
				if ! pat.matchRune(r[k], ch) {
					return false
				}
			}
			r = r[len(sec.text):]

		} else {  // GLOB_Many
			if len(s) == 0 {
				// nothing after `*` means we match to the end
				return true
			}
			// recursively check whether the remaining sections match some
			// future sequence of the runes.  while this method is easy to
			// understand, worst-cast performance is really poor.
			for idx := 0; idx < len(r); idx++ {
				if pat.rawMatch(s, r[idx:]) {
					return true
				}
			}
			return false
		}
	}
}

func (pat *GlobPattern) matchRune(ch, ch2 rune) bool {
	if ch == ch2 {
		return true
	}
	if pat.ignore_case {
		if	unicode.ToUpper(ch) == unicode.ToUpper(ch2) ||
			unicode.ToLower(ch) == unicode.ToLower(ch2) ||
			unicode.ToTitle(ch) == unicode.ToTitle(ch2) {

			return true
		}
	}
	return false
}

func (pat *GlobPattern) matchSet(ch rune, sec *GlobSection) bool {
	for _, ch2 := range sec.text {
		if pat.matchRune(ch, ch2) {
			return true
		}
	}
	for _, R := range sec.ranges {
		if R.low <= ch && ch <= R.high {
			return true
		}
		if pat.ignore_case {
			ch2 := unicode.ToLower(ch)
			if R.low <= ch2 && ch2 <= R.high {
				return true
			}

			ch2 = unicode.ToUpper(ch)
			if R.low <= ch2 && ch2 <= R.high {
				return true
			}

			ch2 = unicode.ToTitle(ch)
			if R.low <= ch2 && ch2 <= R.high {
				return true
			}
		}
	}
	return false
}

func (pat *GlobPattern) IsPlain() bool {
	if len(pat.sections) != 1 {
		return false
	}
	sec := pat.sections[0]
	return sec.kind == GLOB_Text
}

func (pat *GlobPattern) GetPlain() string {
	sec := pat.sections[0]
	return string(sec.text)
}

func (pat *GlobPattern) Eq(s string) bool {
	if pat.IsPlain() {
		sec := pat.sections[0]
		return s == string(sec.text)
	}
	return false
}

func (pat *GlobPattern) Dump() {
	println("  Pattern:", len(pat.sections), "sections")

	for _, sec := range pat.sections {
		text := ""
		if sec.text != nil {
			text = string(sec.text)
		}

		switch sec.kind {
		case GLOB_Text: println("    '", text, "'")
		case GLOB_One:  println("    ?")
		case GLOB_Many: println("    *")
		case GLOB_Set:  println("    [", text, "]")

		default: println("    ????")
		}
	}
}

//----------------------------------------------------------------------

// GlobFilename represents a filename separated into components.
// The components are directory entries, which in a filename string
// are normally separated by `/` or `\` characters, depending on the
// operating system.  The patterns may be plain text items, or globs.
//
// The `root` field will be "." for relative filenames, otherwise the
// root of an absolute filename.  For Unix-like operating systems, it
// will be "/", and for Windows it is "\" or a UNC like "\\host\share\".
//
// While Windows supports relative filenames with a drive letter, such
// as "C:foo.txt" (at least I think it does, MSDOS worked that way),
// to keep things simple this code interprets the presence of a drive
// letter to mean an absolute filename.
type GlobFilename struct {
	components   []*GlobPattern
	root         string
	ignore_case  bool
	start_new    bool
	matches      []string
}

func NewGlobFilename(ignore_case bool) *GlobFilename {
	gf := new(GlobFilename)
	gf.components  = make([]*GlobPattern, 0)
	gf.ignore_case = ignore_case

	// in initial state, root is "" which means we are waiting
	// for some text to decide it.
	gf.root = ""

	return gf
}

func (gf *GlobFilename) Valid() bool {
	return len(gf.components) > 0
}

func (gf *GlobFilename) AddText(s string) {
	if s == "" {
		return
	}

	// handle drive letters and UNC paths in Windows
	if gf.root == "" {
		vol := filepath.VolumeName(s)
		if len(vol) > 0 {
			gf.root = vol + string(os.PathSeparator)
			gf.start_new = true
			s = s[len(vol):]
		}
	}

	// NOTE: on Windows we treat `/` and `\` as equivalent,
	//       but not on Unix-like operating systems.
	s = filepath.ToSlash(s)

	if gf.root == "" {
		if s[0] == '/' {
			gf.root = string(os.PathSeparator)
			gf.start_new = true
			s = s[1:]
		} else {
			gf.root = "."
			gf.start_new = true
		}
	}

	// split into components...
	for {
		idx := strings.IndexRune(s, '/')
		if idx < 0 {
			break
		}

		dir := s[0:idx]
		s    = s[idx+1:]

		// ignore multiple slashes in a row
		if dir != "" {
			pat := gf.curPattern()
			pat.AddText(dir)
		}

		// we have a slash, so begin a new component
		gf.start_new = true
	}

	if s != "" {
		pat := gf.curPattern()
		pat.AddText(s)
	}
}

func (gf *GlobFilename) AddGlob(s string) {
	if s == "" {
		return
	}

	if gf.root == "" {
		gf.root = "."
		gf.start_new = true
	}

	pat := gf.curPattern()
	pat.AddGlob(s)
}

func (gf *GlobFilename) curPattern() *GlobPattern {
	if gf.start_new {
		pat := NewGlobPattern(gf.ignore_case)

		gf.components = append(gf.components, pat)
		gf.start_new  = false

		return pat
	}

	return gf.components[len(gf.components)-1]
}

func (gf *GlobFilename) Dump() {
	println("GlobFilename:")
	println("  Root:", gf.root)

	for _, comp := range gf.components {
		comp.Dump()
	}
}

// Matches finds all files matching the glob pattern.
// The returned filenames are relative if the GlobFilename is,
// otherwise they will be absolute.  Errors while walking the
// file system are ignored.
func (gf *GlobFilename) Matches() []string {
	gf.matches = make([]string, 0)

	if gf.Valid() {
		gf.matchRest(gf.root, gf.components)
	}

	return gf.matches
}

func (gf *GlobFilename) matchRest(curpath string, components []*GlobPattern) {
	// when we reach the end of the components, just store the path
	if len(components) == 0 {
		gf.matches = append(gf.matches, curpath)
		return
	}

	pat := components[0]
	components = components[1:]

	// handle plain components (without a glob)...

	if pat.Eq(".") {
		// single dot = current dir, nothing needed
		gf.matchRest(curpath, components)
		return
	}

	if pat.Eq("..") || pat.IsPlain() {
		// the Join does a Clean which handles `..` for us
		curpath = filepath.Join(curpath, pat.GetPlain())

		if gf.dirExists(curpath) {
			gf.matchRest(curpath, components)
		}
		return
	}

	// handle globs...

	// only_dirs is an optimization, letting us skip non-directories
	only_dirs := false
	if len(components) > 0 {
		only_dirs = true
	}

	// read the filenames in directory for the current path
	filenames := gf.matchDir(curpath, pat, only_dirs)

	for _, name := range filenames {
		newpath := filepath.Join(curpath, name)

		// recurse to handle the remaining parts (even if none)
		gf.matchRest(newpath, components)
	}
}

func (gf *GlobFilename) matchDir(curpath string, pat *GlobPattern, only_dirs bool) []string {
	list := make([]string, 0)

	entries, _ := os.ReadDir(curpath)

	if entries == nil {
		return list
	}

	for _, ent := range entries {
		name := ent.Name()

		// always skip the special "." and ".." names
		if name == "" || name == "." || name == ".." {
			continue
		}

		if only_dirs {
			mode    := ent.Type()
			is_dir  := ((mode & fs.ModeDir) != 0)
			is_link := ((mode & fs.ModeSymlink) != 0)

			// treat symbolic links as though they were directories.
			//
			// checking they are really directories is a pain, since we
			// need to follow the link (which may lead to another link...)
			// and it is not really necessary since ReadDir() will simply
			// fail if the target of the link is not a directory.

			if is_dir || is_link {
				// ok
			} else {
				continue
			}
		}

		// if the name begins with a `.` dot, then we do not allow
		// a glob to match that dot, only plain text can match it.
		// this follows the conventions of existing Unix shells.

		if name[0] == '.' {
			if pat.sections[0].kind != GLOB_Text {
				continue
			}
		}

		r := []rune(name)

		if pat.Match(r) {
			list = append(list, name)
		}
	}

	return list
}

func (gf *GlobFilename) dirExists(pathname string) bool {
	stat, err := os.Lstat(pathname)
	if err != nil {
		return false
	}

	mode    := stat.Mode()
	is_dir  := ((mode & fs.ModeDir) != 0)
	is_link := ((mode & fs.ModeSymlink) != 0)

	return is_dir || is_link
}
