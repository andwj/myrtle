#
# This makefile only exists to supply the "osusergo" tag to the
# go build command.  This prevents the os/user package from using
# CGO and pulling in a dependency on libc.
#

PROGRAM=myrtle

all:
	go build -tags osusergo

clean:
	rm -f $(PROGRAM) $(PROGRAM).exe ERRS

.PHONY: all clean
