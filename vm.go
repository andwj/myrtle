// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "math"
import "sort"
import "unicode/utf8"

type Operation struct {
	kind OpCode  // Kind
	line int32   // Line number
	S    int32   // Source
	D    int32   // Destination
}

const NO_PART int32 = -0x7FFFFFFF

type OpCode int32

const (
	OP_INVALID OpCode = iota

	/* Register and Stack */

	OP_A_TO_B       //  B := A
	OP_A_TO_C       //  C := A
	OP_B_TO_A       //  A := B
	OP_C_TO_A       //  A := C

	OP_LOAD_A       //  A := S (stack or constant)
	OP_LOAD_B       //  B := S (stack or constant)
	OP_LOAD_C       //  C := S (stack or constant)

	OP_STORE_A      //  D (stack) := A
	OP_STORE_B      //  D (stack) := B
	OP_STORE_C      //  D (stack) := C

	OP_COPY         //  D := S (stack or constant)

	/* Control Flow */

	OP_NOP          //  does nothing
	OP_ERROR        //  produce a fatal error, A = message string

	OP_JUMP         //  PC := D
	OP_IF_FALSE     //  if A == NIL/FALSE then PC := D
	OP_IF_TRUE      //  if A != NIL/FALSE then PC := D

	OP_FUN_CALL     //  call function,  S = stack frame, D = params
	OP_TAIL_CALL    //  tail-call func, S = stack frame, D = params
	OP_RETURN       //  return from current function

	OP_FOR_BEGIN    //  check start/end in S[0..1] are valid, if D > 0 compute step
	OP_FOR_PAST     //  A := TRUE if gone past the end, otherwise FALSE
	OP_FOR_STEP     //  perform step: add S[2] onto S[0]

	OP_EACH_BEGIN   //  S[0..3] = initialize for object in A
	OP_EACH_STEP    //  S[0..3] = get next key/val pair, if no more then PC := D

	/* Variables */

	OP_NEW_EXVAR    //  create a fresh external var at stack D slot
	OP_NEW_UPVAR    //  add var in S slot to new closure in A
	OP_COPY_UPVAR   //  copy upvars[S] to new closure in A
	OP_HOIST_VAL    //  add S as upvar to new closure in A

	OP_GLOB_READ    //  A := globals[S]
	OP_GLOB_WRITE   //  globals[D] := A
	OP_ENV_READ     //  A := env-vars[S]
	OP_ENV_WRITE    //  env-vars[D] := A

	OP_EXVAR_READ   //  A := stack S slot for external var
	OP_EXVAR_WRITE  //  external var in stack D slot := A
	OP_UPV_READ     //  A := upvars[S]
	OP_UPV_WRITE    //  upvars[D] := A

	/* Container Access */

	OP_ELEM_READ    //  A := B[A]
	OP_ELEM_WRITE   //  B[C] := A
	OP_APPEND       //  append A onto array B

	OP_GET_METHOD   //  A := lookup method for object in A,  S = name string
	OP_HAS_METHOD   //  A := TRUE if object in B has method, S = name string
	OP_SET_CLASS    //  object in A (a map) becomes typedef[S], or none if -1

	/* Values */

	OP_MAKE_NIL     //  A := NIL
	OP_MAKE_TRUE    //  A := TRUE
	OP_MAKE_FALSE   //  A := FALSE
	OP_MAKE_CHAR    //  A := S, as a unicode point
	OP_MAKE_NUMBER  //  A := S, converted from int32 -> float64
	OP_MAKE_FUNC    //  A := new closure from template S
	OP_MAKE_ARRAY   //  A := a freshly allocated array
	OP_MAKE_MAP     //  A := a freshly allocated map

	/* Unary operators */

	OP_NOT          //  A := not A
	OP_NEG          //  A :=  0 - A
	OP_FLIP         //  A := -1 ~ A
	OP_TO_BOOL      //  A := A converted to boolean
	OP_TO_STR       //  A := A converted to a string

	/* Binary math operators */

	OP_ADD          //  A := B + A
	OP_SUB          //  A := B - A
	OP_MUL          //  A := B * A
	OP_DIV          //  A := B / A
	OP_REM          //  A := B % A  (truncating remainder)
	OP_POW          //  A := B ** A (power)

	OP_AND          //  A := B & A  (bitwise and)
	OP_OR           //  A := B | A  (bitwise or)
	OP_XOR          //  A := B ~ A  (bitwise xor)
	OP_SHIFT_L      //  A := B << A (left shift)
	OP_SHIFT_R      //  A := B >> A (right shift)

	OP_MIN          //  A := minimum of A and B
	OP_MAX          //  A := maximum of A and B

	/* Comparison operators */

	OP_EQ           //  A := (B == A)
	OP_NE           //  A := (B != A)
	OP_LT           //  A := (B <  A)
	OP_LE           //  A := (B <= A)
	OP_GT           //  A := (B >  A)
	OP_GE           //  A := (B >= A)

	/* Commands */

	OP_CALC_OK      //  A := (Status == 0)
	OP_ARG_NUM      //  A := (len ARGS - 1)
	OP_ARG_READ     //  A := [ARGS S], "" when out of bounds
	OP_USER_DIR     //  A := string for a directory, S = username or ""

	OP_EXEC_BEGIN   //  S := create a new command builder
	OP_EXEC_ENV     //  add env-var (name in D, value in A) to command in S
	OP_EXEC_ARG     //  append A (usually a string) to command arguments in S
	OP_EXEC_COUNT   //  A := number of arguments so far in S
	OP_EXEC_REDIR   //  redirect fd in A to open file in D, for command S
	OP_EXEC_RUN     //  run command via builder in S

	OP_PAT_BEGIN    //  S := begin constructing a glob pattern, D = ignore case
	OP_PAT_TEXT     //  add plain text in A (a string) to pattern in S
	OP_PAT_GLOB     //  add glob part in D, like "*", to the pattern in S
	OP_PAT_RUN      //  append filenames matching pattern S to array in A

	OP_FILE_OPEN    //  S := open file, A = filename, D = mode (0=R 1=W 2=A)
	OP_FILE_TEMP    //  S := create temporary file for writing
	OP_FILE_REOPEN  //  close writing file in S, reopen for reading
	OP_FILE_CLOSE   //  close file in S.  if temporary, delete it
)

//----------------------------------------------------------------------

// NOTE: we never clear the data stack, e.g. after a function
// returns, hence the Go garbage collector may keep stuff
// alive longer than necessary (potentially indefinitely).

const MAX_DATA_STACK = 250000
const MAX_CALL_STACK = 25000

var data_stack [MAX_DATA_STACK]Value
var call_stack [MAX_CALL_STACK]CallFrame

var call_p      int
var call_frame  *CallFrame
var data_frame  int  // call_frame.base_p

var register_A Value
var register_B Value
var register_C Value

// this indicates that a run-time error has occurred
var run_error bool

// maximum stack frames to show on an error
var trace_depth int = 12

type CallFrame struct {
	// the function being run
	cl *Closure

	// base of stack frame (SP) for this function
	base_p int

	// the code pointer (PC) for this function.
	// while executing an instruction, this points at the next one.
	op_ptr int

	// the saved status value
	status Value
}

//----------------------------------------------------------------------

func lar_RunCode(cl *Closure) cmError {
	// reset call stack
	call_p = 0

	// reset error condition
	run_error = false

	err := RunClosure(cl, 0)

	return err
}

func lar_SetTraceDepth(depth int) {
	trace_depth = depth
}

const LAR_REG_A = 0

func lar_ValueString(what int) string {
	return register_A.DeepString()
}

func ChildDataFrame() int {
	return data_frame + int(call_frame.cl.high_water) + 1
}

// RunClosure executes the compiled code in the closure.
// Parameters must be setup earlier (in the data stack).
// Returns true if ok, false if an error was raised.
func RunClosure(cl *Closure, base_p int) cmError {
	start_p := call_p

	if !PushCallStack(cl, base_p) {
		return FAILED
	}

	if cl.builtin != nil {
		cl.builtin.code()
		PopCallStack()

		if run_error {
			return FAILED
		}
		return OK
	}

	for call_p > start_p {
		op := &call_frame.cl.vm_ops[call_frame.op_ptr]
		call_frame.op_ptr += 1

		if op.kind == OP_RETURN {
			PopCallStack()
		} else {
			PerformOperation(op)
		}

		if run_error {
			return FAILED
		}
	}

	return OK
}

func PushCallStack(cl *Closure, base_p int) bool {
	// check for stack overflow
	if call_p+2 >= len(call_stack) {
		Die("overflow of call stack")
		return false
	}

	if base_p+int(cl.highest_water)+2 >= len(data_stack) {
		Die("overflow of data stack")
		return false
	}

	call_p++

	call_frame = &call_stack[call_p]
	data_frame = base_p

	call_frame.cl     = cl
	call_frame.base_p = data_frame
	call_frame.op_ptr = 0

	// save old Status value, reset current one
	if cl.builtin == nil && cl.is_function {
		call_frame.status = *Status.loc
	}

	if cl.builtin == nil {
		if cl.is_function || cl.is_command {
			Status.loc.MakeNumber(0)
		}
	}

	return true
}

func PopCallStack() {
	// restore the Status value
	cl := call_frame.cl
	if cl.builtin == nil && cl.is_function {
		*Status.loc = call_frame.status
	}

	call_frame.cl = nil

	call_p--

	if call_p > 0 {
		call_frame = &call_stack[call_p]
		data_frame = call_frame.base_p
	} else {
		call_frame = nil
	}
}

func ShowStackTrace() {
	fmt.Printf("\n")
	fmt.Printf("Stack trace:\n")

	last_p := call_p - trace_depth
	if last_p < 0 {
		last_p = 0
	}

	for i := call_p; i > last_p; i-- {
		frame := &call_stack[i]
		line  := frame.DetermineLine()

		if line <= 0 {
			fmt.Printf("   [%03d] %s\n", i, frame.cl.debug_name)
		} else {
			fmt.Printf("   [%03d] %s (line %d)\n", i, frame.cl.debug_name, line )
		}
	}

	fmt.Printf("\n")
}

func (frame *CallFrame) DetermineLine() int {
	if frame.cl.builtin != nil {
		return 0
	}

	op_ptr := frame.op_ptr - 1

	if op_ptr < 0 || op_ptr >= len(frame.cl.vm_ops) {
		return 0
	}

	return int(frame.cl.vm_ops[op_ptr].line)
}

//----------------------------------------------------------------------

func PerformOperation(op *Operation) {
	S := int(op.S)
	D := int(op.D)

	switch op.kind {
	/* Register and Stack */

	case OP_A_TO_B:
		register_B = register_A

	case OP_A_TO_C:
		register_C = register_A

	case OP_B_TO_A:
		register_A = register_B

	case OP_C_TO_A:
		register_A = register_C

	case OP_LOAD_A:
		register_A = GetStackOrConstant(S)

	case OP_LOAD_B:
		register_B = GetStackOrConstant(S)

	case OP_LOAD_C:
		register_C = GetStackOrConstant(S)

	case OP_STORE_A:
		data_stack[data_frame+D] = register_A

	case OP_STORE_B:
		data_stack[data_frame+D] = register_B

	case OP_STORE_C:
		data_stack[data_frame+D] = register_C

	case OP_COPY:
		data_stack[data_frame+D] = GetStackOrConstant(S)

	/* Control Flow */

	case OP_NOP:
		// nothing to do

	case OP_ERROR:
		Die("%s", register_A.Str)

	case OP_JUMP:
		perform_Jump(D)

	case OP_IF_FALSE:
		if !register_A.IsTrue() {
			perform_Jump(D)
		}

	case OP_IF_TRUE:
		if register_A.IsTrue() {
			perform_Jump(D)
		}

	case OP_FUN_CALL:
		perform_FunCall(S, D)

	case OP_TAIL_CALL:
		perform_TailCall(S, D)

	case OP_RETURN:
		// NOTE: handled by caller

	case OP_FOR_BEGIN:
		perform_ForBegin(S, D)

	case OP_FOR_PAST:
		perform_ForPast(S)

	case OP_FOR_STEP:
		perform_ForStep(S)

	case OP_EACH_BEGIN:
		perform_EachBegin(S)

	case OP_EACH_STEP:
		perform_EachStep(S, D)

	/* Variables */

	case OP_NEW_EXVAR:
		var ref Value
		ref.MakeArray(1)
		ref.Array.data[0].MakeNIL()
		data_stack[data_frame+D] = ref

	case OP_NEW_UPVAR:
		new_cl := register_A.Clos
		upvar := new(Upvar)
		upvar.ptr = data_stack[data_frame+S].Array
		new_cl.upvars = append(new_cl.upvars, upvar)

	case OP_COPY_UPVAR:
		new_cl := register_A.Clos
		new_cl.upvars = append(new_cl.upvars, call_frame.cl.upvars[S])

	case OP_HOIST_VAL:
		var mem Value
		mem.MakeArray(1)

		upvar := new(Upvar)
		upvar.ptr = mem.Array
		upvar.ptr.data[0] = data_stack[data_frame+S]

		new_cl := register_A.Clos
		new_cl.upvars = append(new_cl.upvars, upvar)

	case OP_GLOB_READ:
		if S < 0 || S >= len(globals) {
			panic("BAD GLOBAL-DEF INDEX")
		}
		register_A = *globals[S].loc

	case OP_GLOB_WRITE:
		if D < 0 || D >= len(globals) {
			panic("BAD GLOBAL-DEF INDEX")
		}
		*globals[D].loc = register_A

	case OP_ENV_READ:
		name := GetStackOrConstant(S).Str
		register_A.MakeString(Exec_ReadVar(name))

	case OP_ENV_WRITE:
		name := GetStackOrConstant(D).Str
		if register_A.kind != TYP_String {
			Die("require string for OP_ENV_WRITE, got %s", register_A.kind.String())
			return
		}
		Exec_WriteVar(name, register_A.Str)

	case OP_EXVAR_READ:
		ref := data_stack[data_frame+S]
		register_A = ref.Array.data[0]

	case OP_EXVAR_WRITE:
		ref := data_stack[data_frame+D]
		ref.Array.data[0] = register_A

	case OP_UPV_READ:
		cl := call_frame.cl
		if S < 0 || S >= len(cl.upvars) {
			panic("BAD UPVALUE INDEX for READ")
		}
		register_A = cl.upvars[S].ptr.data[0]

	case OP_UPV_WRITE:
		cl := call_frame.cl
		if D < 0 || D >= len(cl.upvars) {
			panic("BAD UPVALUE INDEX for WRITE")
		}
		cl.upvars[D].ptr.data[0] = register_A

	/* Container Access */

	case OP_ELEM_READ:
		perform_ElemRead()

	case OP_ELEM_WRITE:
		perform_ElemWrite()

	case OP_APPEND:
		if register_B.kind != TYP_Array {
			Die("cannot append onto %s", register_A.kind.String())
			return
		}
		register_B.AppendElem(register_A)

	case OP_GET_METHOD:
		perform_GetMethod(S)

	case OP_HAS_METHOD:
		perform_HasMethod(S)

	case OP_SET_CLASS:
		perform_SetClass(S)

	/* Values */

	case OP_MAKE_NIL:
		register_A.MakeNIL()

	case OP_MAKE_TRUE:
		register_A.MakeBool(true)

	case OP_MAKE_FALSE:
		register_A.MakeBool(false)

	case OP_MAKE_CHAR:
		register_A.MakeChar(rune(S))

	case OP_MAKE_NUMBER:
		register_A.MakeNumber(float64(S))

	case OP_MAKE_FUNC:
		perform_MakeFunc(S)

	case OP_MAKE_ARRAY:
		register_A.MakeArray(0)

	case OP_MAKE_MAP:
		register_A.MakeMap()

	/* Unary operators */

	case OP_NOT:
		perform_Not()

	case OP_NEG:
		perform_Neg()

	case OP_FLIP:
		perform_Flip()

	case OP_TO_BOOL:
		register_A.MakeBool(register_A.IsTrue())

	case OP_TO_STR:
		perform_ToString()

	/* Binary math operators */

	case OP_ADD:
		perform_Add()

	case OP_SUB:
		perform_Sub()

	case OP_MUL:
		perform_Mul()

	case OP_DIV:
		perform_Div()

	case OP_REM:
		perform_Rem()

	case OP_POW:
		perform_Power()

	case OP_AND:
		perform_And()

	case OP_OR:
		perform_Or()

	case OP_XOR:
		perform_Xor()

	case OP_SHIFT_L:
		perform_ShiftLeft()

	case OP_SHIFT_R:
		perform_ShiftRight()

	case OP_MIN:
		if VM_comparison() < 0 {
			register_A = register_B
		}

	case OP_MAX:
		if VM_comparison() > 0 {
			register_A = register_B
		}

	/* Comparison operators */

	case OP_EQ:
		b := register_B.BasicEqual(&register_A)
		register_A.MakeBool(b)

	case OP_NE:
		b := register_B.BasicEqual(&register_A)
		register_A.MakeBool(! b)

	case OP_LT:
		cmp := VM_comparison()
		register_A.MakeBool(cmp < 0)

	case OP_LE:
		cmp := VM_comparison()
		register_A.MakeBool(cmp <= 0)

	case OP_GT:
		cmp := VM_comparison()
		register_A.MakeBool(cmp > 0)

	case OP_GE:
		cmp := VM_comparison()
		register_A.MakeBool(cmp >= 0)

	/* Command stuff */

	case OP_CALC_OK:
		register_A.MakeBool(Status.loc.IsOk())

	case OP_ARG_NUM:
		count := Glob.ARGS.loc.Length()
		if count > 0 { count -= 1 }
		register_A.MakeNumber(float64(count))

	case OP_ARG_READ:
		perform_ArgRead(S)

	case OP_USER_DIR:
		name := GetStackOrConstant(S).Str
		dir  := GetUserDirectory(name)
		register_A.MakeString(dir)

	case OP_EXEC_BEGIN:
		data_stack[data_frame+S].MakeNIL()
		data_stack[data_frame+S].Cmd = NewCommandBuilder()

	case OP_EXEC_ENV:
		cmd := data_stack[data_frame+S].Cmd
		name := GetStackOrConstant(D).Str
		if register_A.kind != TYP_String {
			Die("require string for OP_EXEC_ENV, got %s", register_A.kind.String())
			return
		}
		cmd.EnvVar(name, register_A.Str)

	case OP_EXEC_ARG:
		cmd := data_stack[data_frame+S].Cmd
		perform_ExecArg(cmd)

	case OP_EXEC_COUNT:
		cmd := data_stack[data_frame+S].Cmd
		num := len(cmd.args)
		register_A.MakeNumber(float64(num))

	case OP_EXEC_REDIR:
		cmd  := data_stack[data_frame+S].Cmd
		file := data_stack[data_frame+D].File
		perform_ExecRedir(cmd, file)

	case OP_EXEC_RUN:
		cmd := data_stack[data_frame+S].Cmd
		perform_ExecRun(cmd)

	case OP_PAT_BEGIN:
		ignore_case := (D > 0)
		data_stack[data_frame+S].MakeNIL()
		data_stack[data_frame+S].Glob = NewGlobFilename(ignore_case)

	case OP_PAT_TEXT:
		if register_A.kind != TYP_String {
			panic("OP_PAT_TEXT with non-string")
		}
		s := register_A.Str
		pat := data_stack[data_frame+S].Glob
		pat.AddText(s)

	case OP_PAT_GLOB:
		s := GetStackOrConstant(D).Str
		pat := data_stack[data_frame+S].Glob
		pat.AddGlob(s)

	case OP_PAT_RUN:
		pat := data_stack[data_frame+S].Glob
		list := pat.Matches()
		data_stack[data_frame+S].Glob = nil

		// append each matching name to array in A
		for _, s := range list {
			var v Value
			v.MakeString(s)
			register_A.AppendElem(v)
		}

	case OP_FILE_OPEN:
		if register_A.kind != TYP_String {
			panic("OP_FILE_OPEN with non-string filename")
		}
		data_stack[data_frame+S].MakeNIL()
		data_stack[data_frame+S].File = File_Open(register_A.Str, D, false)
		if data_stack[data_frame+S].File == nil {
			Die("cannot open file: %s", register_A.Str)
		}

	case OP_FILE_TEMP:
		data_stack[data_frame+S].MakeNIL()
		data_stack[data_frame+S].File = File_CreateTemp("PIPE", D)
		if data_stack[data_frame+S].File == nil {
			Die("cannot create temporary file")
		}

	case OP_FILE_REOPEN:
		file := data_stack[data_frame+S].File
		if File_Reopen(file, false) != nil {
			Die("cannot reopen temporary file")
		}

	case OP_FILE_CLOSE:
		file := data_stack[data_frame+S].File
		File_Close(file)

	default:
		panic("UNKNOWN OPCODE FOUND")
	}
}

func perform_Jump(D int) {
	if D < 0 || D >= len(call_frame.cl.vm_ops) {
		panic("BAD DESTINATION FOR JUMP")
	}

	call_frame.op_ptr = D
}

func perform_GetMethod(S int) {
	name_val := GetStackOrConstant(S)
	name     := name_val.Str

	if register_A.kind != TYP_Map {
		Die("cannot call methods on %s", register_A.kind.String())
		return
	}

	obj_class := register_A.Map.obj_class
	if obj_class == nil {
		Die("cannot call methods on a plain map")
		return
	}

	ptr := obj_class.methods[name]
	if ptr == nil {
		Die("no such method '%s' on class %s", name, obj_class.name)
		return
	}

	// ok
	register_A = *ptr
}

func perform_HasMethod(S int) {
	name_val := GetStackOrConstant(S)
	name     := name_val.Str

	if register_B.kind != TYP_Map {
		register_B.MakeBool(false)
		return
	}

	obj_class := register_B.Map.obj_class
	if obj_class == nil {
		register_B.MakeBool(false)
		return
	}

	_, exist := obj_class.methods[name]
	register_A.MakeBool(exist)
}

func perform_SetClass(S int) {
	if register_A.kind != TYP_Map {
		Die("cannot set class on %s", register_A.kind.String())
		return
	}
	if S < 0 {
		register_A.Map.obj_class = nil
		return
	}
	if S >= len(typedefs) {
		panic("BAD TYPEDEF for OP_SET_CLASS")
	}
	register_A.Map.obj_class = typedefs[S]
}

func perform_ForBegin(S, D int) {
	base_p := data_frame + S

	v_pos  := data_stack[base_p+0]
	v_targ := data_stack[base_p+1]
	v_step := data_stack[base_p+2]

	if !(v_pos.kind == TYP_Number || v_pos.kind == TYP_Char) {
		Die("for loop requires numeric start, got %s", v_pos.kind.String())
		return
	}
	if !(v_targ.kind == TYP_Number || v_targ.kind == TYP_Char) {
		Die("for loop requires numeric end, got %s", v_targ.kind.String())
		return
	}
	if v_targ.kind != v_pos.kind {
		Die("for loop requires same type for start and end")
		return
	}

	if D > 0 {
		// calculate a step value: -1 or +1

		if v_targ.Num >= v_pos.Num {
			data_stack[base_p+2].MakeNumber(+1)
		} else {
			data_stack[base_p+2].MakeNumber(-1)
		}

	} else {
		// validate an explicit step

		if !(v_step.kind == TYP_Number) {
			Die("for loop requires numeric step, got %s", v_step.kind.String())
			return
		}
	}
}

func perform_ForPast(S int) {
	base_p := data_frame + S

	pos  := data_stack[base_p+0].Num
	targ := data_stack[base_p+1].Num
	step := data_stack[base_p+2].Num

	if step > 0 {
		register_A.MakeBool(pos > targ)
	} else if step < 0 {
		register_A.MakeBool(pos < targ)
	} else {
		register_A.MakeBool(true)
	}
}

func perform_ForStep(S int) {
	base_p := data_frame + S

	pos  := data_stack[base_p+0].Num
	step := data_stack[base_p+2].Num
	next := pos + step

	// check for loss of accuracy
	if math.IsInf(next, 0) {
		goto failed

	} else if step > 0 && !(next > pos) {
		goto failed

	} else if step < 0 && !(next < pos) {
		goto failed
	}

	// ok
	data_stack[base_p+0].Num = next
	return

failed:
	Die("for loop failed to advance (loss of precision)")
}

func perform_EachBegin(S int) {
	base_p := data_frame + S

	// store the object
	data_stack[base_p+2] = register_A

	switch register_A.kind {
	case TYP_Array, TYP_String:
		// initial index
		data_stack[base_p+0].MakeNumber(0)

	case TYP_Map:
		// collect all the keys into a list.
		// we work our way *backwards* through this list.
		data_stack[base_p+3] = register_A.CollectKeys()

	default:
		Die("for-each requires an array, map or string")
		return
	}
}

func perform_EachStep(S, D int) {
	base_p := data_frame + S

	obj := data_stack[base_p+2]

	switch obj.kind {
	case TYP_Array:
		// update index var
		idx := int(data_stack[base_p+0].Num)
		data_stack[base_p+0].Num = float64(idx + 1)

		if idx >= len(obj.Array.data) {
			// nothing left, jump to end
			perform_Jump(D)
		} else {
			// update value var
			data_stack[base_p+1] = obj.GetElem(idx)
		}

	case TYP_String:
		// update index var
		idx := int(data_stack[base_p+0].Num)
		data_stack[base_p+0].Num = float64(idx + 1)

		str  := data_stack[base_p+2].Str
		r, n := utf8.DecodeRuneInString(str)

		if n == 0 {
			perform_Jump(D)
		} else {
			data_stack[base_p+1].MakeChar(r)

			// remove first rune from the string
			str = str[n:]
			data_stack[base_p+2].Str = str
		}

	case TYP_Map:
		length := data_stack[base_p+3].Length()

		if length == 0 {
			perform_Jump(D)
		} else {
			// remove last key from the list
			key := data_stack[base_p+3].GetElem(length - 1)
			data_stack[base_p+3].RemoveElem(length - 1)

			// update index and value vars
			data_stack[base_p+0] = key
			data_stack[base_p+1] = obj.KeyGet(key.Str)
		}

	default:
		Die("for-each requires an array/map/string")
		return
	}
}

func perform_FunCall(S, D int) {
	F := GetStackOrConstant(S)

	if F.kind != TYP_Function {
		Die("cannot call %s", F.kind.String())
		return
	}

	new_cl   := F.Clos
	base_p   := data_frame + S
	num_pars := D

	// a built-in primitive?
	if new_cl.builtin != nil {
		if num_pars != new_cl.builtin.args {
			Die("wrong number of params to %s, wanted: %d, got: %d",
				new_cl.builtin.name, new_cl.builtin.args, num_pars)
			return
		}

		if PushCallStack(new_cl, base_p) {
			new_cl.builtin.code()
			PopCallStack()
		}
	} else {
		if num_pars != len(new_cl.parameters) {
			Die("wrong number of params to %s, wanted: %d, got: %d",
				new_cl.debug_name, len(new_cl.parameters), num_pars)
			return
		}

		PushCallStack(new_cl, base_p)

		// nothing else is needed, RunClosure() handles it
	}
}

func perform_TailCall(S, D int) {
	F := GetStackOrConstant(S)

	if F.kind != TYP_Function {
		Die("cannot call %s", F.kind.String())
		return
	}

	new_cl := F.Clos

	// builtin functions must be done as a normal call
	if new_cl.builtin != nil {
		perform_FunCall(S, D)
		return
	}

	// check number of parameters
	num_pars := D

	if num_pars != len(new_cl.parameters) {
		Die("wrong number of parameters, wanted: %d, got: %d",
			len(new_cl.parameters), num_pars)
		return
	}

	// rejig the data stack: copy down the parameters
	offset := S

	for i := 0; i <= num_pars; i++ {
		data_stack[data_frame+i] = data_stack[data_frame+offset+i]
	}

	// rejig the call stack
	old_frame := data_frame

	PopCallStack()
	PushCallStack(new_cl, old_frame)
}

func perform_ElemRead() {
	switch register_B.kind {
	case TYP_Array:
		if register_A.kind != TYP_Number {
			Die("array access: index must be integer")
			return
		}
		idx := util_to_int(register_A.Num)

		if idx < 0 || idx >= int64(register_B.Length()) {
			Die("array access: index out of bounds")
			return
		}

		register_A = register_B.GetElem(int(idx))

	case TYP_String:
		if register_A.kind != TYP_Number {
			Die("string access: index must be integer")
			return
		}
		idx := util_to_int(register_A.Num)

		r := []rune(register_B.Str)

		if idx < 0 || idx >= int64(len(r)) {
			Die("string access: index out of bounds")
			return
		}
		register_A.MakeChar(r[idx])

	case TYP_Map:
		if register_A.kind != TYP_String {
			Die("map access: key must be a string")
			return
		}

		key := register_A.Str
		register_A = register_B.KeyGet(key)

	default:
		Die("cannot read an element of %s", register_B.kind.String())
		return
	}
}

func perform_ElemWrite() {
	switch register_B.kind {
	case TYP_Array:
		if register_C.kind != TYP_Number {
			Die("array access: index must be a number")
			return
		}
		idx := util_to_int(register_C.Num)

		if idx < 0 || idx >= int64(register_B.Length()) {
			Die("array access: index out of bounds")
			return
		}
		register_B.SetElem(int(idx), register_A)

	case TYP_Map:
		if register_C.kind != TYP_String {
			Die("map access: key must be a string")
			return
		}
		register_B.KeySet(register_C.Str, register_A)

	default:
		Die("cannot set an element of %s", register_B.kind.String())
		return
	}
}

func perform_MakeFunc(S int) {
	template := GetStackOrConstant(S).Clos

	new_cl := NewClosure(template.debug_name)

	new_cl.is_function = true
	new_cl.vm_ops      = template.vm_ops
	new_cl.parameters  = template.parameters
	new_cl.constants   = template.constants

	// upvars are created by OP_NEW_UPVAR instructions

	register_A.MakeFunction(new_cl)
}

func perform_ToString() {
	switch register_A.kind {
	case TYP_Char:
		var r [1]rune ; r[0] = rune(register_A.Num)
		register_A.MakeString(string(r[:]))
		return

	case TYP_String:
		// no change
		return

	case TYP_Map:
		// for an object, look for a stringify method
		obj_class := register_A.Map.obj_class

		if obj_class != nil {
			v := obj_class.methods["str#"]

			if v != nil {
				child_p := ChildDataFrame()
				data_stack[child_p+1] = register_A
				RunClosure(v.Clos, child_p)

				if register_A.kind != TYP_String {
					Die("str# method of class %s returned a non-string", obj_class.name)
				}
				return
			}
		}

		// no method, so use normal stringifier
	}

	s := register_A.DeepString()
	register_A.MakeString(s)
}

func perform_ArgRead(S int) {
	count := Glob.ARGS.loc.Length()

	if 0 <= S && S < count {
		register_A = Glob.ARGS.loc.GetElem(S)
	} else {
		register_A.MakeString("")
	}
}

func perform_ExecArg(cmd *CommandBuilder) {
	switch register_A.kind {
	case TYP_String:
		cmd.Arg(register_A.Str)

	case TYP_Array:
		for i := range register_A.Array.data {
			v := register_A.GetElem(i)

			// TODO Review this
			switch v.kind {
			case TYP_String: cmd.Arg(v.Str)
			case TYP_Array:  cmd.Arg("[array]")
			case TYP_Map:    cmd.Arg("[map]")
			default:         cmd.Arg(v.DeepString())
			}
		}

	case TYP_Map:
		obj_class := register_A.Map.obj_class

		// objects may have a stringify method...
		if obj_class != nil {
			perform_ToString()
			cmd.Arg(register_A.Str)
			return
		}

		// TODO CollectSortedKeys
		list := make([]string, 0)

		for key, _ := range register_A.Map.data {
			if key != "" {
				list = append(list, key)
			}
		}

		sort.Slice(list, func(i, k int) bool {
			return list[i] < list[k]
		})

		for _, s := range list {
			cmd.Arg(s)
		}

	default:
		perform_ToString()
		cmd.Arg(register_A.Str)
	}
}

func perform_ExecRedir(cmd *CommandBuilder, info *FileInfo) {
	switch register_A.Num {
	case 0: cmd.stdin  = info
	case 1: cmd.stdout = info
	case 2: cmd.stderr = info
	}
}

func perform_ExecRun(cmd *CommandBuilder) {
	err := Run_Command(cmd)

	if err != nil {
		// FIXME temp stuff
		println("exec failure:", err.Error())
	}
}

//----------------------------------------------------------------------

func perform_Neg() {
	if register_A.kind != TYP_Number {
		Die("operator - requires a number, got %s", register_A.kind.String())
	}
	register_A.Num = - register_A.Num
}

func perform_Not() {
	register_A.MakeBool(! register_A.IsTrue())
}

func perform_Flip() {
	if register_A.kind != TYP_Number {
		Die("operator ~ requires a number, got %s", register_A.kind.String())
	}
	register_A.MakeNumber(util_bit_flip(register_A.Num))
}

func perform_Add() {
	if register_B.kind != register_A.kind {
		Die("operator + requires same type, got %s and %s",
			register_B.kind.String(), register_A.kind.String())
		return
	}
	if register_B.kind == TYP_String {
		register_A.MakeString(register_B.Str + register_A.Str)
		return
	}
	if register_B.kind != TYP_Number {
		Die("operator + requires a number, got %s", register_B.kind.String())
		return
	}
	register_A.MakeNumber(register_B.Num + register_A.Num)
}

func perform_Sub() {
	if register_A.kind != TYP_Number {
		Die("operator - requires a number, got %s", register_A.kind.String())
	}
	if register_B.kind != TYP_Number {
		Die("operator - requires a number, got %s", register_B.kind.String())
		return
	}
	register_A.MakeNumber(register_B.Num - register_A.Num)
}

func perform_Mul() {
	if register_A.kind != TYP_Number {
		Die("operator * requires a number, got %s", register_A.kind.String())
	}
	if register_B.kind != TYP_Number {
		Die("operator * requires a number, got %s", register_B.kind.String())
		return
	}
	register_A.MakeNumber(register_B.Num * register_A.Num)
}

func perform_Div() {
	if register_B.kind != register_A.kind {
		Die("operator / requires same type, got %s and %s",
			register_B.kind.String(), register_A.kind.String())
		return
	}
	if register_B.kind != TYP_Number {
		Die("operator / requires a number, got %s", register_B.kind.String())
		return
	}
	if register_A.Num == 0 {
		Die("division by zero")
		return
	}
	register_A.MakeNumber(register_B.Num / register_A.Num)
}

func perform_Rem() {
	if register_A.kind != TYP_Number {
		Die("operator % requires a number, got %s", register_A.kind.String())
	}
	if register_B.kind != TYP_Number {
		Die("operator % requires a number, got %s", register_B.kind.String())
	}
	if register_A.Num == 0 {
		Die("division by zero")
		return
	}
	register_A.MakeNumber(util_remainder(register_B.Num, register_A.Num))
}

func perform_Power() {
	if register_A.kind != TYP_Number {
		Die("operator ** requires a number, got %s", register_A.kind.String())
	}
	if register_B.kind != TYP_Number {
		Die("operator ** requires a number, got %s", register_B.kind.String())
	}
	register_A.MakeNumber(util_power(register_B.Num, register_A.Num))
}

func perform_And() {
	if register_A.kind != TYP_Number {
		Die("operator & requires a number, got %s", register_A.kind.String())
	}
	if register_B.kind != TYP_Number {
		Die("operator & requires a number, got %s", register_B.kind.String())
	}
	register_A.MakeNumber(util_bit_and(register_B.Num, register_A.Num))
}

func perform_Or() {
	if register_A.kind != TYP_Number {
		Die("operator | requires a number, got %s", register_A.kind.String())
	}
	if register_B.kind != TYP_Number {
		Die("operator | requires a number, got %s", register_B.kind.String())
	}
	register_A.MakeNumber(util_bit_or(register_B.Num, register_A.Num))
}

func perform_Xor() {
	if register_A.kind != TYP_Number {
		Die("operator ~ requires a number, got %s", register_A.kind.String())
	}
	if register_B.kind != TYP_Number {
		Die("operator ~ requires a number, got %s", register_B.kind.String())
	}
	register_A.MakeNumber(util_bit_xor(register_B.Num, register_A.Num))
}

func perform_ShiftLeft() {
	if register_A.kind != TYP_Number {
		Die("operator >> requires a number, got %s", register_A.kind.String())
	}
	if register_B.kind != TYP_Number {
		Die("operator >> requires a number, got %s", register_B.kind.String())
	}
	register_A.MakeNumber(util_shift_left(register_B.Num, register_A.Num))
}

func perform_ShiftRight() {
	if register_A.kind != TYP_Number {
		Die("operator << requires a number, got %s", register_A.kind.String())
	}
	if register_B.kind != TYP_Number {
		Die("operator << requires a number, got %s", register_B.kind.String())
	}
	register_A.MakeNumber(util_shift_right(register_B.Num, register_A.Num))
}

func VM_comparison() int {
	a := register_B
	b := register_A

	if a.kind != b.kind {
		Die("cannot compare order of different types")
		return 0
	}

	switch a.kind {
	case TYP_Void:
		return 0

	case TYP_Number, TYP_Char, TYP_Bool:
		switch {
		case a.Num < b.Num:
			return -1
		case a.Num > b.Num:
			return +1
		default:
			return 0
		}

	case TYP_String:
		switch {
		case a.Str < b.Str:
			return -1
		case a.Str > b.Str:
			return +1
		default:
			return 0
		}

	case TYP_Array:
		Die("cannot compare order of arrays")

	case TYP_Map:
		Die("cannot compare order of maps")

	case TYP_Function:
		Die("cannot compare order of functions")
	}

	return 0
}

//----------------------------------------------------------------------

func GetStackOrConstant(ofs int) Value {
	if ofs >= 0 {
		return data_stack[data_frame+ofs]
	}

	// a constant index
	cl := call_frame.cl
	ofs = -ofs - 1

	if ofs >= len(cl.constants) {
		panic("BAD CONSTANT INDEX")
	}
	return cl.constants[ofs]
}

func Die(msg string, a ...interface{}) {
	run_error = true

	// determine location of the error
	line_num := 0
	file_num := 0

	frame := &call_stack[call_p]

	if call_p >= 1 && frame.cl.builtin != nil {
		frame = &call_stack[call_p-1]
	}

	line_num = frame.DetermineLine()
	file_num = frame.cl.cur_pos.file

	Error_SetFile(file_num)
	Error_SetLine(line_num)

	PostError(msg, a...)

	if trace_depth > 0 {
		ShowStackTrace()
	}
}

//----------------------------------------------------------------------

func DumpOps(cl *Closure) {
	fmt.Printf("vm_ops for %s\n", cl.debug_name)
	for idx, op := range cl.vm_ops {
		fmt.Printf("   [%03d] %s\n", idx, op.String(cl))
	}
}

func (op *Operation) String(cl *Closure) string {
	S := op.PartString(cl, "S", op.S)
	D := op.PartString(cl, "D", op.D)

	return fmt.Sprintf("%-14s  %-14s  %-14s", op.OpString(), S, D)
}

func (op *Operation) OpString() string {
	switch op.kind {
	case OP_A_TO_B:      return "OP_A_TO_B"
	case OP_A_TO_C:      return "OP_A_TO_C"
	case OP_B_TO_A:      return "OP_B_TO_A"
	case OP_C_TO_A:      return "OP_C_TO_A"

	case OP_LOAD_A:      return "OP_LOAD_A"
	case OP_LOAD_B:      return "OP_LOAD_B"
	case OP_LOAD_C:      return "OP_LOAD_C"

	case OP_STORE_A:     return "OP_STORE_A"
	case OP_STORE_B:     return "OP_STORE_B"
	case OP_STORE_C:     return "OP_STORE_C"

	case OP_COPY:        return "OP_COPY"

	case OP_NOP:         return "OP_NOP"
	case OP_ERROR:       return "OP_ERROR"
	case OP_JUMP:        return "OP_JUMP"
	case OP_IF_FALSE:    return "OP_IF_FALSE"
	case OP_IF_TRUE:     return "OP_IF_TRUE"
	case OP_FUN_CALL:    return "OP_FUN_CALL"
	case OP_TAIL_CALL:   return "OP_TAIL_CALL"
	case OP_RETURN:      return "OP_RETURN"

	case OP_FOR_BEGIN:   return "OP_FOR_BEGIN"
	case OP_FOR_PAST:    return "OP_FOR_PAST"
	case OP_FOR_STEP:    return "OP_FOR_STEP"
	case OP_EACH_BEGIN:  return "OP_EACH_BEGIN"
	case OP_EACH_STEP:   return "OP_EACH_STEP"

	case OP_NEW_EXVAR:   return "OP_NEW_EXVAR"
	case OP_NEW_UPVAR:   return "OP_NEW_UPVAR"
	case OP_COPY_UPVAR:  return "OP_COPY_UPVAR"
	case OP_HOIST_VAL:   return "OP_HOIST_VAL"

	case OP_GLOB_READ:   return "OP_GLOB_READ"
	case OP_GLOB_WRITE:  return "OP_GLOB_WRITE"
	case OP_ENV_READ:    return "OP_ENV_READ"
	case OP_ENV_WRITE:   return "OP_ENV_WRITE"

	case OP_EXVAR_READ:  return "OP_EXVAR_READ"
	case OP_EXVAR_WRITE: return "OP_EXVAR_WRITE"
	case OP_UPV_READ:    return "OP_UPV_READ"
	case OP_UPV_WRITE:   return "OP_UPV_WRITE"

	case OP_ELEM_READ:   return "OP_ELEM_READ"
	case OP_ELEM_WRITE:  return "OP_ELEM_WRITE"
	case OP_APPEND:      return "OP_APPEND"
	case OP_GET_METHOD:  return "OP_GET_METHOD"
	case OP_HAS_METHOD:  return "OP_HAS_METHOD"
	case OP_SET_CLASS:   return "OP_SET_CLASS"

	case OP_MAKE_NIL:    return "OP_MAKE_NIL"
	case OP_MAKE_TRUE:   return "OP_MAKE_TRUE"
	case OP_MAKE_FALSE:  return "OP_MAKE_FALSE"
	case OP_MAKE_CHAR:   return "OP_MAKE_CHAR"
	case OP_MAKE_NUMBER: return "OP_MAKE_NUMBER"
	case OP_MAKE_FUNC:   return "OP_MAKE_FUNC"
	case OP_MAKE_ARRAY:  return "OP_MAKE_ARRAY"
	case OP_MAKE_MAP:    return "OP_MAKE_MAP"

	case OP_NOT:         return "OP_NOT"
	case OP_NEG:         return "OP_NEG"
	case OP_FLIP:        return "OP_FLIP"
	case OP_TO_BOOL:     return "OP_TO_BOOL"
	case OP_TO_STR:      return "OP_TO_STR"

	case OP_ADD:         return "OP_ADD"
	case OP_SUB:         return "OP_SUB"
	case OP_MUL:         return "OP_MUL"
	case OP_DIV:         return "OP_DIV"
	case OP_REM:         return "OP_REM"
	case OP_POW:         return "OP_POW"

	case OP_AND:         return "OP_AND"
	case OP_OR:          return "OP_OR"
	case OP_XOR:         return "OP_XOR"
	case OP_SHIFT_L:     return "OP_SHIFT_L"
	case OP_SHIFT_R:     return "OP_SHIFT_R"
	case OP_MIN:         return "OP_MIN"
	case OP_MAX:         return "OP_MAX"

	case OP_EQ:          return "OP_EQ"
	case OP_NE:          return "OP_NE"
	case OP_LT:          return "OP_LT"
	case OP_LE:          return "OP_LE"
	case OP_GT:          return "OP_GT"
	case OP_GE:          return "OP_GE"

	case OP_CALC_OK:     return "OP_CALC_OK"
	case OP_ARG_NUM:     return "OP_ARG_NUM"
	case OP_ARG_READ:    return "OP_ARG_READ"
	case OP_USER_DIR:    return "OP_USER_DIR"

	case OP_EXEC_BEGIN:  return "OP_EXEC_BEGIN"
	case OP_EXEC_ENV:    return "OP_EXEC_ENV"
	case OP_EXEC_ARG:    return "OP_EXEC_ARG"
	case OP_EXEC_COUNT:  return "OP_EXEC_COUNT"
	case OP_EXEC_REDIR:  return "OP_EXEC_REDIR"
	case OP_EXEC_RUN:    return "OP_EXEC_RUN"

	case OP_PAT_BEGIN:   return "OP_PAT_BEGIN"
	case OP_PAT_TEXT:    return "OP_PAT_TEXT"
	case OP_PAT_GLOB:    return "OP_PAT_GLOB"
	case OP_PAT_RUN:     return "OP_PAT_RUN"

	case OP_FILE_OPEN:   return "OP_FILE_OPEN"
	case OP_FILE_TEMP:   return "OP_FILE_TEMP"
	case OP_FILE_REOPEN: return "OP_FILE_REOPEN"
	case OP_FILE_CLOSE:  return "OP_FILE_CLOSE"

	default:
		return "?????"
	}
}

func (op *Operation) PartString(cl *Closure, name string, val int32) string {
	if val == NO_PART {
		return ""
	}

	// handle global vars
	if op.kind == OP_GLOB_READ && name == "S" {
		def := globals[op.S]
		return fmt.Sprintf("%s=%v (%s)", name, val, def.name)
	}
	if op.kind == OP_GLOB_WRITE && name == "D" {
		def := globals[op.D]
		return fmt.Sprintf("%s=%v (%s)", name, val, def.name)
	}

	// handle the constants block
	if op.kind != OP_MAKE_NUMBER {
		if val < 0 && int(-val-1) < len(cl.constants) {
			v := cl.constants[-val-1]
			switch v.kind {
			case TYP_Bool, TYP_Char, TYP_Number, TYP_String:
				return fmt.Sprintf("%s=%v (%s)", name, val, v.DeepString())
			}
		}
	}

	return fmt.Sprintf("%s=%v", name, val)
}
