// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "io"
import "math"
import "strings"
import "math/rand"
import "unicode"

import "github.com/peterh/liner"

type Builtin struct {
	name string
	args int
	code func()
}

func SetupBuiltins() {
	// mathematical
	RegisterBuiltin("abs",   1, BI_Abs)
	RegisterBuiltin("sqrt",  1, BI_Sqrt)

	RegisterBuiltin("round", 1, BI_Round)
	RegisterBuiltin("trunc", 1, BI_Truncate)
	RegisterBuiltin("floor", 1, BI_Floor)
	RegisterBuiltin("ceil",  1, BI_Ceil)

	RegisterBuiltin("zero?", 1, BI_IsZero)
	RegisterBuiltin("some?", 1, BI_IsSome)
	RegisterBuiltin("neg?",  1, BI_IsNeg)
	RegisterBuiltin("pos?",  1, BI_IsPos)

	RegisterBuiltin("inf?",  1, BI_IsInfinity)
	RegisterBuiltin("nan?",  1, BI_IsNan)

	RegisterBuiltin("log",   1, BI_Log)
	RegisterBuiltin("log10", 1, BI_Log10)
	RegisterBuiltin("frexp", 1, BI_Frexp)
	RegisterBuiltin("ldexp", 2, BI_Ldexp)

	RegisterBuiltin("rad->deg", 1, BI_ToDegrees)
	RegisterBuiltin("deg->rad", 1, BI_ToRadians)

	RegisterBuiltin("sin",   1, BI_TrigSin)
	RegisterBuiltin("cos",   1, BI_TrigCos)
	RegisterBuiltin("tan",   1, BI_TrigTan)
	RegisterBuiltin("asin",  1, BI_TrigAsin)
	RegisterBuiltin("acos",  1, BI_TrigAcos)
	RegisterBuiltin("atan",  1, BI_TrigAtan)
	RegisterBuiltin("atan2", 2, BI_TrigAtan2)
	RegisterBuiltin("hypot", 2, BI_TrigHypot)

	// object identitiy
	RegisterBuiltin("ref-eq?", 2, BI_RefEQ)
	RegisterBuiltin("ref-ne?", 2, BI_RefNE)

	// type checking
	RegisterBuiltin("nil?",    1, BI_TypeNil)
	RegisterBuiltin("bool?",   1, BI_TypeBool)
	RegisterBuiltin("char?",   1, BI_TypeChar)
	RegisterBuiltin("number?", 1, BI_TypeNumber)

	RegisterBuiltin("string?", 1, BI_TypeString)
	RegisterBuiltin("array?",  1, BI_TypeArray)
	RegisterBuiltin("map?",    1, BI_TypeMap)
	RegisterBuiltin("fun?",    1, BI_TypeFunction)

	RegisterBuiltin("class$",  1, BI_ClassName)

	// string manipulation
	RegisterBuiltin("hex$",   1, BI_HexStr)
	RegisterBuiltin("quote$", 1, BI_QuotedStr)

	RegisterBuiltin("lowercase", 1, BI_Lowercase)
	RegisterBuiltin("uppercase", 1, BI_Uppercase)
	RegisterBuiltin("titlecase", 1, BI_Titlecase)

	RegisterBuiltin("parse-int",   1, BI_ParseInt)
	RegisterBuiltin("parse-float", 1, BI_ParseFloat)

	// character tests, manipulation
	RegisterBuiltin("char-letter?",  1, BI_CharLetter)
	RegisterBuiltin("char-digit?",   1, BI_CharDigit)
	RegisterBuiltin("char-numeric?", 1, BI_CharNumeric)
	RegisterBuiltin("char-symbol?",  1, BI_CharSymbol)
	RegisterBuiltin("char-mark?",    1, BI_CharMark)
	RegisterBuiltin("char-space?",   1, BI_CharSpace)
	RegisterBuiltin("char-control?", 1, BI_CharControl)

	RegisterBuiltin("char-lower?", 1, BI_CharLower)
	RegisterBuiltin("char-upper?", 1, BI_CharUpper)
	RegisterBuiltin("char-title?", 1, BI_CharTitle)

	RegisterBuiltin("char->num", 1, BI_CharToNum)
	RegisterBuiltin("num->char", 1, BI_NumToChar)

	// array and map functions
	RegisterBuiltin("len",        1, BI_Len)
	RegisterBuiltin("empty?",     1, BI_Empty)
	RegisterBuiltin("not-empty?", 1, BI_NotEmpty)

	RegisterBuiltin("copy",    1, BI_Copy)
	RegisterBuiltin("subseq",  3, BI_Subseq)
	RegisterBuiltin("find",    2, BI_Find)

	RegisterBuiltin("append!",    2, BI_Append)
	RegisterBuiltin("insert!",    3, BI_Insert)
	RegisterBuiltin("remove!",    2, BI_Remove)
	RegisterBuiltin("clear-all!", 1, BI_ClearAll)

	RegisterBuiltin("join!",      2, BI_Join)
	RegisterBuiltin("map!",       2, BI_Map)
	RegisterBuiltin("filter!",    2, BI_Filter)
	RegisterBuiltin("sort!",      2, BI_Sort)

	// random numbers
	RegisterBuiltin("rand-seed",  1, BI_RandSeed)
	RegisterBuiltin("rand-float", 0, BI_RandFloat)
	RegisterBuiltin("rand-range", 2, BI_RandRange)

	// I/O and misc
	RegisterBuiltin("error", 1, BI_Error)
	RegisterBuiltin("print", 1, BI_Print)
	RegisterBuiltin("input", 1, BI_Input)
	RegisterBuiltin("enable-editor", 0, BI_EnableEditor)
}

func RegisterBuiltin(name string, args int, code func()) {
	bu := new(Builtin)
	bu.name = name
	bu.args = args
	bu.code = code

	cl := NewClosure(name)
	cl.builtin = bu

	def := MakeGlobal(name, false)
	def.loc.MakeFunction(cl)
}

//----------------------------------------------------------------------

func BI_HexStr() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("hex$ function requires a number, got %s", v.kind.String())
		return
	}

	num := uint64(util_to_int(v.Num))

	s := fmt.Sprintf("0x%016X", num)

	register_A.MakeString(s)
}

func BI_QuotedStr() {
	v := data_stack[data_frame+1]

	switch v.kind {
	case TYP_Char:
		quoted := EncodeCharLiteral(rune(v.Num))
		register_A.MakeString(quoted)

	case TYP_String:
		quoted := EncodeStringLiteral(v.Str)
		register_A.MakeString(quoted)

	default:
		Die("quote$ function requires a char or string, got %s", v.kind.String())
		return
	}
}

func BI_ParseInt() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_String {
		Die("parse-int function requires a string, got %s", v.kind.String())
		return
	}

	// DecodeInteger will reject anything that is beyond a 64-bit
	// signed integer, but also anything which cannot be exactly
	// represented in a 64-bit float.

	err := register_A.DecodeInteger(v.Str)
	if err != nil {
		register_A.MakeNIL()
	}
}

func BI_ParseFloat() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_String {
		Die("parse-int function requires a string, got %s", v.kind.String())
		return
	}

	err := register_A.DecodeFloat(v.Str)
	if err != nil {
		register_A.MakeNIL()
	}
}

func BI_Lowercase() {
	v := data_stack[data_frame+1]

	switch v.kind {
	case TYP_Char:
		register_A.MakeChar(unicode.ToLower(rune(v.Num)))

	case TYP_String:
		register_A.MakeString(strings.ToLower(v.Str))

	default:
		Die("lowercase function requires char or string")
		return
	}
}

func BI_Uppercase() {
	v := data_stack[data_frame+1]

	switch v.kind {
	case TYP_Char:
		register_A.MakeChar(unicode.ToUpper(rune(v.Num)))

	case TYP_String:
		register_A.MakeString(strings.ToUpper(v.Str))

	default:
		Die("uppercase function requires char or string")
		return
	}
}

func BI_Titlecase() {
	v := data_stack[data_frame+1]

	switch v.kind {
	case TYP_Char:
		register_A.MakeChar(unicode.ToTitle(rune(v.Num)))

	case TYP_String:
		register_A.MakeString(strings.ToTitle(v.Str))

	default:
		Die("titlecase function requires char or string")
		return
	}
}

//----------------------------------------------------------------------

func BI_CharLetter() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char-letter? function requires a char, got %s", v.kind.String())
		return
	}

	r   := rune(v.Num)
	res := unicode.IsLetter(r)

	register_A.MakeBool(res)
}

func BI_CharDigit() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char-digit? function requires a char, got %s", v.kind.String())
		return
	}

	r   := rune(v.Num)
	res := unicode.IsDigit(r)

	register_A.MakeBool(res)
}

func BI_CharNumeric() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char-numeric? function requires a char, got %s", v.kind.String())
		return
	}

	r   := rune(v.Num)
	res := unicode.IsNumber(r)

	register_A.MakeBool(res)
}

func BI_CharSymbol() {
	// Note: we treat P-class ("Punctuation") and S-class "Symbols"
	// the same here.  It is not really clear what the difference is.
	// e.g. '*' is considered to be punctuation, and '+' a symbol.
	// Also a single quote is punctuation, back-quote is a symbol.

	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char-symbol? function requires a char, got %s", v.kind.String())
		return
	}

	r   := rune(v.Num)
	res := unicode.IsSymbol(r) || unicode.IsPunct(r)

	register_A.MakeBool(res)
}

func BI_CharMark() {
	// Note: a "mark" is a combining character, like diacritics.
	// The unicode range U+0300 to U+036F are the main ones (but
	// there are others).

	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char-mark? function requires a char, got %s", v.kind.String())
		return
	}

	r   := rune(v.Num)
	res := unicode.IsMark(r)

	register_A.MakeBool(res)
}

func BI_CharSpace() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char-space? function requires a char, got %s", v.kind.String())
		return
	}

	r   := rune(v.Num)
	res := unicode.IsSpace(r)

	register_A.MakeBool(res)
}

func BI_CharControl() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char-control? function requires a char, got %s", v.kind.String())
		return
	}

	r   := rune(v.Num)
	res := unicode.IsControl(r)

	register_A.MakeBool(res)
}

//----------------------------------------------------------------------

func BI_CharUpper() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char-upper? function requires a char, got %s", v.kind.String())
		return
	}

	r   := rune(v.Num)
	res := unicode.IsUpper(r)

	register_A.MakeBool(res)
}

func BI_CharLower() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char-lower? function requires a char, got %s", v.kind.String())
		return
	}

	r   := rune(v.Num)
	res := unicode.IsLower(r)

	register_A.MakeBool(res)
}

func BI_CharTitle() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char-title? function requires a char, got %s", v.kind.String())
		return
	}

	r   := rune(v.Num)
	res := unicode.IsTitle(r)

	register_A.MakeBool(res)
}

func BI_CharToNum() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Char {
		Die("char->num function requires a char, got %s", v.kind.String())
		return
	}

	register_A.MakeNumber(v.Num)
}

func BI_NumToChar() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("num->char function requires a number, got %s", v.kind.String())
		return
	}

	num := util_to_int(v.Num)

	// illegal values become the "error char"
	if num < 0 || num > 0x10FFFF {
		num = 0xFFFD
	}

	register_A.MakeChar(rune(num))
}

//----------------------------------------------------------------------

func BI_Len() {
	v := data_stack[data_frame+1]

	register_A.MakeNumber(float64(v.Length()))
}

func BI_Empty() {
	v := data_stack[data_frame+1]

	register_A.MakeBool(v.Length() == 0)
}

func BI_NotEmpty() {
	v := data_stack[data_frame+1]

	register_A.MakeBool(v.Length() > 0)
}

func BI_Append() {
	obj := data_stack[data_frame+1]
	val := data_stack[data_frame+2]

	if obj.kind != TYP_Array {
		Die("append! function requires an array, got %s", obj.kind.String())
		return
	}

	obj.AppendElem(val)

	register_A.MakeNIL()
}

func BI_Insert() {
	obj := data_stack[data_frame+1]
	idx := data_stack[data_frame+2]
	val := data_stack[data_frame+3]

	switch obj.kind {
	case TYP_Array:
		if idx.kind != TYP_Number {
			Die("insert! in array requires a numeric index, got %s", idx.kind.String())
			return
		}
		n := util_to_int(idx.Num)
		if n < 0 { n = 0 }
		if n > 0x7FFFFFFF { n = 0x7FFFFFFF }
		obj.InsertElem(int(n), val)

	case TYP_Map:
		if idx.kind != TYP_String {
			Die("insert! in map requires a string key, got %s", idx.kind.String())
			return
		}
		obj.KeySet(idx.Str, val)

	default:
		Die("insert! function requires an array or map, got %s", obj.kind.String())
		return
	}

	register_A.MakeNIL()
}

func BI_Remove() {
	obj := data_stack[data_frame+1]
	idx := data_stack[data_frame+2]

	switch obj.kind {
	case TYP_Array:
		if idx.kind != TYP_Number {
			Die("remove! in array requires a numeric index, got %s", idx.kind.String())
			return
		}
		n := util_to_int(idx.Num)
		if 0 <= n && n <= 0x7FFFFFFF {
			obj.RemoveElem(int(n))
		}

	case TYP_Map:
		if idx.kind != TYP_String {
			Die("remove! in map requires a string key, got %s", idx.kind.String())
			return
		}
		obj.KeyDelete(idx.Str)

	default:
		Die("remove! function requires an array or map, got %s", obj.kind.String())
		return
	}

	register_A.MakeNIL()
}

func BI_ClearAll() {
	obj := data_stack[data_frame+1]

	switch obj.kind {
	case TYP_Array:
		obj.ClearArray()

	case TYP_Map:
		obj.ClearMap()

	default:
		Die("clear-all! function requires an array or map, got %s", obj.kind.String())
		return
	}

	register_A.MakeNIL()
}

func BI_Join() {
	dest := data_stack[data_frame+1]
	src  := data_stack[data_frame+2]

	if src.kind != dest.kind {
		Die("join! function requires same types, got %s and %s",
			dest.kind.String(), src.kind.String())
		return
	}

	if dest.kind == TYP_Array {
		if dest.Array == src.Array {
			Die("cannot join array with itself")
			return
		}
		for _, val := range src.Array.data {
			dest.AppendElem(val)
		}
		register_A.MakeNIL()
		return
	}

	if dest.kind == TYP_Map {
		if dest.Map == src.Map {
			// joining a map with itself is a no-op
		} else {
			for key, ptr := range src.Map.data {
				dest.KeySet(key, *ptr)
			}
		}
		register_A.MakeNIL()
		return
	}

	Die("join! function requires arrays or maps, got %s", dest.kind.String())
}

func BI_Copy() {
	register_A.ShallowCopy(& data_stack[data_frame+1])
}

func BI_Subseq() {
	obj := data_stack[data_frame+1]

	if !(obj.kind == TYP_Array || obj.kind == TYP_String) {
		Die("subseq function requires an array or string, got %s", obj.kind.String())
		return
	}

	// get current length, for string: convert to runes
	var r []rune
	var length int

	if obj.kind == TYP_Array {
		length = len(obj.Array.data)
	} else {
		r = []rune(obj.Str)
		length = len(r)
	}

	// determine the start and end indices
	v1 := data_stack[data_frame+2]
	v2 := data_stack[data_frame+3]

	if v1.kind != TYP_Number {
		Die("subseq function requires a numeric range, got %s", v1.kind.String())
		return
	}
	if v2.kind != TYP_Number {
		Die("subseq function requires a numeric range, got %s", v2.kind.String())
		return
	}

	start := util_to_int(v1.Num)
	end   := util_to_int(v2.Num)

	// clamp the start to 0, end to the length
	if start < 0 {
		start = 0
	}
	if end > int64(length) {
		end = int64(length)
	}

	// result will be empty?
	// [ this handles all weird cases, e.g. start >= length ]
	if start >= end {
		if obj.kind == TYP_Array {
			register_A.MakeArray(0)
		} else {
			register_A.MakeString("")
		}
		return
	}

	// produce a shallow copy of the given range
	if obj.kind == TYP_String {
		r = r[start:end]
		register_A.MakeString(string(r))

	} else {
		register_A.MakeArray(int(end - start))
		copy(register_A.Array.data, obj.Array.data[start:end])
	}
}

func BI_Map() {
	dest := data_stack[data_frame+1]

	if dest.kind != TYP_Array {
		Die("map! function requires an array, got %s", dest.kind.String())
		return
	}

	pred := data_stack[data_frame+2]

	if pred.kind != TYP_Function {
		Die("map! function requires a function predicate, got %s", pred.kind.String())
		return
	}
	if pred.NumParameters() != 1 {
		Die("map! function requires a predicate with one parameter")
		return
	}

	for i := 0; i < dest.Length(); i++ {
		// push arguments to predicate
		child_p := ChildDataFrame()

		data_stack[child_p+1] = dest.GetElem(i)

		// run the predicate!
		if RunClosure(pred.Clos, child_p) != OK {
			return
		}

		dest.SetElem(i, register_A)
	}

	register_A.MakeNIL()
}

func BI_Filter() {
	dest := data_stack[data_frame+1]

	if dest.kind != TYP_Array {
		Die("filter! function requires an array, got %s", dest.kind.String())
		return
	}

	pred := data_stack[data_frame+2]

	if pred.kind != TYP_Function {
		Die("filter! function requires a function predicate, got %s", pred.kind.String())
		return
	}
	if pred.NumParameters() != 1 {
		Die("filter! function requires predicate with one parameter")
		return
	}

	// it is essential to iterate *backwards* through the array
	for i := dest.Length() - 1; i >= 0; i-- {
		child_p := ChildDataFrame()

		data_stack[child_p+1] = dest.GetElem(i)

		// run the predicate!
		if RunClosure(pred.Clos, child_p) != OK {
			return
		}

		if !register_A.IsTrue() {
			dest.RemoveElem(i)
		}
	}

	register_A.MakeNIL()
}

func BI_Find() {
	obj  := data_stack[data_frame+1]
	pred := data_stack[data_frame+2]

	if obj.kind != TYP_Array {
		Die("find function requires an array, got %s", obj.kind.String())
		return
	}

	if pred.kind != TYP_Function {
		Die("find function requires a function predicate, got %s", pred.kind.String())
		return
	}
	if pred.NumParameters() != 1 {
		Die("find function requires a predicate with one parameter")
		return
	}

	for i := range obj.Array.data {
		child_p := ChildDataFrame()

		data_stack[child_p+1] = obj.GetElem(i)

		// run the predicate!
		if RunClosure(pred.Clos, child_p) != OK {
			return
		}

		// found the wanted element?
		if register_A.IsTrue() {
			register_A.MakeNumber(float64(i))
			return
		}
	}

	// not found
	register_A.MakeNIL()
}

func BI_Sort() {
	obj  := data_stack[data_frame+1]
	pred := data_stack[data_frame+2]

	if obj.kind != TYP_Array {
		Die("sort! function requires an array, got %s", obj.kind.String())
		return
	}

	if pred.kind != TYP_Function {
		Die("sort! function requires a function predicate, got %s", pred.kind.String())
		return
	}
	if pred.NumParameters() != 2 {
		Die("sort! function requires a predicate with two parameters")
		return
	}

	// if empty or a single element, nothing to do
	if obj.Length() < 2 {
		register_A.MakeNIL()
		return
	}

	comparator := func(i, k int) bool {
		// check if an run-time error has occurred
		if run_error {
			return false
		}

		child_p := ChildDataFrame()

		data_stack[child_p+1] = obj.Array.data[i]
		data_stack[child_p+2] = obj.Array.data[k]

		// run the predicate!
		if RunClosure(pred.Clos, child_p) != OK {
			return false
		}

		return register_A.IsTrue()
	}

	// perform a preliminary pass over the array and run the
	// predicate on neighboring pairs.  This is to catch obvious
	// errors early, e.g. array contains wrong type of elements.

	for i := obj.Length() - 2; i >= 0; i-- {
		comparator(i, i+1)

		if run_error {
			return
		}
	}

	safesort_Array(&obj, 0, obj.Length()-1, comparator)

	register_A.MakeNIL()
}

func safesort_Array(arr *Value, s, e int, cmp func(i, k int) bool) {
	// this is a Quick-sort algorithm, but crafted so that the
	// comparison function may return complete garbage and this
	// code will not crash (e.g. access out-of-bound elements).

	// s is start and e is end, inclusive.
	if s > e {
		panic("safesort_Array: s > e")
	}

	safesort_Partition := func(lo, hi, pivot_idx int) int {
		/* this is Hoare's algorithm */

		s := lo
		e := hi

		for {
			for s <= e && cmp(s, pivot_idx) {
				s++
			}

			if s > hi {
				// all values were < pivot, including the pivot itself!

				if pivot_idx != hi {
					arr.SwapElems(pivot_idx, hi)
				}

				return hi - 1
			}

			for e >= s && !cmp(e, pivot_idx) {
				e--
			}

			if e < lo {
				// all values were >= pivot

				if pivot_idx != lo {
					arr.SwapElems(pivot_idx, lo)
				}

				return lo
			}

			if s < e {
				arr.SwapElems(s, e)

				if pivot_idx == s {
					pivot_idx = e
				} else if pivot_idx == e {
					pivot_idx = s
				}

				s++
				e--
				continue
			}

			return s - 1
		}
	}

	for s < e {
		// handle the two element case (trivially)
		if s == e-1 {
			if cmp(e, s) {
				arr.SwapElems(s, e)
			}
			break
		}

		// choosing a pivot in the middle should avoid the worst-case
		// behavior that otherwise occurs when pivot is first or last
		// element and the array is already sorted.
		pivot_idx := (s + e) >> 1

		mid := safesort_Partition(s, e, pivot_idx)

		// handle degenerate cases
		if mid <= s {
			s++
			continue
		}
		if mid+1 >= e {
			e--
			continue
		}

		// only use recursion on the smallest group
		// [ it helps to limit stack usage ]
		if (mid - s) < (e - mid) {
			safesort_Array(arr, s, mid, cmp)
			s = mid + 1
		} else {
			safesort_Array(arr, mid+1, e, cmp)
			e = mid
		}
	}
}

//----------------------------------------------------------------------

func BI_Abs() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("abs function requires a number, got %s", v.kind.String())
		return
	}

	if v.Num < 0 {
		register_A.MakeNumber(- v.Num)
	} else {
		register_A.MakeNumber(v.Num)
	}
}

func BI_Sqrt() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("sqrt function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Sqrt(v.Num))
}

func BI_Round() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("round function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Round(v.Num))
}

func BI_Truncate() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("trunc function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Trunc(v.Num))
}

func BI_Floor() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("floor function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Floor(v.Num))
}

func BI_Ceil() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("ceil function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Ceil(v.Num))
}

func BI_IsZero() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("zero? function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeBool(v.Num == 0)
}

func BI_IsSome() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("some? function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeBool(v.Num != 0)
}

func BI_IsNeg() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("neg? function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeBool(v.Num < 0)
}

func BI_IsPos() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("pos? function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeBool(v.Num > 0)
}

func BI_IsInfinity() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("inf? function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeBool(math.IsInf(v.Num, 0))
}

func BI_IsNan() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("nan? function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeBool(math.IsNaN(v.Num))
}

//----------------------------------------------------------------------

func BI_Log() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("log function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Log(v.Num))
}

func BI_Log10() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("log10 function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Log10(v.Num))
}

func BI_Frexp() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("frexp function requires a number, got %s", v.kind.String())
		return
	}

	frac, exp := math.Frexp(v.Num)

	var v_frac Value ; v_frac.MakeNumber(frac)
	var v_exp  Value ; v_exp .MakeNumber(float64(exp))

	register_A.MakeArray(2)
	register_A.SetElem(0, v_frac)
	register_A.SetElem(1, v_exp)
}

func BI_Ldexp() {
	x := data_stack[data_frame+1]
	y := data_stack[data_frame+2]

	if x.kind != TYP_Number {
		Die("ldexp function requires a number, got %s", x.kind.String())
		return
	}
	if y.kind != TYP_Number {
		Die("ldexp function requires a number, got %s", y.kind.String())
		return
	}

	exp := util_to_int(y.Num)

	register_A.MakeNumber(math.Ldexp(x.Num, int(exp)))
}

func BI_ToDegrees() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("rad->deg function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(v.Num * 180.0 / math.Pi)
}

func BI_ToRadians() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("deg->rad function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(v.Num * math.Pi / 180.0)
}

//----------------------------------------------------------------------

func BI_TrigSin() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("sin function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Sin(v.Num))
}

func BI_TrigCos() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("cos function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Cos(v.Num))
}

func BI_TrigTan() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("tan function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Tan(v.Num))
}

func BI_TrigAsin() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("asin function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Asin(v.Num))
}

func BI_TrigAcos() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("acos function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Acos(v.Num))
}

func BI_TrigAtan() {
	v := data_stack[data_frame+1]

	if v.kind != TYP_Number {
		Die("atan function requires a number, got %s", v.kind.String())
		return
	}
	register_A.MakeNumber(math.Atan(v.Num))
}

func BI_TrigAtan2() {
	y := data_stack[data_frame+1]
	x := data_stack[data_frame+2]

	if y.kind != TYP_Number {
		Die("atan2 function requires a number, got %s", y.kind.String())
		return
	}
	if x.kind != TYP_Number {
		Die("atan2 function requires a number, got %s", x.kind.String())
		return
	}
	register_A.MakeNumber(math.Atan2(y.Num, x.Num))
}

func BI_TrigHypot() {
	x := data_stack[data_frame+1]
	y := data_stack[data_frame+2]

	if x.kind != TYP_Number {
		Die("hypot function requires a number, got %s", x.kind.String())
		return
	}
	if y.kind != TYP_Number {
		Die("hypot function requires a number, got %s", y.kind.String())
		return
	}
	register_A.MakeNumber(math.Hypot(x.Num, y.Num))
}

//----------------------------------------------------------------------

func BI_RefEQ() {
	a := data_stack[data_frame+1]
	b := data_stack[data_frame+2]

	if a.kind != b.kind {
		register_A.MakeBool(false)
		return
	}

	switch a.kind {
	case TYP_Array:
		register_A.MakeBool(a.Array == b.Array)

	case TYP_Map:
		register_A.MakeBool(a.Map == b.Map)

	case TYP_Function:
		register_A.MakeBool(a.Clos == b.Clos)

	default:
		register_A.MakeBool(false)
	}
}

func BI_RefNE() {
	BI_RefEQ()

	register_A.MakeBool(register_A.Num == 0)
}

func BI_TypeNil() {
	v := data_stack[data_frame+1]

	register_A.MakeBool(v.kind == TYP_Void)
}

func BI_TypeBool() {
	v := data_stack[data_frame+1]

	register_A.MakeBool(v.kind == TYP_Bool)
}

func BI_TypeChar() {
	v := data_stack[data_frame+1]

	register_A.MakeBool(v.kind == TYP_Char)
}

func BI_TypeNumber() {
	v := data_stack[data_frame+1]

	register_A.MakeBool(v.kind == TYP_Number)
}

func BI_TypeString() {
	v := data_stack[data_frame+1]

	register_A.MakeBool(v.kind == TYP_String)
}

func BI_TypeArray() {
	v := data_stack[data_frame+1]

	register_A.MakeBool(v.kind == TYP_Array)
}

func BI_TypeMap() {
	v := data_stack[data_frame+1]

	register_A.MakeBool(v.kind == TYP_Map)
}

func BI_TypeFunction() {
	v := data_stack[data_frame+1]

	register_A.MakeBool(v.kind == TYP_Function)
}

func BI_ClassName() {
	v := data_stack[data_frame+1]

	name := ""

	if v.kind == TYP_Map {
		if v.Map.obj_class != nil {
			name = v.Map.obj_class.name
		}
	}

	register_A.MakeString(name)
}

//----------------------------------------------------------------------

func BI_RandSeed() {
	seed := data_stack[data_frame+1]

	if seed.kind != TYP_Number {
		Die("rand-seed function requires a number, got %s", seed.kind.String())
		return
	}

	rand.Seed(util_to_int(seed.Num))

	register_A.MakeNIL()
}

func BI_RandFloat() {
	num := rand.Float64()

	register_A.MakeNumber(num)
}

func BI_RandRange() {
	low  := data_stack[data_frame+1]
	high := data_stack[data_frame+2]

	if low.kind != TYP_Number {
		Die("rand-range function requires a number, got %s", low.kind.String())
		return
	}
	if high.kind != TYP_Number {
		Die("rand-range function requires a number, got %s", high.kind.String())
		return
	}

	r   := rand.Float64()
	num := low.Num + (high.Num - low.Num) * r

	register_A.MakeNumber(num)
}

//----------------------------------------------------------------------

func BI_Error() {
	msg := data_stack[data_frame+1]

	if msg.kind != TYP_String {
		Die("error message must be a string, got %s", msg.kind.String())
		return
	}

	Die("program error: %s", msg.Str)
}

func BI_Print() {
	msg := data_stack[data_frame+1]

	if msg.kind == TYP_String {
		Print_Out("%s\n", msg.Str)
	} else {
		s := msg.DeepString()
		Print_Out("%s\n", s)
	}

	register_A.MakeNIL()
}

func BI_Input() {
	prompt := data_stack[data_frame+1]

	if prompt.kind != TYP_String {
		Die("input prompt must be a string, got %s", prompt.kind.String())
		return
	}

	var s string
	var err error

	if editor != nil {
		// use the line editor
		s, err = editor.Prompt(prompt.Str)

	} else {
		// read directly from Stdin
		var sb strings.Builder

		Print_Out("%s", prompt.Str)

		// with C, we would need a fflush(stdout) here, but with Go
		// it is not needed since os.Stdin is a raw (unbuffered) file.

		for {
			var buf [1]byte
			var n int

			n, err = exec_ctx.stdin.Read(buf[:])

			if n == 0 {
				if err == nil {
					// this should never happen
					err = fmt.Errorf("bad read on Stdin")
					break
				}

				// allow unterminated line before EOF
				if err == io.EOF && sb.Len() > 0 {
					err = nil
				}

				break
			}

			if buf[0] == '\r' || buf[0] == 0 {
				// skip CRs and NULs
				continue
			}

			if buf[0] == '\n' {
				// we have reached end-of-line
				err = nil
				break
			}

			sb.Write(buf[:])
		}

		s = sb.String()
	}

	if err == io.EOF {
		// NOTE: following does not makes sense if Stdin is a pipe
		//// fmt.Fprintf(os.Stderr, "\nEOF\n")
		register_A = *Glob.EOF.loc
		return
	}

	if err == liner.ErrPromptAborted {
		Die("aborted\n")
		return
	}

	// assume other errors are unrecoverable
	if err != nil {
		Die("%s", err.Error())
		return
	}

	// remember history (line editor only)
	if editor != nil {
		editor.AppendHistory(s)
	}

	register_A.MakeString(s)
}

func BI_EnableEditor() {
	EnableEditor()

	register_A.MakeNIL()
}

//----------------------------------------------------------------------

func util_remainder(a, b float64) float64 {
	// this implements a remainder: if N is the numerator and
	// D is the divisor, then compute (N - (N / D) * D) where
	// (N / D) produces an integer by truncating the result.
	//
	// examples:  5.0 %  3.0 =  2.0
	//           -5.0 %  3.0 = -2.0
	//            5.0 % -3.0 =  2.0
	//           -5.0 % -3.0 = -2.0
	//
	// Note: languages vary wildly about how the implement the
	//       floating-point remainder.  The Java and Javascript
	//       '%' operator is the same as here.  The C#, Python
	//       and Lua '%' operators use a floor()ed division.
	//       Scheme (R7RS) nas "truncate-remainder" which acts
	//       the same as here.

	// NOTE! the caller must check for division by zero.

	c := math.Trunc(a / b);

	return a - c * b;
}

func util_modulo(a, b float64) float64 {
	// this implements a modulo which uses the *floor* of the
	// integer division i.e. as if the division was done with
	// infinite precision, then rounded down toward -infinity.
	//
	// examples:  5.0 mod  3.0 =  2.0
	//           -5.0 mod  3.0 =  1.0
	//            5.0 mod -3.0 = -1.0
	//           -5.0 mod -3.0 = -2.0
	//
	// Note: some languages use a different way to compute a
	//       floating point modulo.  The C "fmod" function and
	//       Go's math.Mod() both use truncated division, and
	//       a few other languages follow suit.

	// NOTE! the caller must check for division by zero.

	c := math.Floor(a / b);

	return a - c * b;
}

func util_power(a, b float64) float64 {
	return math.Pow(a, b)
}

func util_to_int(a float64) int64 {
	const limit = 9.223372036854775e+18

	if math.IsNaN(a) {
		return 0
	} else if a > limit {
		return 0x7FFFFFFFFFFFFFFF
	} else if a < -limit {
		return -0x7FFFFFFFFFFFFFFF
	} else {
		// truncate toward zero
		return int64(a)
	}
}

func util_bit_flip(a float64) float64 {
	a = math.Trunc(a)
	return - a - 1
}

func util_bit_and(a, b float64) float64 {
	c := util_to_int(a)
	d := util_to_int(b)
	return float64(c & d)
}

func util_bit_or(a, b float64) float64 {
	c := util_to_int(a)
	d := util_to_int(b)
	return float64(c | d)
}

func util_bit_xor(a, b float64) float64 {
	c := util_to_int(a)
	d := util_to_int(b)
	return float64(c ^ d)
}

func util_shift_left(a, b float64) float64 {
	c := util_to_int(a)
	d := util_to_int(b) & 63

	return float64(c << d)
}

func util_shift_right(a, b float64) float64 {
	c := util_to_int(a)
	d := util_to_int(b) & 63

	return float64(c >> d)
}
