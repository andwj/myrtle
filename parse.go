// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

/* The code in this file is primarily concerned with syntax, it is the
   main place where special syntax is decoded into a simpler node tree
   (the other place is "expr.go" for expressions).
*/

import "fmt"
import "strconv"

// these extra node kinds are the result of parsing the raw tokens
// from the lexer into something the compiler can consume.
const (
	NX_INVALID NodeKind = 50 + iota

	NX_Value      // val is a decoded literal or computed const-expr
	NX_Const      // str is name of global const.  val is value
	NX_Global     // str is name of global var
	NX_Local      // str is name.  lval is a LocalVar struct

	NX_EnvVar     // str is name of environment var
	NX_ArgParm    // str is an integer ("0" and up)
	NX_UserDir    // str is user name, or "" for current one

	NX_Unary      // value.          str is operator name
	NX_Binary     // lhs    | rhs.   str is operator name
	NX_Call       // func   | zero or more parameters
	NX_Method     // method | one or more parameters (first is self)
	NX_Access     // object | one or more indexors

	NX_Lambda     // val holds the template closure
	NX_Matches    // main value | one or more compare values
	NX_Supports   // main value | one or more method names
	NX_Min        // children are the values
	NX_Max        // children are the values

	NS_Return     // child is value (optional)
	NS_Skip       // child is value (optional).  str is label name
	NS_Label      // child is value (optional).  str is label name
	NS_Continue   // condition (optional).  flags: EF_NEGATE
	NS_Break      // condition (optional).  flags: EF_NEGATE

	NS_Let        // var  | value.   var is NX_Local
	NS_If         // cond | true-clause | false-clause (optional)
	NS_Loop       // body | condition (optional).  flags: EF_NEGATE
	NS_For        // var  | begin | end | body | step (optional)
	NS_ForEach    // idx-var | val-var | object | body
	NS_Assign     // var  | value

	ND_Array      // children are the array elements
	ND_Set        // children are the set elements
	ND_Map        // children are ND_MapElem
	ND_MapElem    // key | value
	ND_StrBuild   // children are the strings/chars

	NC_Command    // vars | redirs | cmd | args...
	NC_Backgnd    // child is NC_Command or NC_Pipe
	NC_Pipe       // parent | child.  parent can be another NC_Pipe

	NC_GlobMatch  // children are NW_Glob or string exprs
	NC_ArgGroup   // children are dynamic args (NC_GlobMatch, ND_XXX, NX_XXX)
	NC_Redir      // child is target.  str is symbol (like ">>")

	NC_Ok         // the pseudo-constant `Ok`
	NC_Failed     // the pseudo-constant `Failed`
	NC_ArgNum     // the pseudo-constant `ArgNum`
)

// extra node flags
const (
	EF_NEGATE     = (1 << 0)  // NS_Loop, NS_Continue, NS_Break
	EF_HAS_GLOB   = (1 << 3)  // NC_ArgGroup
)

type ParsingCtx struct {
	// this is nil if outside a function (e.g. global vars)
	fu *Closure

	// the result of a successful parse.  if the expression was a const
	// expression (regardless of `fu` above) this will be a NX_Value node.
	out *Node

	// the current scope for local vars
	scope *Scope

	// used when creating nodes in EV_XXX functions
	ev_pos Position
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) ParseBlock(t *Node, label string) cmError {
	err_count := 0

	nd := NewNode(NG_Block, "", t.pos)

	nd.scope = ctx.PushScope()
	nd.scope.end_label = label

	for idx, line := range t.children {
		is_last := (idx == t.Len()-1)

		if ctx.ParseLine(line, is_last) != OK {
			err_count += 1
			continue
		}

		nd.Add(ctx.out)
	}

	ctx.PopScope()

	if err_count > 0 {
		return FAILED
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseLine(t *Node, is_last bool) cmError {
	Error_SetPos(t.pos)

	if t.kind != NG_Line {
		panic("weird node for ParseLine")
	} else if t.Len() == 0 {
		panic("empty line for ParseLine")
	}

	head := t.children[0]

	if head.kind == NG_Block && t.Len() == 1 {
		return ctx.ParseBlock(head, "")
	}

	// handle a command to execute, or a form containing one
	if head.Match("background") {
		return ctx.ParseBackground(t.children)
	}
	if ctx.CheckCommand(t) {
		return ctx.ParsePipeline(t.children)
	}

	// this is definitely a code line, so reconstitute the tokens
	for _, child := range t.children {
		child.Unglob()
	}

	// handle special statements....
	switch {
	case head.Match("let"):
		return ctx.ParseLet(t, is_last)

	case head.Match("return"):
		return ctx.ParseReturn(t)

	case head.Match("skip"):
		return ctx.ParseSkip(t)

	case head.Match("label"):
		return ctx.ParseLabel(t, is_last)

	case head.Match("continue"):
		return ctx.ParseContinueOrBreak(t, NS_Continue)

	case head.Match("break"):
		return ctx.ParseContinueOrBreak(t, NS_Break)

	case head.Match("do"):
		return ctx.ParseDo(t)

	case head.Match("if"):
		return ctx.ParseIf(t)

	case head.Match("loop"):
		return ctx.ParseLoop(t)

	case head.Match("for"):
		return ctx.ParseFor(t)
	}

	// handle an assignment
	if head.Match("=") {
		PostError("missing destination before '='")
		return FAILED
	}
	if t.Len() >= 2 {
		second := t.children[1]
		if second.kind == NL_DirSym && second.Match("=") {
			return ctx.ParseAssign(t)
		}
	}

	// handle an expression on a line by itself
	if ctx.ParseComputation(t.children, "statement") != OK {
		return FAILED
	}

	if ctx.out.IsValue() && !is_last {
		PostError("found a value wandering around by itself")
		return FAILED
	}
	return OK
}

func (ctx *ParsingCtx) ParseReturn(t *Node) cmError {
	nd := NewNode(NS_Return, "", t.pos)

	if t.Len() > 1 {
		if ctx.ParseComputation(t.children[1:], "return") != OK {
			return FAILED
		}
		nd.Add(ctx.out)
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseDo(t *Node) cmError {
	if t.Len() < 2 {
		PostError("missing block after do")
		return FAILED
	}
	if t.Len() > 2 {
		PostError("too many elements for do block")
		return FAILED
	}

	t_block := t.children[1]

	if t_block.kind != NG_Block {
		PostError("expected block after do, got: %s", t_block.String())
		return FAILED
	}

	return ctx.ParseBlock(t_block, "")
}

func (ctx *ParsingCtx) ParseSkip(t *Node) cmError {
	if t.Len() < 2 {
		PostError("missing label name for skip")
		return FAILED
	}

	t_lab := t.children[1]
	name  := t_lab.str

	if ValidateName(t_lab, "label", ALLOW_SHADOW) != OK {
		return FAILED
	}

	nd := NewNode(NS_Skip, name, t.pos)

	if t.Len() > 2 {
		if ctx.ParseComputation(t.children[2:], "skip") != OK {
			return FAILED
		}
		nd.Add(ctx.out)
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseLabel(t *Node, is_last bool) cmError {
	if !is_last {
		PostError("a label must be the last thing in a block")
		return FAILED
	}
	if ctx.scope.end_label != "" {
		PostError("cannot use a label at end of loop block")
		return FAILED
	}

	if t.Len() < 2 {
		PostError("missing label name")
		return FAILED
	}

	t_lab := t.children[1]
	name  := t_lab.str

	if ValidateName(t_lab, "label", ALLOW_SHADOW) != OK {
		return FAILED
	}
	if ctx.scope.HasLocal(name) {
		PostError("label name is same as a local: %s", name)
		return FAILED
	}

	nd := NewNode(NS_Label, name, t.pos)

	if t.Len() > 2 {
		if ctx.ParseComputation(t.children[2:], "label") != OK {
			return FAILED
		}
		nd.Add(ctx.out)
	}

	ctx.scope.end_label = name

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseContinueOrBreak(t *Node, kind NodeKind) cmError {
	what := t.children[0].str

	nd := NewNode(kind, "", t.pos)
	negate := false

	// support a condition after the keyword
	if t.Len() > 1 {
		t_word   := t.children[1]
		children := t.children[2:]

		if t_word.Match("if") {
			// ok
		} else if t_word.Match("unless") {
			negate = true
		} else {
			PostError("expected if/unless after %s, got: %s", what, t_word.String())
			return FAILED
		}

		if len(children) == 0 {
			PostError("missing expression after %s %s", what, t_word.str)
			return FAILED
		}
		if ctx.ParseComputation(children, what) != OK {
			return FAILED
		}

		nd.Add(ctx.out)
	}

	if negate {
		nd.flags = EF_NEGATE
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseLet(t *Node, is_last bool) cmError {
	nd := NewNode(NS_Let, "", t.pos)

	children := t.children[1:]

	if len(children) < 1 {
		PostError("missing var name")
		return FAILED
	}

	t_var := children[0]
	name  := t_var.str

	if ValidateName(t_var, "local", ALLOW_SHADOW) != OK {
		return FAILED
	}

	// by policy, we disallow re-binding the same name in the same scope.
	// that is consistent with C/Go/Java and even Haskell, but not Rust.

	if ctx.scope.HasLocal(name) {
		PostError("duplicate var name '%s'", name)
		return FAILED
	}

	// check for a value
	if len(children) < 2 {
		PostError("missing '=' after var name")
		return FAILED
	}
	if !children[1].Match("=") {
		PostError("missing '=' after var name")
		return FAILED
	}
	if len(children) < 3 {
		PostError("missing value for local var")
		return FAILED
	}

	// parse the value.
	// must create the local var *after* parsing the value.
	// to inhibit unhelpful errors in the future, we create the local
	// even if the computation fails to parse.

	result := ctx.ParseComputation(children[2:], "local var")
	lvar   := ctx.AddLocal(t_var.str, true)  // mutable

	if result == OK {
		t_var.kind = NX_Local
		t_var.lvar = lvar

		nd.Add(t_var)
		nd.Add(ctx.out)

		ctx.out = nd
	}
	return result
}

func (ctx *ParsingCtx) ParseIf(t *Node) cmError {
	nd := NewNode(NS_If, "", t.pos)

	children := t.children[1:]

	// find the `else` clause (possibly an `else if` clause)
	else_p := -1

	for i, sub := range children {
		if sub.Match("else") {
			else_p = i
			break
		}
	}

	var t_else *Node

	if else_p >= 0 {
		if else_p+1 >= len(children) {
			PostError("missing block after else")
			return FAILED
		}
		t_next := children[else_p+1]

		if t_next.kind == NG_Block {
			if else_p+2 < len(children) {
				PostError("extra rubbish after else clause")
				return FAILED
			}
			t_else = t_next

		} else if t_next.Match("if") {
			t_else = NewNode(NG_Line, "", t.pos)

			for k := else_p+1; k < len(children); k++ {
				t_else.Add(children[k])
			}

		} else {
			PostError("expected block or 'if' after else, got: %s", t_next.String())
			return FAILED
		}

		children = children[0:else_p]
	}

	// handle block for `if` clause
	if len(children) < 1 {
		PostError("missing condition in if statement")
		return FAILED
	}
	if len(children) < 2 {
		PostError("missing block in if statement")
		return FAILED
	}

	t_block := children[  len(children)-1]
	children = children[0:len(children)-1]

	if t_block.kind != NG_Block {
		PostError("expected block in if statement, got: %s", t_block.String())
		return FAILED
	}

	// handle the condition
	if ctx.ParseComputation(children, "if") != OK {
		return FAILED
	}
	nd.Add(ctx.out)

	// parse the `if` body
	if ctx.ParseBlock(t_block, "") != OK {
		return FAILED
	}
	nd.Add(ctx.out)

	// parse the `else` body (if any)
	if t_else != nil {
		if t_else.kind == NG_Block {
			if ctx.ParseBlock(t_else, "") != OK {
				return FAILED
			}
		} else {
			if ctx.ParseIf(t_else) != OK {
				return FAILED
			}
		}
		nd.Add(ctx.out)
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseLoop(t *Node) cmError {
	out_block := ctx.BlockAroundLoop(t.pos)

	if t.Len() < 2 {
		PostError("missing block for loop")
		return FAILED
	}

	last   := t.Len() - 1
	t_body := t.children[last]

	if t_body.kind != NG_Block {
		PostError("expected block for loop body, got: %s", t_body.String())
		return FAILED
	}

	// support a condition after the `loop` keyword
	var t_cond *Node
	negate := false

	if last > 1 {
		keyword := t.children[1]

		if keyword.Match("while") {
			// ok
		} else if keyword.Match("until") {
			// ok
			negate = true
		} else {
			PostError("expected while/until, got: %s", keyword.String())
			return FAILED
		}

		children := t.children[2:last]
		if len(children) == 0 {
			PostError("missing expression after %s", keyword.str)
			return FAILED
		}

		// parse the condition
		if ctx.ParseComputation(children, "loop") != OK {
			return FAILED
		}
		t_cond = ctx.out
	}

	// parse the body
	if ctx.ParseBlock(t_body, "NEXT") != OK {
		return FAILED
	}

	loop_nd := NewNode(NS_Loop, "", t.pos)
	loop_nd.Add(ctx.out)

	if negate {
		loop_nd.flags = EF_NEGATE
	}

	if t_cond != nil {
		loop_nd.Add(t_cond)
	}

	out_block.Add(loop_nd)

	// pop the scope created in BlockAroundLoop
	ctx.PopScope()

	ctx.out = out_block
	return OK
}

func (ctx *ParsingCtx) ParseFor(t *Node) cmError {
	if t.Len() >= 3 {
		if t.children[2].Match("=") {
			return ctx.ParseForRange(t)
		}
	}
	if t.Len() >= 4 {
		if t.children[3].Match("=") {
			return ctx.ParseForEach(t)
		}
	}

	if t.Len() < 2 {
		PostError("missing variable in for statement")
	} else {
		PostError("bad syntax in for statement")
	}
	return FAILED
}

func (ctx *ParsingCtx) ParseForRange(t *Node) cmError {
	// syntax is: `for VAR = start end [step] { .... }`

	out_block := ctx.BlockAroundLoop(t.pos)

	children := t.children[1:]
	t_var    := children[0]

	if len(children) < 2 {
		PostError("missing '=' in for statement")
		return FAILED
	}
	if len(children) < 5 {
		PostError("missing start/end in for statement")
		return FAILED
	}

	if ValidateName(t_var, "var", ALLOW_SHADOW | ALLOW_UNDERSCORE) != OK {
		return FAILED
	}

	// we need three contiguous stack slots:
	//    +0 = the variable, holding current position
	//    +1 = the end/finish/target
	//    +2 = the step value

	t_var.kind = NX_Local
	t_var.lvar = ctx.AddLocal(t_var.str, false)

	_ = ctx.AddLocal("_", false)
	_ = ctx.AddLocal("_", false)

	// grab the body of the loop
	if len(children) < 5 {
		PostError("missing block in for statement")
		return FAILED
	}
	if len(children) > 6 {
		PostError("too many values in for statement")
		return FAILED
	}

	t_block := children[len(children)-1]
	children = children[0:len(children)-1]

	if t_block.kind != NG_Block {
		PostError("expected block in for statement, got: %s", t_block.String())
		return FAILED
	}

	loop_nd := NewNode(NS_For, "", t.pos)
	loop_nd.Add(t_var)

	// the start/end range
	if ctx.ParseTerm(children[2]) != OK {
		return FAILED
	}
	loop_nd.Add(ctx.out)

	if ctx.ParseTerm(children[3]) != OK {
		return FAILED
	}
	loop_nd.Add(ctx.out)

	// the optional step
	var t_step *Node

	if len(children) >= 5 {
		if ctx.ParseTerm(children[4]) != OK {
			return FAILED
		}
		t_step = ctx.out
	}

	// parse the body
	if ctx.ParseBlock(t_block, "NEXT") != OK {
		return FAILED
	}
	loop_nd.Add(ctx.out)

	if t_step != nil {
		loop_nd.Add(t_step)
	}

	out_block.Add(loop_nd)

	// pop the scope created in BlockAroundLoop
	ctx.PopScope()

	ctx.out = out_block
	return OK
}

func (ctx *ParsingCtx) ParseForEach(t *Node) cmError {
	// this will create a scope for the two variables
	out_block := ctx.BlockAroundLoop(t.pos)

	children := t.children[1:]

	if len(children) < 2 {
		PostError("missing variables in for-each")
		return FAILED
	}

	// the variables names
	t_idx := children[0]
	t_var := children[1]

	if ValidateName(t_idx, "var", ALLOW_SHADOW | ALLOW_UNDERSCORE) != OK {
		return FAILED
	}
	if ValidateName(t_var, "var", ALLOW_SHADOW | ALLOW_UNDERSCORE) != OK {
		return FAILED
	}
	if t_idx.str != "_" {
		if t_idx.str == t_var.str {
			PostError("duplicate var name '%s'", t_idx.str)
			return FAILED
		}
	}

	// we need four contiguous stack slots:
	//    +0 = index variable
	//    +1 = value variable
	//    +2 = work slot 1 (usually the array or map)
	//    +3 = work slot 2

	t_idx.kind = NX_Local
	t_idx.lvar = ctx.AddLocal(t_idx.str, false)

	t_var.kind = NX_Local
	t_var.lvar = ctx.AddLocal(t_var.str, false)

	_ = ctx.AddLocal("_", false)
	_ = ctx.AddLocal("_", false)

	// the code block
	if len(children) < 4 {
		PostError("missing block in for-each")
		return FAILED
	}
	if len(children) < 5 {
		PostError("missing object after '=' in for-each")
		return FAILED
	}

	t_block := children[len(children)-1]
	children = children[0:len(children)-1]

	if t_block.kind != NG_Block {
		PostError("expected block in for-each, got: %s", t_block.String())
		return FAILED
	}

	loop_nd := NewNode(NS_ForEach, "", t.pos)

	loop_nd.Add(t_idx)
	loop_nd.Add(t_var)

	// the object to iterate over
	if ctx.ParseComputation(children[3:], "for-each") != OK {
		return FAILED
	}
	loop_nd.Add(ctx.out)

	// parse the body
	if ctx.ParseBlock(t_block, "NEXT") != OK {
		return FAILED
	}
	loop_nd.Add(ctx.out)

	out_block.Add(loop_nd)

	// pop the scope created in BlockAroundLoop
	ctx.PopScope()

	ctx.out = out_block
	return OK
}

func (ctx *ParsingCtx) BlockAroundLoop(pos Position) *Node {
	nd := NewNode(NG_Block, "", pos)

	nd.scope = ctx.PushScope()
	nd.scope.end_label = "OUT"

	return nd
}

func (ctx *ParsingCtx) ParseAssign(t *Node) cmError {
	nd := NewNode(NS_Assign, "", t.pos)

	if t.Len() < 3 {
		PostError("missing value in assignment")
		return FAILED
	}

	t_dest := t.children[0]

	if t_dest.kind == NX_EnvVar {
		goto okay
	}

	// rule out things like `$(...)` and `$3`
	if t_dest.kind == NG_Word || t_dest.kind == NX_ArgParm {
		PostError("bad destination for assignment")
		return FAILED
	}

	if ctx.ParseTerm(t_dest) != OK {
		return FAILED
	}
	t_dest = ctx.out

	switch t_dest.kind {
	case NX_Local:
		if !t_dest.lvar.mutable {
			PostError("cannot assign to immutable local var '%s'", t_dest.str)
			return FAILED
		}

	case NX_Global:
		// ok

	case NX_Const:
		PostError("cannot assign to global constant '%s'", t_dest.str)
		return FAILED

	case NC_Ok:
		PostError("cannot assign to pseudo-constant 'Ok'")
		return FAILED

	case NC_Failed:
		PostError("cannot assign to pseudo-constant 'Failed'")
		return FAILED

	case NC_ArgNum:
		PostError("cannot assign to pseudo-constant 'ArgNum'")
		return FAILED

	case NX_Value:
		PostError("cannot assign to a literal value")
		return FAILED

	case NX_Access:
		// ok

	case ND_Array, ND_Map, ND_Set, ND_StrBuild /* ND_FmtString */:
		PostError("expected access in [], got a data structure")
		return FAILED

	default:
		PostError("bad destination in assignment")
		return FAILED
	}

okay:
	// handle the value
	if ctx.ParseComputation(t.children[2:], "assignment") != OK {
		return FAILED
	}

	nd.Add(t_dest)
	nd.Add(ctx.out)

	ctx.out = nd
	return OK
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) ParseComputation(children []*Node, what string) cmError {
	// currently there is nothing different between a "computation"
	// and an expression in `()` parentheses.

	return ctx.OperatorExpand(children)
}

func (ctx *ParsingCtx) ParseTerm(t *Node) cmError {
	Error_SetPos(t.pos)

	switch t.kind {
	case NL_Name:
		return ctx.ParseName(t)

	case NL_Char:
		// this should never fail
		ch, _ := strconv.ParseInt(t.str, 10, 32)
		ctx.out = NewNode(NX_Value, "", t.pos)
		ctx.out.val = new(Value)
		ctx.out.val.MakeChar(rune(ch))
		return OK

	case NG_Expr:
		if t.Len() == 0 {
			PostError("missing expression in ()")
			return FAILED
		}
		// reconstitute all tokens inside the `()`
		for _, child := range t.children {
			child.Unglob()
		}
		return ctx.OperatorExpand(t.children)

	case NG_Data:
		return ctx.ParseDataOrAccess(t)

	case NG_Block:
		return ctx.ParseBlock(t, "")

	case NX_Value, NX_EnvVar, NX_ArgParm:
		ctx.out = t
		return OK

	case NG_Word:
		return ctx.ParseCodeWord(t)

	default:
		PostError("bad term in expression, got: %s", t.String())
		return FAILED
	}
}

func (ctx *ParsingCtx) ParseName(t *Node) cmError {
	// the lexer produces numbers as a NL_Name, which we must decode here.
	// [ we cannot do it in the lexer since they may be filenames ]
	if LEX_Number(t.str) {
		return ctx.ParseNumber(t)
	}

	// handle some hard-coded constants
	switch t.str {
	case "Ok":
		ctx.out = NewNode(NC_Ok, "", t.pos)
		return OK

	case "Failed":
		ctx.out = NewNode(NC_Failed, "", t.pos)
		return OK

	case "ArgNum":
		ctx.out = NewNode(NC_ArgNum, "", t.pos)
		return OK

	case "NIL":
		ctx.out = NewNode(NX_Value, "", t.pos)
		ctx.out.val = new(Value)
		ctx.out.val.MakeNIL()
		return OK

	case "FALSE":
		ctx.out = NewNode(NX_Value, "", t.pos)
		ctx.out.val = new(Value)
		ctx.out.val.MakeBool(false)
		return OK

	case "TRUE":
		ctx.out = NewNode(NX_Value, "", t.pos)
		ctx.out.val = new(Value)
		ctx.out.val.MakeBool(true)
		return OK

	case "+INF", "-INF", "NAN":
		ctx.out = NewNode(NX_Value, "", t.pos)
		ctx.out.val = new(Value)
		ctx.out.val.DecodeFloatSpec(t.str)
		return OK
	}

	if t.IsField() {
		PostError("unexpected field name: %s", t.str)
		return FAILED
	}
	if t.IsMethod() {
		PostError("unexpected method name: %s", t.str)
		return FAILED
	}

	// local variables always shadow a global
	if ctx.fu != nil {
		lvar := ctx.LookupLocal(t.str)

		if lvar != nil {
			// ensure locals in a parent closure are external
			if lvar.mutable && lvar.owner != ctx.fu {
				lvar.external = true
			}

			t.kind = NX_Local
			t.lvar = lvar

			ctx.out = t
			return OK
		}
	}

	def := LookupGlobal(t.str)

	if def == nil {
		if IsLanguageKeyword(t.str) {
			PostError("expected binding name, got keyword: %s", t.str)
			return FAILED
		}

		PostError("unknown binding: %s", t.str)
		return FAILED
	}

	if def.mutable {
		t.kind  = NX_Global
		ctx.out = t
		return OK
	}

	// an unevaluated constant?
	if def.expr != nil {
		return HIT_UNEVAL
	}

	t.kind = NX_Const
	t.val  = new(Value)
	*t.val = *def.loc

	ctx.out = t
	return OK
}

func (ctx *ParsingCtx) ParseAccess(parsed_head *Node, indexors []*Node) cmError {
	nd := NewNode(NX_Access, "", parsed_head.pos)

	nd.Add(parsed_head)

	if len(indexors) == 0 {
		PostError("missing indexors in []")
		return FAILED
	}

	for _, t_key := range indexors {
		if t_key.IsField() {
			t_str := NewNode(NX_Value, "", t_key.pos)
			t_str.val = new(Value)
			t_str.val.MakeString(t_key.str)

			nd.Add(t_str)

		} else {
			if ctx.ParseTerm(t_key) != OK {
				return FAILED
			}
			nd.Add(ctx.out)
		}
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseForm(children []*Node) cmError {
	// this is either a function or method call, or a special form
	// which resembles such.

	head := children[0]

	Error_SetPos(head.pos)

	if head.Match("call") {
		return ctx.ParseExplicitCall(children)
	}
	if head.Match("lam") {
		return ctx.ParseLambda(children)
	}
	if head.Match("supports?") {
		return ctx.ParseSupports(children)
	}
	if head.Match("matches?") {
		return ctx.ParseGenericForm(children, NX_Matches, 2)
	}
	if head.Match("max") {
		return ctx.ParseGenericForm(children, NX_Max, 2)
	}
	if head.Match("min") {
		return ctx.ParseGenericForm(children, NX_Min, 2)
	}

	// anything else is an implicit function call...

	if head.kind == NG_Word {
		if ctx.ParseCodeWord(head) != OK {
			return FAILED
		}
		head = ctx.out
	}

	if head.kind != NL_Name || LEX_Number(head.str) {
		PostError("bad function call, func must be a name, got %s", head.String())
		return FAILED
	}
	return ctx.ParseFunCall(head, children[1:])
}

func (ctx *ParsingCtx) ParseGenericForm(children []*Node, kind NodeKind, min_args int) cmError {
	nd := NewNode(kind, "", children[0].pos)

	if len(children[1:]) < min_args {
		PostError("not enough arguments to %s, wanted %d, got %d",
			children[0].str, min_args, len(children))
		return FAILED
	}

	for _, t_arg := range children[1:] {
		if ctx.ParseTerm(t_arg) != OK {
			return FAILED
		}
		nd.Add(ctx.out)
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseLambda(children []*Node) cmError {
	if len(children) < 2 {
		PostError("missing parameter list after lam")
		return FAILED
	}
	if len(children) < 3 {
		PostError("missing block for lam body")
		return FAILED
	}

	t_pars  := children[1]
	t_block := children[2]

	if t_block.kind == NG_Block {
		if len(children) > 3 {
			PostError("extra rubbish after lam body")
			return FAILED
		}
	} else {
		// allow a "naked" body, remaining tokens on the line become
		// an implicit block.  For example: (lam (x y) x < y)

		t_line  := NewNode(NG_Line, "",  t_block.pos)
		t_block  = NewNode(NG_Block, "", t_block.pos)

		for _, t2 := range children[2:] {
			t_line.Add(t2)
		}
		t_block.Add(t_line)
	}

	// compile code into a template closure
	cl := ctx.fu
	cl.num_lambdas += 1

	name     := fmt.Sprintf("%s:lam%d", cl.debug_name, cl.num_lambdas)
	template := NewClosure(name)

	template.parent = cl
	template.is_function = true
	template.uncompiled_pars = t_pars
	template.uncompiled_body = t_block

	if ParseParameters(t_pars, template) != OK {
		return FAILED
	}
	if CompileClosure(template, ctx.scope) != OK {
		return FAILED
	}

	nd := NewNode(NX_Lambda, "", t_pars.pos)

	nd.val = new(Value)
	nd.val.MakeFunction(template)

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseSupports(children []*Node) cmError {
	if len(children) < 2 {
		PostError("missing object for supports?")
		return FAILED
	}
	if len(children) < 3 {
		PostError("missing method names for supports?")
		return FAILED
	}

	nd := NewNode(NX_Supports, "", children[0].pos)

	// grab the object
	if ctx.ParseTerm(children[1]) != OK {
		return FAILED
	}
	nd.Add(ctx.out)

	// grab the method names
	for _, t_meth := range children[2:] {
		if !t_meth.IsMethod() {
			PostError("expected method name, got: %s", t_meth.String())
			return FAILED
		}
		nd.Add(t_meth)
	}

	ctx.out = nd
	return OK
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) ParseExplicitCall(children []*Node) cmError {
	if len(children) < 2 {
		PostError("missing function for call statement")
		return FAILED
	}

	t_func := children[1]

	return ctx.ParseFunCall(t_func, children[2:])
}

func (ctx *ParsingCtx) ParseFunCall(t_func *Node, params []*Node) cmError {
	is_method := t_func.IsMethod()

	var nd *Node

	if is_method {
		nd = NewNode(NX_Method, "", t_func.pos)
	} else {
		nd = NewNode(NX_Call, "", t_func.pos)

		if ctx.ParseTerm(t_func) != OK {
			return FAILED
		}
		t_func = ctx.out
	}

	nd.Add(t_func)

	// skip a single parameter which is solely a dot
	if len(params) == 1 {
		if params[0].Match(".") {
			params = params[0:0]
		}
	}

	if is_method && len(params) == 0 {
		PostError("missing receiver for method call")
		return FAILED
	}

	// for global funcs, check number of parameters
	if t_func.kind == NX_Const {
		def := LookupGlobal(t_func.str)

		if def.loc.kind != TYP_Function {
			PostError("cannot call %s as a function", def.name)
			return FAILED
		}

		cl := def.loc.Clos
		wanted := -1

		if cl.builtin != nil {
			wanted = cl.builtin.args
		} else {
			wanted = len(cl.parameters)
		}

		if len(params) != wanted {
			PostError("wrong number of params to %s: wanted %d, got %d",
				cl.debug_name, wanted, len(params))
			return FAILED
		}
	}

	// grab the parameters
	for _, t_par := range params {
		if ctx.ParseTerm(t_par) != OK {
			return FAILED
		}
		nd.Add(ctx.out)
	}

	ctx.out = nd
	return OK
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) ParseDataOrAccess(t *Node) cmError {
	children := t.children

	if len(children) == 0 {
		// an empty `[]` is always an array
		return ctx.ParseArray(t, children, nil)
	}

	// reconstitute all tokens inside the `[]`
	for _, child := range children {
		child.Unglob()
	}

	head := children[0]

	if head.Match("array") {
		return ctx.ParseArray(t, children[1:], nil)
	}
	if head.Match("map") {
		return ctx.ParseMap(t, children[1:], nil)
	}
	if head.Match("set") {
		return ctx.ParseSet(t, children[1:])
	}
	if head.Match("$") {
		return ctx.ParseStrBuilder(t)
	}
	if head.Match("fmt$") {
		return ctx.ParseFmtString(t)
	}

	if head.IsField() {
		return ctx.ParseMap(t, children, nil)
	}

	// if head is a known user type, ParseMap
	if head.kind == NL_Name {
		tdef := LookupType(head.str)
		if tdef != nil {
			return ctx.ParseMap(t, children[1:], tdef)
		}
	}

	// outside of a function, stuff in `[]` is always data
	if ctx.fu == nil {
		return ctx.ParseArray(t, children, nil)
	}

	// determine if first element is a constant.
	// unfortunately that means parsing it, which complicates things...

	err := ctx.ParseTerm(head)
	if err != OK {
		return err
	}

	parsed_head := ctx.out
	if parsed_head.IsValue() {
		return ctx.ParseArray(t, children[1:], parsed_head)
	}

	// it must be an access
	return ctx.ParseAccess(parsed_head, children[1:])
}

func (ctx *ParsingCtx) ParseArray(t *Node, children []*Node, parsed_head *Node) cmError {
	nd := NewNode(ND_Array, "", t.pos)

	if parsed_head != nil {
		nd.Add(parsed_head)
	}

	for _, t_elem := range children {
		err := ctx.ParseTerm(t_elem)
		if err != OK {
			return err
		}
		nd.Add(ctx.out)
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseSet(t *Node, children []*Node) cmError {
	nd := NewNode(ND_Set, "", t.pos)

	// handle each element in turn...
	for _, t_elem := range children {
		err := ctx.ParseTerm(t_elem)
		if err != OK {
			return err
		}
		nd.Add(ctx.out)
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseMap(t *Node, children []*Node, tdef *TypeDef) cmError {
	nd := NewNode(ND_Map, "", t.pos)
	nd.tdef = tdef

	if len(children) % 2 != 0 {
		PostError("bad map constructor: odd number of elements")
		return FAILED
	}

	// visit each element in turn...
	for i := 0; i < len(children); i += 2 {
		t_key := children[i]
		t_val := children[i+1]

		pair := NewNode(ND_MapElem, "", t_key.pos)

		// allow field names like `.thing`
		if t_key.IsField() {
			t_str := NewNode(NX_Value, "", t_key.pos)
			t_str.val = new(Value)
			t_str.val.MakeString(t_key.str)

			t_key = t_str
		}

		err := ctx.ParseTerm(t_key)
		if err != OK {
			return err
		}
		pair.Add(ctx.out)

		err = ctx.ParseTerm(t_val)
		if err != OK {
			return err
		}
		pair.Add(ctx.out)

		nd.Add(pair)
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseStrBuilder(t *Node) cmError {
	nd := NewNode(ND_StrBuild, "", t.pos)

	append_literal := func(s string) {
		if s == "" {
			return
		}

		// optimize for multiple literals in a row
		if nd.Len() > 0 {
			last := nd.Last()

			if last.IsValue() {
				if last.val.kind == TYP_String {
					v_string := new(Value)
					v_string.MakeString(last.val.Str + s)
					last.val = v_string
					return
				}
			}
		}

		v_string := new(Value)
		v_string.MakeString(s)

		t_str := NewNode(NX_Value, "", nd.pos)
		t_str.val = v_string

		nd.Add(t_str)
	}

	process_child := func(child *Node) {
		// handle string and character literals
		if child.IsValue() {
			switch child.val.kind {
			case TYP_String:
				append_literal(child.val.Str)
				return

			case TYP_Char:
				ch := rune(child.val.Num)
				append_literal(string(ch))
				return

			case TYP_Void, TYP_Bool, TYP_Number, TYP_Function:
				append_literal(child.val.DeepString())
				return
			}
		}

		nd.Add(child)
	}

	for _, t_elem := range t.children[1:] {
		if t_elem.Match("|") {
			append_literal(" ")
			continue
		}

		err := ctx.ParseTerm(t_elem)
		if err != OK {
			return err
		}
		parsed_elem := ctx.out

		// disallow global vars in a non-function context
		if ctx.fu == nil && parsed_elem.kind == NX_Global {
			PostError("cannot stringify global '%s' outside of a function", parsed_elem.str)
			return FAILED
		}

		// handle an embedded string-builder (flatten the tree)
		if parsed_elem.kind == ND_StrBuild {
			for _, sub_elem := range parsed_elem.children {
				process_child(sub_elem)
			}
		} else {
			process_child(parsed_elem)
		}
	}

	// an empty builder is allowed, it becomes the empty string
	if nd.Len() == 0 {
		v_string := new(Value)
		v_string.MakeString("")

		t_empty := NewNode(NX_Value, "", nd.pos)
		t_empty.val = v_string

		ctx.out = t_empty
		return OK
	}

	// simplify pure strings to a literal node
	if nd.Len() == 1 {
		last := nd.children[0]

		if last.IsValue() {
			if last.val.kind == TYP_String {
				ctx.out = last
				return OK
			}
		}
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseFmtString(t *Node) cmError {
	if ctx.fu == nil {
		PostError("cannot use fmt$ outside of a function")
		return FAILED
	}

	PostError("fmt$ NYI")
	return FAILED
}

//----------------------------------------------------------------------

func ParseMethodInParams(t *Node) *TypeDef {
	if t.kind != NG_Expr {
		PostError("expected parameters in (), got: %s", t.String())
		return nil
	}

	if t.Len() == 0 {
		PostError("bad method def: missing self parameter")
		return nil
	}
	if t.Len() < 2 {
		PostError("bad method def: missing class name")
		return nil
	}

	t_class := t.children[1]

	// find the type definition
	name := t_class.str
	tdef := LookupType(name)

	if tdef == nil {
		PostError("bad method def: no such class: %s", name)
		return nil
	}

	// remove the class name from the parameter list
	for k := 2; k < t.Len(); k++ {
		t.children[k-1] = t.children[k]
	}
	t.children = t.children[0:len(t.children)-1]

	return tdef
}

func ParseParameters(t *Node, cl *Closure) cmError {
	if t.kind != NG_Expr {
		PostError("expected parameters in (), got: %s", t.String())
		return FAILED
	}

	for _, p := range t.children {
		p.Unglob()

		if p.kind != NL_Name {
			PostError("bad parameter name: %s", p.String())
			return FAILED
		}

		if p.str == "_" {
			// ok
			continue
		}

		if ValidateName(p, "parameter", ALLOW_SHADOW) != OK {
			return FAILED
		}

		for _, p2 := range t.children {
			if p2 != p && p2.str == p.str {
				PostError("duplicate parameter name '%s'", p.str)
				return FAILED
			}
		}

		cl.parameters = append(cl.parameters, p.str)
	}

	return OK
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) CheckCommand(t *Node) bool {
	// determine if a NG_Line is a command to execute, or a normal
	// language construct.

	head := t.children[0]

	if head.Match("run") || head.Match("with") {
		return true
	}

	// glob patterns cannot be a command name, but might be a function
	// name once reconstituted...
	if head.kind == NG_Word {
		return ! head.HasGlob()
	}

	if head.kind != NL_Name {
		return false
	}
	if LEX_Number(head.str) {
		return false
	}

	// an assignment?
	if t.Len() >= 2 {
		if t.children[1].Match("=") {
			return false
		}
	}

	name := head.str

	if IsLanguageKeyword(name) {
		return false
	}
	if IsOperatorName(name) {
		return false
	}
	if head.IsMethod() {
		return false
	}

	// a local binding?
	lvar := ctx.LookupLocal(name)
	if lvar != nil {
		return false
	}

	// a defined global?  includes builtin funcs
	_, g_exist := global_lookup[name]
	if g_exist {
		return false
	}

	// anything else will be a command
	return true
}

func (ctx *ParsingCtx) ParseBackground(children []*Node) cmError {
	children = children[1:]

	if len(children) == 0 {
		PostError("missing command after background")
		return FAILED
	}

	if ctx.ParsePipeline(children) != OK {
		return FAILED
	}

	if ctx.out.kind == NC_Pipe {
		PostError("backgrounding a pipeline is not implemented")
		return FAILED
	}

	nd := NewNode(NC_Backgnd, "", children[0].pos)
	nd.Add(ctx.out)

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParsePipeline(children []*Node) cmError {
	// look for the last pipe symbol
	pos := -1
	symbol := ""

	for i := len(children)-1; i >= 0; i-- {
		t := children[i]

		if t.kind == NL_DirSym && t.str[0] == '|' {
			pos    = i
			symbol = t.str

			// check it is known
			switch symbol {
			case "|", "|!", "||":
				// ok
			default:
				PostError("unknown pipe symbol: %s", symbol)
				return FAILED
			}
			break
		}
	}

	if pos < 0 {
		return ctx.ParseWithCommand(children)
	}
	if pos == 0 {
		PostError("missing command before %s", symbol)
		return FAILED
	}
	if pos == len(children)-1 {
		PostError("missing command after %s", symbol)
		return FAILED
	}

	// handle each side of the pipe symbol
	if ctx.ParsePipeline(children[0:pos]) != OK {
		return FAILED
	}
	lhs := ctx.out

	if ctx.ParseWithCommand(children[pos+1:]) != OK {
		return FAILED
	}
	rhs := ctx.out

	// check that LHS does not redirect stdout
	lhs2 := lhs
	if lhs2.kind == NC_Pipe {
		lhs2 = lhs2.children[1]
	}
	if ctx.CheckRedirects(lhs2, true) != OK {
		return FAILED
	}

	nd := NewNode(NC_Pipe, symbol, children[0].pos)
	nd.Add(lhs)
	nd.Add(rhs)

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) ParseWithCommand(children []*Node) cmError {
	head := children[0]
	vars := NewNode(NG_Data, "", head.pos)

	if ! head.Match("with") {
		return ctx.ParseRawCommand(children, vars)
	}

	children = children[1:]

	// there should be one or more triplets of the form "VAR = VAL",
	// which should then be followed by the command itself or the `run`
	// statement.

	for len(children) >= 2 {
		if !children[1].Match("=") {
			break
		}

		if len(children) < 3 {
			PostError("missing value after =")
			return FAILED
		}

		lhs := children[0]
		rhs := children[2]

		if lhs.kind == NL_DirSym {
			PostError("stray %s in with statement", lhs.str)
			return FAILED
		}
		if rhs.kind == NL_DirSym {
			PostError("stray %s in with statement", rhs.str)
			return FAILED
		}

		if lhs.kind == NX_EnvVar {
			// ok
		} else if lhs.kind == NL_Name {
			if ! LEX_EnvironmentVar(lhs.str) {
				PostError("invalid environment variable name: %s", lhs.String())
				return FAILED
			}
			lhs.kind = NX_EnvVar
		} else {
			PostError("expected environment variable, got: %s", lhs.String())
			return FAILED
		}

		if ctx.ParseArgument(rhs) != OK {
			return FAILED
		}
		rhs = ctx.out

		assign := NewNode(NS_Assign, "", lhs.pos)
		assign.Add(lhs)
		assign.Add(rhs)

		vars.Add(assign)

		children = children[3:]
	}

	if vars.Len() == 0 {
		PostError("missing var assignments after with")
		return FAILED
	}
	if len(children) == 0 {
		PostError("missing command after with")
		return FAILED
	}

	return ctx.ParseRawCommand(children, vars)
}

func (ctx *ParsingCtx) ParseRawCommand(children []*Node, env_vars *Node) cmError {
	head := children[0]

	if head.Match("run") {
		children = children[1:]

		if len(children) == 0 {
			PostError("missing command after run")
			return FAILED
		}
	}

	head = children[0]

	if head.kind == NL_DirSym {
		PostError("missing command name, got: ", head.str)
		return FAILED
	}
	if head.HasGlob() {
		PostError("command name cannot be a glob pattern")
		return FAILED
	}

	redirects := NewNode(NG_Data, "", head.pos)

	// process the command name (as per args)
	if ctx.ParseArgument(head) != OK {
		return FAILED
	}

	nd := NewNode(NC_Command, "", head.pos)
	nd.Add(env_vars)
	nd.Add(redirects)
	nd.Add(ctx.out)

	children = children[1:]

	var group *Node

	for len(children) > 0 {
		arg := children[0]

		if arg.kind == NL_DirSym {
			if arg.Match("=") || arg.Match("!") || arg.Match("&") {
				PostError("stray %s found", arg.str)
				return FAILED
			}

			redir := NewNode(NC_Redir, arg.str, arg.pos)
			children = children[1:]

			{
				if len(children) == 0 {
					PostError("missing target after %s redirect", arg.str)
					return FAILED
				}

				arg = children[0]
				children = children[1:]

				// certain redirects are followed by a expression or var name
				to_var    := (redir.str[len(redir.str)-1] == '=')
				from_expr := (redir.str == "<<")

				if to_var || from_expr {
					arg.Unglob()
					if ctx.ParseTerm(arg) != OK {
						return FAILED
					}

					if to_var {
						switch ctx.out.kind {
						case NX_Global, NX_Local, NX_EnvVar:
							// ok
						default:
							PostError("expected variable after %s, got %s", redir.str, arg.String())
							return FAILED
						}
					}
				} else {
					if ctx.ParseArgument(arg) != OK {
						return FAILED
					}
				}

				redir.Add(ctx.out)
			}

			redirects.Add(redir)
			continue
		}

		// a normal argument
		if ctx.ParseArgument(arg) != OK {
			return FAILED
		}
		arg = ctx.out

		// group "dynamic" arguments together
		if arg.IsDynamic() {
			if group == nil {
				group = NewNode(NC_ArgGroup, "", arg.pos)
				nd.Add(group)
			}
			group.Add(arg)
			if arg.kind == NC_GlobMatch {
				group.flags |= EF_HAS_GLOB
			}

		} else {
			nd.Add(arg)
			group = nil
		}

		children = children[1:]
	}

	if ctx.CheckRedirects(nd, false) != OK {
		return FAILED
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) CheckRedirects(cmd *Node, is_pipe bool) cmError {
	// we check two things:
	// -  all redirects are known,
	// -  each channel is redirected at most once.

	have_stdin  := false
	have_stdout := is_pipe
	have_stderr := false

	redirects := cmd.children[1]

	for _, redir := range redirects.children {
		switch redir.str {
		case "<", "<<":
			if have_stdin {
				PostError("command has multiple redirects of stdin")
				return FAILED
			}
			have_stdin = true

		case ">", ">>", ">=", ">>=":
			if have_stdout {
				PostError("command has multiple redirects of stdout")
				return FAILED
			}
			have_stdout = true

		case "!>", "!>>", "!>=", "!>>=":
			if have_stderr {
				PostError("command has multiple redirects of stderr")
				return FAILED
			}
			have_stderr = true

		case "&>", "&>>", "&>=", "&>>=":
			if have_stdout {
				PostError("command has multiple redirects of stdout")
				return FAILED
			}
			if have_stderr {
				PostError("command has multiple redirects of stderr")
				return FAILED
			}
			have_stdout = true
			have_stderr = true

		default:
			PostError("unknown redirect symbol: %s", redir.str)
			return FAILED
		}
	}

	return OK
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) ParseCodeWord(t *Node) cmError {
	// this handles a complex NG_Word in a code context.

	if t.Len() > 1 {
		PostError("cannot use a complex word in that context")
		return FAILED
	}

	child := t.children[0]

	switch child.kind {
	case NW_Plain:
		// this is something rejected earlier
		PostError("invalid identifier: %s", child.str)
		return FAILED

	case NW_Single:
		return ctx.ParseSingleString(child)

	case NG_Double:
		return ctx.ParseDoubleString(child)

	case NG_Expr:
		// this must become a string builder
		if ctx.ParseTerm(child) != OK {
			return FAILED
		}
		nd := NewNode(ND_StrBuild, "", child.pos)
		nd.Add(ctx.out)

		ctx.out = nd
		return OK

	default:
		PostError("cannot use a complex word in that context")
		return FAILED
	}
}

func (ctx *ParsingCtx) ParseSingleString(t *Node) cmError {
	ctx.out = NewNode(NX_Value, "", t.pos)
	ctx.out.val = new(Value)
	ctx.out.val.MakeString(t.str)
	return OK
}

func (ctx *ParsingCtx) ParseDoubleString(t *Node) cmError {
	// this node will have been unglobbed already, but it may
	// contain some dynamic elements...

	if t.Len() > 1 {
		return ctx.WordBuilder(t)
	}

	s := ""

	if t.Len() == 1 {
		child := t.children[0]
		if child.kind != NW_Plain {
			return ctx.WordBuilder(t)
		}
		s = child.str
	}

	ctx.out = NewNode(NX_Value, "", t.pos)
	ctx.out.val = new(Value)
	ctx.out.val.MakeString(s)
	return OK
}

func (ctx *ParsingCtx) ParseArgument(t *Node) cmError {
	if t.HasGlob() {
		return ctx.GlobConstructor(t)
	}

	switch t.kind {
	case NX_EnvVar, NX_ArgParm, NX_Value:
		ctx.out = t
		return OK

	case NG_Expr, NG_Data, NG_Block:
		// these can evaluate as normal
		return ctx.ParseTerm(t)

	case NL_Name:
		ctx.out = NewNode(NX_Value, "", t.pos)
		ctx.out.val = new(Value)
		ctx.out.val.MakeString(t.str)
		return OK

	case NL_DirSym:
		PostError("stray %s found", t.str)
		return FAILED

	case NG_Word:
		return ctx.WordBuilder(t)

	default:
		PostError("expected command or argument, got: %s", t.String())
		return FAILED
	}
}

func (ctx *ParsingCtx) WordBuilder(t *Node) cmError {
	nd := NewNode(ND_StrBuild, "", t.pos)

	for _, child := range t.children {
		switch child.kind {
		case NW_Plain, NW_Glob, NW_Single:
			t_str := NewNode(NX_Value, "", nd.pos)
			t_str.val = new(Value)
			t_str.val.MakeString(child.str)

			nd.Add(t_str)

		case NW_Var:
			child.kind = NX_EnvVar
			nd.Add(child)

		case NW_Param:
			child.kind = NX_ArgParm
			nd.Add(child)

		case NW_Tilde:
			child.kind = NX_UserDir
			child.str  = child.str[1:]  // drop the tilde
			nd.Add(child)

		case NG_Expr:
			if ctx.ParseTerm(child) != OK {
				return FAILED
			}
			nd.Add(ctx.out)

		case NG_Double:
			if ctx.WordBuilder(child) != OK {
				return FAILED
			}
			if ctx.out.kind == ND_StrBuild {
				// flatten the tree
				for _, sub := range ctx.out.children {
					nd.Add(sub)
				}
			} else {
				nd.Add(ctx.out)
			}

		default:
			panic("odd thing in word / dbl string")
		}
	}

	ctx.out = nd
	return OK
}

func (ctx *ParsingCtx) GlobConstructor(t *Node) cmError {
	// this creates something like a string-builder, but certain parts
	// will be glob patterns (NW_Glob), the rest are expressions which
	// evaluate to a string.

	// firstly, flatten the heirarchy (double-quoted strings)
	nodes := make([]*Node, 0)

	for _, child := range t.children {
		if child.kind == NG_Double {
			for _, sub := range child.children {
				nodes = append(nodes, sub)
			}
		} else {
			nodes = append(nodes, child)
		}
	}

	// secondly, construct the NC_GlobMatch node
	nd := NewNode(NC_GlobMatch, "", t.pos)

	for _, child := range nodes {
		switch child.kind {
		case NW_Glob:
			// kept as-is
			nd.Add(child)

		case NW_Plain, NW_Single:
			t_str := NewNode(NX_Value, "", nd.pos)
			t_str.val = new(Value)
			t_str.val.MakeString(child.str)

			nd.Add(t_str)

		case NW_Var:
			child.kind = NX_EnvVar
			nd.Add(child)

		case NW_Param:
			child.kind = NX_ArgParm
			nd.Add(child)

		case NW_Tilde:
			child.kind = NX_UserDir
			child.str  = child.str[1:]  // drop the tilde
			nd.Add(child)

		case NG_Expr:
			if ctx.ParseTerm(child) != OK {
				return FAILED
			}
			build_nd := NewNode(ND_StrBuild, "", child.pos)
			build_nd.Add(ctx.out)

			nd.Add(build_nd)

		default:
			panic("odd thing in word / dbl string")
		}
	}

	ctx.out = nd
	return OK
}
