// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"

// import "time"
// import "math/rand"
// import "path/filepath"

import "gitlab.com/andwj/argv"

const VERSION = "0.3.3"

var Options struct {
	script  string
	args    []string

	cmd   []string  // -c option
	dump  bool
	trace int

	help     bool
	version  bool
}

//----------------------------------------------------------------------

func main() {
	lar_Init()

	// this will fatal error on invalid arguments
	HandleArgs()

	InitExecution()

	status := 0

	if Options.script != "" {
		status = RunScriptFile()
	} else if len(Options.cmd) > 0 {
		status = RunCommandString()
	} else {
		status = RunInteractive()
	}

	os.Exit(status)
}

func HandleArgs() {
	Options.trace = -1

	// TODO: Multi is not adequate for -c, it needs to be like `--` and grab
	//       everything after it.

	argv.Multi  ("c", "cmd",    &Options.cmd,     "str", "execute commands from a string")
	argv.Enabler("D", "dump",   &Options.dump,    "dump VM instructions of compiled code")
	argv.Integer("T", "trace",  &Options.trace,   "num", "max depth of stack traces, 0 to disable")
	argv.Enabler("h", "help",   &Options.help,    "display this help text")
	argv.Enabler("", "version", &Options.version, "display the version")

	err := argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	if Options.help {
		ShowUsage()
		os.Exit(0)
	}
	if Options.version {
		ShowVersion()
		os.Exit(0)
	}

	unparsed := argv.Unparsed()

	if len(unparsed) > 0 {
		if len(Options.cmd) > 0 {
			FatalError("extra arguments found with -c")
		} else {
			Options.script = unparsed[0]
			Options.args   = unparsed[1:]
		}
	}

	lar_SetDumpCode(Options.dump)

	if Options.trace >= 0 {
		lar_SetTraceDepth(Options.trace)
	}
}

func ShowUsage() {
	fmt.Printf("Usage: myrtle [OPTIONS] [SCRIPT ARGS...]\n")

	fmt.Printf("\n")
	fmt.Printf("Available options:\n")

	argv.Display(os.Stdout)
}

func ShowVersion() {
	fmt.Printf("Myrtle %s\n", VERSION)
}

func FatalError(format string, a ...interface{}) {
	if format != "" {
		format = "myrtle: " + format + "\n"
		fmt.Fprintf(os.Stderr, format, a...)
	}
	os.Exit(1)
}

//----------------------------------------------------------------------

func RunScriptFile() int {
	lar_AddARGS([]string{Options.script})
	lar_AddARGS(Options.args)

	// if the script uses the command line editor, this will reset
	// the terminal into a usable state should a panic occur.
	defer func() {
		if editor != nil {
			editor.Close()
		}
	}()

	if lar_LoadFile(Options.script) != OK {
		ShowErrors()
		return 1
	}
	if lar_CompileCode() != OK {
		ShowErrors()
		return 1
	}
	if ExecuteStatements() != OK {
		ShowErrors()
		return 1
	}

	return 0  // ok
}

func RunCommandString() int {
	lar_AddARGS([]string{"myrtle"})

/* TODO

	// if the script uses the command line editor, this will reset
	// the terminal into a usable state should a panic occur.
	defer func() {
		if editor != nil {
			editor.Close()
		}
	}()

	if lar_LoadLine(Options.expr, true) != OK {
		ShowErrors()
		return 1
	}
	if lar_CompileCode() != OK {
		ShowErrors()
		return 1
	}
	if ExecuteStatements() != OK {
		ShowErrors()
		return 1
	}

*/
	return 0  // ok
}

func ExecuteStatements() cmError {
	for {
		cl := lar_NextStatement()
		if cl == nil {
			break
		}

		if lar_RunCode(cl) != OK {
			return FAILED
		}

		/*
			s := lar_ValueString(LAR_REG_A)
			fmt.Printf("%s\n", s)
		*/
	}

	return OK
}

func ShowErrors() {
	for {
		msg := lar_NextError()
		if msg == "" {
			break
		}
		fmt.Fprintf(os.Stderr, "%s\n", msg)
	}
}
