// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "strconv"
import "math/rand"
import "path/filepath"

type EnvVar struct {
	name     string
	value    string
	exported bool
}

type ExecutionCtx struct {
	// the current input/output/error handles
	stdin  *os.File
	stdout *os.File
	stderr *os.File

	// the current set of arguments (mirrored in Glob.ARGS.loc)
	args *Value

	// the set of environment variables
	env    []EnvVar
	lookup map[string]int

	// the current working directory (an absolute path)
	cwd string

	// the parent context (earlier one in stack)
	parent *ExecutionCtx
}

// the stack of execution contexts.  base of stack is for the shell
// itself, the rest are for invocations of user-defined or built-in
// commands.
var exec_ctx *ExecutionCtx

var temp_directory string

func NewExecutionCtx() *ExecutionCtx {
	ctx := new(ExecutionCtx)
	ctx.env    = make([]EnvVar, 0)
	ctx.lookup = make(map[string]int)
	return ctx
}

func InitExecution() {
	exec_ctx = NewExecutionCtx()

	exec_ctx.stdin  = os.Stdin
	exec_ctx.stdout = os.Stdout
	exec_ctx.stderr = os.Stderr

	exec_ctx.args = Glob.ARGS.loc

	exec_ctx.cwd = DetermineInitialDir()

	// read the initial environment
	for _, s := range os.Environ() {
		HandleEnvString(s)
	}

	// determine directory for temp files
	temp_directory = DetermineTempDir()
}

func HandleEnvString(env_str string) {
	// find the '=' between key and value
	pos := -1

	for i := 0; i < len(env_str); i++ {
		if env_str[i] == '=' {
			pos = i
			break
		}
	}

	if pos <= 0 {
		// something weird, ignore it
		return
	}

	name  := env_str[0:pos]
	value := env_str[pos+1:]

	// if the same variable appears twice, the first one wins
	_, exist := exec_ctx.lookup[name]
	if !exist {
		exec_ctx.rawAddVar(name, value, true)
	}
}

func DetermineInitialDir() string {
	dir, err := os.Getwd()
	if err == nil {
		return dir
	}

	// TODO consider if we should display a message (if interactive)

	// hmmm, the above is very unlikely to fail, but if it does
	// fail then pick a directory and chdir into it.

	dir, err = os.UserHomeDir()
	if err == nil {
		if filepath.IsAbs(dir) {
			os.Chdir(dir)
			return dir
		}
	}

	if Exec_HasVar("HOME") {
		home := Exec_ReadVar("HOME")
		home = filepath.Clean(home)

		if filepath.IsAbs(home) {
			os.Chdir(home)
			return home
		}
	}

	// emergency fallback: root of the filesystem
	dir = string(os.PathSeparator)
	os.Chdir(dir)
	return dir
}

func DetermineTempDir() string {
	var err error

	dir := Exec_ReadVar("MYRTLE_TMPDIR")
	if dir != "" {
		if TestTempDir(dir, true) {
			return dir
		}
		FatalError("MYRTLE_TMPDIR is not usable")
	}

	// try the normal place, e.g. $TMPDIR and /tmp on Unix
	dir = os.TempDir()
	dir = filepath.Join(dir, "myrtle-tmp")

	if TestTempDir(dir, true) {
		return dir
	}

	// emergency fallback, try the user's home directory
	dir, err = os.UserCacheDir()

	if err == nil {
		// ensure something like $HOME/.cache exists
		os.Mkdir(dir, 0777)

		dir = filepath.Join(dir, "myrtle-tmp")

		if TestTempDir(dir, true) {
			return dir
		}
	}

	FatalError("unable to determine a temp-dir")
	return ""
}

func TestTempDir(dir string, mkdir bool) bool {
	if mkdir {
		os.Mkdir(dir, 0777)
	}

	fname := filepath.Join(dir, RandomBaseName("TEST", ""))

	f, err := os.Create(fname)
	if err != nil {
		return false
	}
	f.Close()

	os.Remove(fname)
	return true
}

func RandomBaseName(base, ext string) string {
	// NOTE: we assume the random number generator was seeded
	//       with the current time (done in lar_Init).

	p := os.Getpid()
	r := rand.Uint64()

	s1 := strconv.FormatUint(uint64(p), 36)
	s2 := strconv.FormatUint(r, 36)

	return base + "_" + s1 + "_" + s2 + ext
}

//----------------------------------------------------------------------

func Exec_PushContext() {
	ctx := NewExecutionCtx()

	ctx.stdin  = exec_ctx.stdin
	ctx.stdout = exec_ctx.stdout
	ctx.stderr = exec_ctx.stderr

	ctx.copyExportedVars(exec_ctx)

	ctx.cwd = exec_ctx.cwd

	ctx.args = new(Value)
	ctx.args.MakeArray(0)

	// make it current
	ctx.parent = exec_ctx
	exec_ctx   = ctx

	Glob.ARGS.loc = ctx.args
}

func Exec_PopContext() {
	if exec_ctx.parent == nil {
		panic("Exec_PopContext at base stack")
	}
	exec_ctx = exec_ctx.parent

	// restore the ARGS global
	Glob.ARGS.loc = exec_ctx.args

	// restore the current directory
	os.Chdir(exec_ctx.cwd)
}

func Exec_HasVar(name string) bool {
	_, exist := exec_ctx.lookup[name]
	return exist
}

func Exec_ReadVar(name string) string {
	idx, exist := exec_ctx.lookup[name]
	if exist {
		return exec_ctx.env[idx].value
	}
	return ""
}

func Exec_WriteVar(name, value string) {
	idx, exist := exec_ctx.lookup[name]
	if exist {
		exec_ctx.env[idx].value = value
	} else {
		// Note that new environment vars will be unexported
		exec_ctx.rawAddVar(name, value, false)
	}
}

func Exec_DeleteVar(name string) {
	idx, exist := exec_ctx.lookup[name]
	if exist {
		exec_ctx.env[idx].name  = ""
		exec_ctx.env[idx].value = ""
		exec_ctx.env[idx].exported = false

		delete(exec_ctx.lookup, name)
	}
}

func Exec_IsExported(name string) bool {
	idx, exist := exec_ctx.lookup[name]
	if exist {
		return exec_ctx.env[idx].exported
	}
	return false
}

func Exec_ExportVar(name string) {
	idx, exist := exec_ctx.lookup[name]
	if exist {
		exec_ctx.env[idx].exported = true
	} else {
		exec_ctx.rawAddVar(name, "", true)
	}
}

func Exec_UnexportVar(name string) {
	idx, exist := exec_ctx.lookup[name]
	if exist {
		exec_ctx.env[idx].exported = false
	} else {
		exec_ctx.rawAddVar(name, "", false)
	}
}

func Exec_ExportedStrings(extra map[string]string) []string {
	// this needs to have strings of the form "XYZ=something"
	list := make([]string, 0)

	for name, value := range extra {
		s := name + "=" + value
		list = append(list, s)
	}

	for _, ev := range exec_ctx.env {
		if ev.name != "" && ev.exported {
			_, exist := extra[ev.name]
			if !exist {
				s := ev.name + "=" + ev.value
				list = append(list, s)
			}
		}
	}
	return list
}

func (ctx *ExecutionCtx) copyExportedVars(source *ExecutionCtx) {
	for _, ev := range source.env {
		if ev.name != "" && ev.exported {
			ctx.rawAddVar(ev.name, ev.value, ev.exported)
		}
	}
}

func (ctx *ExecutionCtx) rawAddVar(name, value string, exported bool) {
	ctx.lookup[name] = len(ctx.env)
	ctx.env = append(ctx.env, EnvVar{name, value, exported})
}

//----------------------------------------------------------------------

func Run_UserCommand(user *UserCommand, cmd *CommandBuilder) error {
	// NOTE: we don't need to do anything for environment vars,
	//       they are inherited naturally from the parent ctx.

	Exec_PushContext()

	// TODO redirects

	// populate the ARGS array
	cmd.copyToARGS()

	// run the code
	child_p := ChildDataFrame()

	if RunClosure(user.loc.Clos, child_p) != OK {
		Status.loc.MakeNumber(1)
	}

	Exec_PopContext()

	return nil  // ok
}

//----------------------------------------------------------------------

func Print_Out(format string, a ...interface{}) {
	s := fmt.Sprintf(format, a...)
	exec_ctx.stdout.WriteString(s)
}

func Print_Err(format string, a ...interface{}) {
	s := fmt.Sprintf(format, a...)
	exec_ctx.stderr.WriteString(s)
}
