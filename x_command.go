// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "os/exec"
import "fmt"
import "path/filepath"

type CommandBuilder struct {
	// the argument list (including command itself).
	args []string

	// additional environment vars via `with` syntax
	env map[string]string

	// redirect files, or NIL for no change.
	// stdout and stderr may refer to the same file.
	stdin  *FileInfo
	stdout *FileInfo
	stderr *FileInfo
}

type FileInfo struct {
	f     *os.File    // open handle, nil if closed
	name  string      // filename
	temp  bool        // true for a temporary file
	null  bool        // true for NUL or /dev/null
	next  *FileInfo   // next in list of open files
}

type PipeConnection struct {
	offset int32

	stdout bool  // one or both of these must be true
	stderr bool  //
}

var open_files *FileInfo

func NewCommandBuilder() *CommandBuilder {
	cmd := new(CommandBuilder)
	cmd.args = make([]string, 0)
	cmd.env  = make(map[string]string)
	return cmd
}

func (cmd *CommandBuilder) copyToARGS() {
	Glob.ARGS.loc.MakeArray(0)

	for _, arg := range cmd.args {
		var elem Value
		elem.MakeString(arg)

		Glob.ARGS.loc.AppendElem(elem)
	}
}

func (cmd *CommandBuilder) Arg(arg string) {
	cmd.args = append(cmd.args, arg)
}

func (cmd *CommandBuilder) EnvVar(name, value string) {
	cmd.env[name] = value
}

//----------------------------------------------------------------------

func File_Open(name string, mode int, temp bool) *FileInfo {
	var err error

	info := new(FileInfo)

	if name == "NUL" || name == "/dev/null" {
		name = os.DevNull
		info.null = true
	}

	info.name = name
	info.temp = temp

	// determine parms for os.OpenFile
	var flag int
	var perm os.FileMode

	if mode == 0 {
		flag = os.O_RDONLY
	} else if mode == 1 {
		flag = os.O_WRONLY | os.O_CREATE | os.O_TRUNC
	} else {
		flag = os.O_WRONLY | os.O_CREATE | os.O_APPEND
	}

	if temp {
		perm = 0600
	} else {
		perm = 0666
	}

	info.f, err = os.OpenFile(name, flag, perm)
	if err != nil {
		return nil
	}

	// link into list of open files
	info.next = open_files
	open_files = info

	return info
}

func File_Reopen(info *FileInfo, use_fallback bool) error {
	var err error

	info.f.Close()

	var perm os.FileMode
	if info.temp {
		perm = 0600
	} else {
		perm = 0666
	}

	info.f, err = os.OpenFile(info.name, os.O_RDONLY, perm)

	if err != nil {
		info.f = nil

		if use_fallback {
			info.f, err = os.OpenFile(os.DevNull, os.O_RDONLY, perm)
			if err != nil {
				panic("unable to open /dev/null")
			}
		}
	}

	return err
}

func File_CreateTemp(prefix string, mode int) *FileInfo {
	loops := 0
	opens := 0

	for {
		loops += 1

		base := RandomBaseName(prefix, "")
		name := filepath.Join(temp_directory, base)

		if ! File_Exist(name) {
			opens += 1

			info := File_Open(name, mode, true)
			if info != nil {
				return info
			}
		}

		// guard against something consistenly going wrong
		if opens > 50 || loops > 1000 {
			return nil
		}
	}
}

func File_Exist(name string) bool {
	_, err := os.Lstat(name)
	return err == nil
}

func File_Close(info *FileInfo) {
	if info.f != nil {
		info.f.Close()
		info.f = nil
	}

	if info.temp {
		os.Remove(info.name)
	}
}

//----------------------------------------------------------------------

// Run_Command runs a command which has been set up in a fresh
// CommandBuilder via its Arg() etc... methods.
//
// The command is either a user-defined one, or an external program
// or script.
//
// If a problem occurs attempting to find/run the command, then an
// error object is returned, and the `Status` variable is set to 127.
// Otherwise the command is run, then waited for, and its exit status
// is stored in the `Status` variable and NIL is returned.
func Run_Command(cmd *CommandBuilder) error {
	cmd_name := cmd.args[0]

	if cmd_name == "" {
		return fmt.Errorf("empty command name")
	}

	// if the command name is bare, find it in $PATH
	base := filepath.Base(cmd_name)

	if base == cmd_name {
		// try a user-command (possibly a built-in)
		user := LookupUserCommand(base)

		if user != nil {
			return Run_UserCommand(user, cmd)
		}

		lp, err := exec.LookPath(base)
		if err != nil {
			Status.loc.MakeNumber(127)
			return err
		}

		cmd_name = lp
		cmd.args[0] = lp
	}

	attr := new(os.ProcAttr)
//	attr.Dir = "."  // TODO review what it should be

	// pass the environment variables
	attr.Env = Exec_ExportedStrings(cmd.env)

	// FIXME check that these don't fail
	/*
	attr.Files[0], _ = os.Open(os.DevNull)
	attr.Files[1], _ = os.OpenFile(os.DevNull, os.O_WRONLY, 0)
	attr.Files[2]    = attr.Files[1]
	*/

	// handle standard input, output and error
	attr.Files = make([]*os.File, 3)

	if cmd.stdin != nil {
		attr.Files[0] = cmd.stdin.f
	} else {
		attr.Files[0] = exec_ctx.stdin
	}
	if cmd.stdout != nil {
		attr.Files[1] = cmd.stdout.f
	} else {
		attr.Files[1] = exec_ctx.stdout
	}
	if cmd.stderr != nil {
		attr.Files[2] = cmd.stderr.f
	} else {
		attr.Files[2] = exec_ctx.stderr
	}

	proc, err := os.StartProcess(cmd_name, cmd.args, attr)
	if err != nil {
		Status.loc.MakeNumber(127)
		return err
	}

	state, err := proc.Wait()
	if err != nil {
		// no need to Kill() it, Wait() releases all resources of the process
		Status.loc.MakeNumber(127)
		return err
	}

	exit_code := state.ExitCode()

	// WISH: detect signal exits, use `128 + signal` like bash does
	if exit_code < 0 {
		exit_code = 128
	} else if exit_code > 255 {
		exit_code = 255
	}

	Status.loc.MakeNumber(float64(exit_code))
	return nil  // ok
}

