// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

type TypeKind int

const (
	TYP_Void  TypeKind = iota
	TYP_Bool
	TYP_Number
	TYP_Char
	TYP_String
	TYP_Array
	TYP_Map
	TYP_Function
)

type TypeDef struct {
	name    string
	index   int
	methods map[string]*Value
}

var typedefs []*TypeDef
var type_lookup map[string]int

func SetupTypes() {
	typedefs    = make([]*TypeDef, 0)
	type_lookup = make(map[string]int)
}

func NewTypeDef(name string) *TypeDef {
	def := new(TypeDef)
	def.name    = name
	def.methods = make(map[string]*Value)

	def.index = len(typedefs)
	typedefs  = append(typedefs, def)
	type_lookup[name] = def.index

	return def
}

func LookupType(name string) *TypeDef {
	idx, exist := type_lookup[name]
	if !exist {
		return nil
	}
	return typedefs[idx]
}

func (ty *TypeDef) HasMethod(name string) bool {
	_, exist := ty.methods[name]
	return exist
}

func (ty *TypeDef) AddMethod(name string, cl *Closure) *Value {
	v := new(Value)
	v.MakeFunction(cl)

	ty.methods[name] = v
	return v
}
