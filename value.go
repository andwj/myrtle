// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "math"
import "sort"
import "strings"
import "strconv"
import "unicode"

type Value struct {
	kind TypeKind

	Num   float64  // used for chars and boolean too
	Str   string
	Array *ArrayImpl
	Map   *MapImpl
	Clos  *Closure

	// internal stuff, not used for values
	Glob  *GlobFilename
	Cmd   *CommandBuilder
	File  *FileInfo
}

type ArrayImpl struct {
	data []Value
}

type MapImpl struct {
	// we are forced to use a pointer to Value here, because Go
	// does not allow taking the address of elements in a map.
	data map[string]*Value

	// non-nil for objects of a particular class
	obj_class *TypeDef
}

//----------------------------------------------------------------------

func (kind TypeKind) String() string {
	switch kind {
	case TYP_Void:
		return "NIL"
	case TYP_Bool:
		return "a boolean"
	case TYP_Char:
		return "a char"
	case TYP_Number:
		return "a number"
	case TYP_String:
		return "a string"
	case TYP_Array:
		return "an array"
	case TYP_Map:
		return "a map"
	case TYP_Function:
		return "a function"
	default:
		return "!!BAD VALUE!!"
	}
}

func (v *Value) ShallowCopy(src *Value) {
	switch src.kind {
	case TYP_Array:
		v.MakeArray(len(src.Array.data))
		copy(v.Array.data, src.Array.data)

	case TYP_Map:
		v.MakeMap()
		v.Map.obj_class = src.Map.obj_class
		for key, ptr := range src.Map.data {
			v.KeySet(key, *ptr)
		}

	default:
		*v = *src
	}
}

func (v *Value) MakeNumber(num float64) {
	v.kind = TYP_Number
	v.Num = num
	v.Str = ""
	v.Array = nil
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeNIL() {
	v.kind = TYP_Void
	v.Str = ""
	v.Array = nil
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeBool(b bool) {
	v.kind = TYP_Bool
	if b { v.Num = 1 } else { v.Num = 0 }
	v.Str = ""
	v.Array = nil
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeChar(ch rune) {
	v.kind = TYP_Char
	v.Num = float64(ch)
	v.Str = ""
	v.Array = nil
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeString(s string) {
	v.kind = TYP_String
	v.Str = s
	v.Array = nil
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeArray(size int) {
	impl := new(ArrayImpl)
	impl.data = make([]Value, size)

	v.kind = TYP_Array
	v.Array = impl
	v.Str = ""
	v.Map = nil
	v.Clos = nil
}

func (v *Value) MakeMap() {
	impl := new(MapImpl)
	impl.data = make(map[string]*Value)

	v.kind = TYP_Map
	v.Map = impl
	v.Str = ""
	v.Array = nil
	v.Clos = nil
}

func (v *Value) MakeFunction(cl *Closure) {
	v.kind = TYP_Function
	v.Clos = cl
	v.Str = ""
	v.Array = nil
	v.Map = nil
}

//----------------------------------------------------------------------

func (v *Value) IsNIL() bool {
	return v.kind == TYP_Void
}

func (v *Value) IsTrue() bool {
	// everything except "NIL" and "FALSE" is considered true
	if v.kind == TYP_Void {
		return false
	}
	if v.kind != TYP_Bool {
		return true
	}
	return v.Num > 0
}

func (v *Value) IsComplex() bool {
	return v.kind == TYP_Array || v.kind == TYP_Map
}

func (v *Value) IsOk() bool {
	// only "NIL" and numeric "0" is consider Ok
	if v.kind == TYP_Void {
		return true
	}
	if v.kind != TYP_Number {
		return false
	}
	return v.Num == 0
}

func (v *Value) Length() int {
	switch v.kind {
	case TYP_Array:
		return len(v.Array.data)

	case TYP_Map:
		return len(v.Map.data)

	case TYP_String:
		return len([]rune(v.Str))

	default:
		return 1
	}
}

func (v *Value) BasicEqual(other *Value) bool {
	if v.kind != other.kind {
		return false
	}

	switch v.kind {
	case TYP_Void:
		return true

	case TYP_Bool:
		return (v.Num > 0) == (other.Num > 0)

	case TYP_Number, TYP_Char:
		return v.Num == other.Num

	case TYP_String:
		return v.Str == other.Str

	case TYP_Function:
		return v.Clos == other.Clos

	default:
		// we do not compare arrays or maps, since this is used for `==`
		// and `!=` operators, and it might be expected that the contents
		// would be examined (a "deep" comparsion).
		return false
	}
}

func (v *Value) NumParameters() int {
	if v.kind != TYP_Function {
		return 0
	}

	if v.Clos.builtin != nil {
		return v.Clos.builtin.args
	}

	return len(v.Clos.parameters)
}

//----------------------------------------------------------------------

func (v *Value) ValidIndex(idx int) bool {
	if idx < 0 {
		return false
	}
	if idx >= len(v.Array.data) {
		return false
	}
	return true
}

func (v *Value) GetElem(idx int) Value {
	return v.Array.data[idx]
}

func (v *Value) SetElem(idx int, elem Value) {
	v.Array.data[idx] = elem
}

func (v *Value) SwapElems(i, k int) {
	A := v.Array.data[i]
	B := v.Array.data[k]

	v.Array.data[i] = B
	v.Array.data[k] = A
}

func (v *Value) AppendElem(elem Value) {
	v.Array.data = append(v.Array.data, elem)
}

func (v *Value) InsertElem(idx int, elem Value) {
	// an index <= zero will insert at the beginning (prepend).
	// an index >= length will insert at the end (append).

	if idx < 0 {
		idx = 0
	}
	if idx >= len(v.Array.data) {
		v.AppendElem(elem)
		return
	}

	// resize the array
	v.Array.data = append(v.Array.data, Value{})

	// shift elements up
	copy(v.Array.data[idx+1:], v.Array.data[idx:])

	// store new element
	v.Array.data[idx] = elem
}

func (v *Value) RemoveElem(idx int) {
	size := len(v.Array.data)

	// trying to remove non-existent elements is a no-op
	if idx < 0 || idx >= size {
		return
	}

	// shift elements down, if necessary
	if idx < size {
		copy(v.Array.data[idx:], v.Array.data[idx+1:])
	}

	// shrink the slice
	v.Array.data = v.Array.data[0 : size-1]
}

func (v *Value) ClearArray() {
	v.Array.data = make([]Value, 0)
}

//----------------------------------------------------------------------

func (v *Value) KeyExists(s string) bool {
	_, ok := v.Map.data[s]
	return ok
}

func (v *Value) KeyGet(s string) Value {
	ptr, ok := v.Map.data[s]
	if ok {
		return *ptr
	}

	var res Value
	res.MakeNIL()

	return res
}

func (v *Value) KeySet(s string, newval Value) {
	if newval.kind == TYP_Void {
		delete(v.Map.data, s)
		return
	}

	ptr, ok := v.Map.data[s]

	if !ok {
		ptr = new(Value)
		v.Map.data[s] = ptr
	}

	*ptr = newval
}

func (v *Value) KeyDelete(s string) {
	delete(v.Map.data, s)
}

func (v *Value) ClearMap() {
	v.Map.data = make(map[string]*Value)
}

func (v *Value) CollectKeys() Value {
	var list Value
	list.MakeArray(0)

	for key, _ := range v.Map.data {
		var v Value
		v.MakeString(key)
		list.AppendElem(v)
	}

	return list
}

//----------------------------------------------------------------------

type StringifyCtx struct {
	// this keeps track of arrays/maps which have been seen, to prevent
	// infinite recursion.
	seen map[string]bool
}

func (v *Value) DeepString() string {
	switch v.kind {
	case TYP_Void:
		return "NIL"

	case TYP_Bool:
		if v.Num > 0 {
			return "TRUE"
		} else {
			return "FALSE"
		}

	case TYP_Number:
		return v.NumberToString()

	case TYP_Char:
		return v.CharToString()

	case TYP_String:
		return v.StringToString()

	case TYP_Array, TYP_Map:
		var ctx StringifyCtx
		return ctx.DeepElement(v)

	case TYP_Function:
		return v.FunctionToString()

	default:
		return "!!INVALID!!"
	}
}

func (ctx *StringifyCtx) DeepElement(v *Value) string {
	if v == nil /* should never happen */ {
		return "NIL"
	}
	if ! v.IsComplex() {
		return v.DeepString()
	}

	// already seen?
	ref_str := ""

	switch v.kind {
		case TYP_Array: ref_str = fmt.Sprintf("%p", v.Array)
		case TYP_Map:   ref_str = fmt.Sprintf("%p", v.Map)
	}

	if ctx.seen != nil {
		_, ok := ctx.seen[ref_str]
		if ok {
			return "<CyclicData>"
		}
	}

	// need a new context which copies the old one
	var new_ctx StringifyCtx

	new_ctx.seen = make(map[string]bool)
	new_ctx.seen[ref_str] = true

	if ctx.seen != nil {
		for k, _ := range ctx.seen {
			new_ctx.seen[k] = true
		}
	}

	if v.kind == TYP_Array {
		return new_ctx.ArrayToString(v)
	} else {
		return new_ctx.MapToString(v)
	}
}

func (v *Value) NumberToString() string {
	if math.IsInf(v.Num, -1) {
		return "-INF"
	} else if math.IsInf(v.Num, 1) {
		return "+INF"
	} else if math.IsNaN(v.Num) {
		return "NAN"
	}

	// always show as an integer when possible
	int_val := util_to_int(v.Num)

	if float64(int_val) == v.Num {
		return fmt.Sprintf("%d", int_val)
	}

	return fmt.Sprintf("%g", v.Num)
}

func (v *Value) CharToString() string {
	return EncodeCharLiteral(rune(v.Num))
}

func (v *Value) StringToString() string {
	if v.Str == "\x1A" {
		return "EOF"
	}
	return EncodeStringLiteral(v.Str)
}

func (v *Value) FunctionToString() string {
	return "(fun " + v.Clos.debug_name + ")"
}

func (ctx *StringifyCtx) ArrayToString(v *Value) string {
	var sb strings.Builder

	sb.WriteString("[")

	for i := range v.Array.data {
		if i > 0 {
			sb.WriteString(" ")
		}

		elem := v.GetElem(i)

		sb.WriteString(ctx.DeepElement(&elem))
	}

	sb.WriteString("]")
	return sb.String()
}

func (ctx *StringifyCtx) MapToString(v *Value) string {
	var sb strings.Builder

	sb.WriteString("[")

	if v.Map.obj_class != nil {
		sb.WriteString(v.Map.obj_class.name)
	} else {
		sb.WriteString("map")
	}

	// collect all the names, and sort them
	keys := make([]string, 0, len(v.Map.data))

	for k := range v.Map.data {
		keys = append(keys, k)
	}

	sort.Slice(keys, func(i, k int) bool {
		return keys[i] < keys[k]
	})

	for i, name := range keys {
		if i >= 0 {
			sb.WriteString(" ")
		}

		// write the key name
		var str_val Value
		str_val.MakeString(name)
		sb.WriteString(str_val.StringToString())

		elem := v.Map.data[name]

		// write the value
		sb.WriteString(" ")
		sb.WriteString(ctx.DeepElement(elem))
	}

	sb.WriteString("]")
	return sb.String()
}

func EncodeCharLiteral(ch rune) string {
	if ch == '\'' {
		return "#`'"
	}

	if LEX_Printable(ch) || ch == ' ' {
		return "#'" + string(ch) + "'"
	}

	return "#" + EncodeEscapedChar(ch)
}

func EncodeStringLiteral(s string) string {
	var sb strings.Builder

	sb.WriteRune('"')

	for _, ch := range s {
		if	ch == '"' || ch == '`' || ch == '$' ||
			ch == '*' || ch == '?' || ch == '[' {

			sb.WriteRune('`')
			sb.WriteRune(ch)

		} else if LEX_Printable(ch) || ch == ' ' {
			sb.WriteRune(ch)

		} else {
			sb.WriteString(EncodeEscapedChar(ch))
		}
	}

	sb.WriteRune('"')
	return sb.String()
}

func EncodeEscapedChar(ch rune) string {
	if ch == 9 /* tab */ {
		return "`t"
	}

	s := strconv.FormatInt(int64(ch), 16)

	if ch < 128 {
		if len(s) < 2 { s = "0" + s }
		return "`x" + strings.ToUpper(s)
	} else {
		for len(s) < 4 { s = "0" + s }
		return "`u{" + strings.ToUpper(s) + "}"
	}
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) ParseNumber(t *Node) cmError {
	Error_SetPos(t.pos)

	r := []rune(t.str)

	ctx.out = NewNode(NX_Value, "", t.pos)
	ctx.out.val = new(Value)

	if len(r) >= 2 && r[0] == '0' && r[1] == 'b' {
		return ctx.ParseBinaryNumber(r, 2)
	}
	if len(r) >= 3 && (r[0] == '-' || r[0] == '+') && r[1] == '0' && r[2] == 'b' {
		return ctx.ParseBinaryNumber(r, 3)
	}

	if len(r) >= 2 && r[0] == '0' && r[1] == 'o' {
		return ctx.ParseOctalNumber(r, 2)
	}
	if len(r) >= 3 && (r[0] == '-' || r[0] == '+') && r[1] == '0' && r[2] == 'o' {
		return ctx.ParseOctalNumber(r, 3)
	}

	if len(r) >= 2 && r[0] == '0' && r[1] == 'x' {
		return ctx.ParseHexNumber(r, 2)
	}
	if len(r) >= 3 && (r[0] == '-' || r[0] == '+') && r[1] == '0' && r[2] == 'x' {
		return ctx.ParseHexNumber(r, 3)
	}

	return ctx.ParseDecimalNumber(r, 0)
}

func (ctx *ParsingCtx) ParseBinaryNumber(r []rune, pos int) cmError {
	is_neg := (r[0] == '-')

	s := ""

	for pos < len(r) {
		ch := r[pos]

		if ch == '0' || ch == '1' {
			s += string(ch)
			pos += 1
			continue
		}
		if unicode.IsDigit(ch) || unicode.IsLetter(ch) ||
			ch == '-' || ch == '+' || ch == '.' {

			PostError("illegal binary digit: %c", ch)
			return FAILED
		}
	}

	if s == "" {
		PostError("bad binary number (no digits)")
		return FAILED
	}

	s = "0b" + s
	if is_neg {
		s = "-" + s
	}

	err := ctx.out.val.DecodeInteger(s)
	if err != nil {
		PostError("%s", err.Error())
		return FAILED
	}
	return OK
}

func (ctx *ParsingCtx) ParseOctalNumber(r []rune, pos int) cmError {
	is_neg := (r[0] == '-')

	s := ""

	for pos < len(r) {
		ch := r[pos]

		if LEX_OctalDigit(ch) {
			s += string(ch)
			pos += 1
			continue
		}
		if unicode.IsDigit(ch) || unicode.IsLetter(ch) ||
			ch == '-' || ch == '+' || ch == '.' {

			PostError("illegal octal digit: %c", ch)
			return FAILED
		}
	}

	if s == "" {
		PostError("bad octal number (no digits)")
		return FAILED
	}

	s = "0o" + s
	if is_neg {
		s = "-" + s
	}

	err := ctx.out.val.DecodeInteger(s)
	if err != nil {
		PostError("%s", err.Error())
		return FAILED
	}
	return OK
}

func (ctx *ParsingCtx) ParseDecimalNumber(r []rune, pos int) cmError {
	is_neg := (r[0] == '-')

	if r[0] == '-' || r[0] == '+' {
		pos = 1
	}

	has_digit := false
	has_dot   := false
	has_E     := false
	has_exp   := false

	s := ""

	for pos < len(r) {
		ch := r[pos]

		if unicode.IsDigit(ch) {
			if has_E {
				has_exp = true
			} else {
				has_digit = true
			}
			s += string(ch)
			pos += 1
			continue
		}

		if ch == '.' {
			if !has_digit || has_dot || has_E {
				PostError("bad float (misplaced '.')")
				return FAILED
			}
			has_dot = true
			s += string(ch)
			pos += 1
			continue
		}

		if ch == 'e' || ch == 'E' {
			if !has_digit || has_E {
				PostError("bad float (misplaced 'e')")
				return FAILED
			}
			has_E = true
			s += string(ch)
			pos += 1

			if pos < len(r) && (r[pos] == '-' || r[pos] == '+') {
				s += string(r[pos])
				pos += 1
			}
			continue
		}

		if unicode.IsLetter(ch) || ch == '-' || ch == '+' {
			PostError("illegal decimal digit: %c", ch)
			return FAILED
		}
	}

	if s == "" {
		PostError("bad number (no digits)")
		return FAILED
	}
	if has_E && !has_exp {
		PostError("bad float (missing exponent)")
		return FAILED
	}

	if is_neg {
		s = "-" + s
	}

	var err error

	if has_dot || has_exp {
		err = ctx.out.val.DecodeFloat(s)
	} else {
		err = ctx.out.val.DecodeInteger(s)
	}
	if err != nil {
		PostError("%s", err.Error())
		return FAILED
	}
	return OK
}

func (ctx *ParsingCtx) ParseHexNumber(r []rune, pos int) cmError {
	is_neg := (r[0] == '-')

	has_digit := false
	has_dot   := false
	has_P     := false
	has_exp   := false

	s := ""

	for pos < len(r) {
		ch := r[pos]

		if has_P && unicode.IsLetter(ch) {
			PostError("illegal hex exponent digit: %c", ch)
			return FAILED
		}

		if unicode.Is(unicode.Hex_Digit, ch) {
			if has_P {
				has_exp = true
			} else {
				has_digit = true
			}
			s += string(ch)
			pos += 1
			continue
		}

		if ch == '.' {
			if !has_digit || has_dot || has_P {
				PostError("bad hex float (misplaced '.')")
				return FAILED
			}
			has_dot = true
			s += string(ch)
			pos += 1
			continue
		}

		if ch == 'p' || ch == 'P' {
			if !has_digit || has_P {
				PostError("bad hex float (misplaced 'p')")
				return FAILED
			}
			has_P = true
			s += string(ch)
			pos += 1

			if pos < len(r) && (r[pos] == '-' || r[pos] == '+') {
				s += string(r[pos])
				pos += 1
			}
			continue
		}

		if unicode.IsLetter(ch) || ch == '-' || ch == '+' {
			PostError("illegal hex digit: %c", ch)
			return FAILED
		}
	}

	if s == "" {
		PostError("bad hex number (no digits)")
		return FAILED
	}
	if (has_dot && !has_P) || (has_P && !has_exp) {
		PostError("bad hex float (missing exponent)")
		return FAILED
	}

	s = "0x" + s
	if is_neg {
		s = "-" + s
	}

	var err error

	if has_dot || has_exp {
		err = ctx.out.val.DecodeFloat(s)
	} else {
		err = ctx.out.val.DecodeInteger(s)
	}
	if err != nil {
		PostError("%s", err.Error())
		return FAILED
	}
	return OK
}

func (v *Value) DecodeInteger(s string) error {
	// passing "0" as base will allow other bases (binary/octal/hex)
	i, err := strconv.ParseInt(s, 0, 64)

	if err == nil {
		// check for loss of accuracy
		f := float64(i)

		if int64(f) == i {
			v.MakeNumber(f)
			return nil  // ok
		}
	}

	return fmt.Errorf("integer too big or invalid: %s", s)
}

func (v *Value) DecodeFloat(s string) error {
	// this should handle hexadecimal floats too
	f, err := strconv.ParseFloat(s, 64)

	if err == nil {
		v.MakeNumber(f)
		return nil  // ok
	}

	return fmt.Errorf("number too big or invalid: %s", s)

}

func (v *Value) DecodeFloatSpec(s string) error {
	switch s {
	case "+INF":
		v.MakeNumber(math.Inf(+1))
	case "-INF":
		v.MakeNumber(math.Inf(-1))
	case "NAN":
		v.MakeNumber(math.NaN())
	default:
		panic("weird NL_FltSpec: " + s)
	}
	return nil  // ok
}
