// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "time"
import "math/rand"

type GlobalDef struct {
	// its name (for debugging)
	name string

	// memory loc for the value
	loc *Value

	// variable can be modified?
	mutable bool

	// the unparsed expression for the value.
	// once parsed this becomes NIL, indicating it is eval'd.
	expr *Node

	// a parsed data structure (array or map)
	data *Node
}

type UserCommand struct {
	name string

	loc *Value
}

// all the global variables/constants
var globals []*GlobalDef
var global_lookup map[string]int

// all the user-defined commands
var commands []*UserCommand
var command_lookup map[string]int

// all compiled-but-not-yet-executed statements
var ready_statements []*Value

var Glob struct {
	EOF  *GlobalDef
	ARGS *GlobalDef
}

var Status *GlobalDef

//----------------------------------------------------------------------

func lar_Init() {
	all_filenames = make([]string, 0)

	rand.Seed(time.Now().Unix())

	SetupTypes()
	SetupGlobals()
	SetupBuiltins()
	SetupOperators()
}

func SetupGlobals() {
	globals = make([]*GlobalDef, 0)
	global_lookup = make(map[string]int)

	commands = make([]*UserCommand, 0)
	command_lookup = make(map[string]int)

	ClearStatements()

	MakeNumConstant("E",     2.7182818284590452353602874713527)
	MakeNumConstant("PI",    3.1415926535897932384626433832795)
	MakeNumConstant("PHI",   1.6180339887498948482045868343656)
	MakeNumConstant("SQRT2", 1.4142135623730950488016887242097)

	Status = MakeGlobal("Status", true)
	Status.loc.MakeNumber(0)

	Glob.EOF = MakeGlobal("EOF", false)
	Glob.EOF.loc.MakeString("\x1A")

	Glob.ARGS = MakeGlobal("ARGS", false)
	Glob.ARGS.loc.MakeArray(0)
}

func lar_AddARGS(args []string) {
	for _, s := range args {
		var strval Value
		strval.MakeString(s)

		Glob.ARGS.loc.AppendElem(strval)
	}
}

func lar_SetDumpCode(enable bool) {
	dump_code = enable
}

func NewUserCommand(name string) *UserCommand {
	cmd := new(UserCommand)
	cmd.name = name

	cl := NewClosure("cmd:" + name)
	cl.is_command = true

	cmd.loc = new(Value)
	cmd.loc.MakeFunction(cl)

	return cmd
}

func AddUserCommand(cmd *UserCommand) {
	command_lookup[cmd.name] = len(commands)
	commands = append(commands, cmd)
}

func LookupUserCommand(name string) *UserCommand {
	idx, exist := command_lookup[name]
	if !exist {
		return nil
	}
	return commands[idx]
}

func ClearStatements() {
	ready_statements = make([]*Value, 0)
}

func AddStatement(loc *Value) {
	ready_statements = append(ready_statements, loc)
}

func lar_NextStatement() *Closure {
	if len(ready_statements) == 0 {
		return nil
	}

	for _, loc := range ready_statements {
		if loc.kind == TYP_Function {
			cl := loc.Clos
			// remove it from further consideration
			loc.MakeNIL()
			return cl
		}
	}

	// none left
	ClearStatements()
	return nil
}

//----------------------------------------------------------------------

func LookupGlobal(name string) *GlobalDef {
	idx, exist := global_lookup[name]
	if !exist {
		return nil
	}
	return globals[idx]
}

func MakeNumConstant(name string, val float64) {
	def := MakeGlobal(name, false)
	def.loc.MakeNumber(val)
}

func MakeGlobal(name string, mutable bool) *GlobalDef {
	idx, exist := global_lookup[name]
	if exist {
		return globals[idx]
	}

	// create a new memory location for the variable
	loc := new(Value)
	loc.MakeNIL()

	def := new(GlobalDef)
	def.loc     = loc
	def.name    = name
	def.mutable = mutable

	global_lookup[name] = len(globals)
	globals = append(globals, def)

	return def
}

func DeleteGlobal(name string) {
	// this is only for the REPL, used to prevent a dud function
	// hanging around if compiling it failed.

	idx, exist := global_lookup[name]
	if exist {
		globals[idx].loc = new(Value)
		globals[idx].loc.MakeNIL()
	}

	delete(global_lookup, name)
}

const (
    ALLOW_SHADOW = (1 << iota)  // allow the name to shadow a global def
    ALLOW_UNDERSCORE
)

func ValidateName(t_name *Node, what string, flags int) cmError {
	if t_name.kind != NL_Name {
		PostError("bad %s name: not an identifier", what)
		return FAILED
	}
	if LEX_Number(t_name.str) {
		PostError("bad %s name: got a number", what)
		return FAILED
	}

	name := t_name.str

	if name == "_" && (flags & ALLOW_UNDERSCORE) != 0 {
		return OK
	}

	// disallow field names
	if len(name) >= 1 && name[0] == '.' {
		PostError("bad %s name: cannot begin with a dot", what)
		return FAILED
	}

	// disallow special keywords
	if len(name) >= 1 && name[0] == '#' {
		PostError("bad %s name: cannot begin with a '#'", what)
		return FAILED
	}

	// disallow method names
	if t_name.IsMethod() {
		PostError("bad %s name: cannot end with a '#'", what)
		return FAILED
	}

	// disallow language keywords
	if IsLanguageKeyword(name) {
		PostError("bad %s name: '%s' is a reserved keyword", what, name)
		return FAILED
	}

	// disallow names of the operators
	if IsOperatorName(name) {
		PostError("bad %s name: '%s' is a math operator", what, name)
		return FAILED
	}

	// already defined?
	// global defs normally cannot be redefined, but the REPL allows it.
	if (flags & ALLOW_SHADOW) == 0 && ! REPL {
		_, g_exist := global_lookup[name]
		_, c_exist := command_lookup[name]
		_, t_exist := type_lookup[name]

		if g_exist || c_exist || t_exist {
			PostError("bad %s name: '%s' already defined", what, name)
			return FAILED
		}
	}

	return OK
}

func IsLanguageKeyword(name string) bool {
	switch name {
	case
		// directives
		"type", "macro", "const", "var", "fun", "do", "command",

		// statements
		"let", "if", "else", "loop", "while", "until", "for",
		"return", "skip", "label", "continue", "break", "unless",
		"call", "lam", "fmt$", "matches?", "min", "max",

		// types
		"class", "array", "map", "set", "enum", "union",

		// constants
		"NIL", "TRUE", "FALSE", "EOF", "+INF", "-INF", "NAN",
		"Ok", "Failed",

		// miscellaneous
		":", "=", "$", "->", "_", ".", "..", "...":

		return true
	}

	return false
}

//----------------------------------------------------------------------

func CompileAllGlobals() cmError {
	// visit all global vars/constants which need to be evaluated.
	// we process them repeatedly until the count shrinks to zero.
	// if it fails to shrink, it means there is a cyclic dependency.

	last_count := -1  // first time through

	for {
		count     := 0
		got_error := false
		example   := ""

		for _, it := range pending_items {
			if it.p_var != nil {
				def := it.p_var
				count += 1

				switch def.Compile(it) {
				case OK:
					it.p_var = nil

				case HIT_UNEVAL:
					// try again in next round
					example = def.name

				case FAILED:
					it.p_var = nil
					got_error = true
				}
			}
		}

		if got_error {
			return FAILED
		}
		if count > 0 && count == last_count {
			PostError("cyclic dependencies with globals (such as %s)", example)
			return FAILED
		}

		if count == 0 {
			break
		}

		last_count = count
	}

	// populate any data structures now...
	for _, it := range pending_items {
		if it.p_data != nil {
			def := it.p_data
			def.Populate()
		}
	}

	if have_errors {
		return FAILED
	}
	return OK
}

func (def *GlobalDef) Compile(it *PendingItem) cmError {
	Error_SetPos(def.expr.pos)

	if def.expr.kind == NG_Block {
		PostError("cannot use a block in that context")
		return FAILED
	}

	var ctx ParsingCtx

	// the result could be HIT_UNEVAL here
	result := ctx.ParseTerm(def.expr)
	if result != OK {
		return result
	}

	lit := ctx.out

	Error_SetPos(lit.pos)

	// mark the global as evaluated
	def.expr = nil

	if lit.IsValue() {
		*def.loc = *lit.val
		return OK
	}

	// an operator node means a "const-expr" had a global var in it
	if lit.kind == NX_Unary || lit.kind == NX_Binary {
		PostError("expression is not constant (uses a global var)")
		return FAILED
	}
	if lit.kind == ND_StrBuild {
		PostError("string builder contains a non-constant element")
		return FAILED
	}

	// by policy, global variables cannot be used to initialize another
	// global var, with one exception: global var X can appear in a data
	// structure for global var Y _IFF_ X is also a data structure.

	if lit.kind == NX_Global {
		PostError("cannot use variable '%s' in initializer for '%s'", lit.str, def.name)
		return FAILED
	}

	// anything else should be a data structure...

	is_data := false
	switch lit.kind {
	case ND_Array, ND_Map, ND_Set:
		is_data = true
	}

	if !is_data {
		PostError("bad syntax in initializer for '%s'", def.name)
		return FAILED
	}

	if !def.mutable {
		PostError("global constants cannot be a data structure")
		return FAILED
	}

	// the data structure is "populated" later.
	// for now, we just create an empty structure.
	it.p_data = def

	def.data = lit

	if lit.kind == ND_Array {
		def.loc.MakeArray(0)
	} else {
		def.loc.MakeMap()
		def.loc.Map.obj_class = lit.tdef
	}
	return OK
}

//----------------------------------------------------------------------

func (def *GlobalDef) Populate() cmError {
	t := def.data

	Error_SetPos(t.pos)

	switch t.kind {
	case ND_Array:
		return def.PopulateArray(def.loc, t)

	case ND_Set:
		return def.PopulateSet(def.loc, t)

	case ND_Map:
		return def.PopulateMap(def.loc, t)

	default:
		panic("weird thing in PopulateGlobal")
	}
}

func (def *GlobalDef) PopulateArray(obj *Value, t *Node) cmError {
	for _, t_elem := range t.children {
		var v Value

		if def.PopTerm(&v, t_elem) != OK {
			return FAILED
		}
		obj.AppendElem(v)
	}

	return OK
}

func (def *GlobalDef) PopulateSet(obj *Value, t *Node) cmError {
	// this is stranger than fiction
	var truth Value
	truth.MakeBool(true)

	for _, t_key := range t.children {
		var k Value

		if def.PopTerm(&k, t_key) != OK {
			return FAILED
		}
		if k.kind != TYP_String {
			PostError("set keys must be a string, got %s", k.kind.String())
			return FAILED
		}

		obj.KeySet(k.Str, truth)
	}

	return OK
}

func (def *GlobalDef) PopulateMap(obj *Value, t *Node) cmError {
	for _, pair := range t.children {
		t_key := pair.children[0]
		t_val := pair.children[1]

		var k Value
		var v Value

		if def.PopTerm(&k, t_key) != OK {
			return FAILED
		}
		if def.PopTerm(&v, t_val) != OK {
			return FAILED
		}

		if k.kind != TYP_String {
			PostError("map keys must be a string, got %s", v.kind.String())
			return FAILED
		}

		// NOTE: we allow duplicate keys (consistent with compiled maps)

		obj.KeySet(k.Str, v)
	}

	return OK
}

func (def *GlobalDef) PopTerm(v *Value, lit *Node) cmError {
	Error_SetPos(lit.pos)

	if lit.IsValue() {
		*v = *lit.val
		return OK
	}

	// an operator node means a "const-expr" had a global var in it
	if lit.kind == NX_Unary || lit.kind == NX_Binary {
		PostError("expression is not constant (uses a global var)")
		return FAILED
	}
	if lit.kind == ND_StrBuild {
		PostError("string builder contains a non-constant element")
		return FAILED
	}

	if lit.kind == NX_Global {
		other := LookupGlobal(lit.str)

		switch other.loc.kind {
		case TYP_Array, TYP_Map:
			// data structures may refer to other data structures
			*v = *other.loc
			return OK

		default:
			PostError("cannot use variable '%s' in initializer for '%s'", lit.str, def.name)
			return FAILED
		}
	}

	// handle an embedded data structure...
	switch lit.kind {
	case ND_Array:
		v.MakeArray(0)
		return def.PopulateArray(v, lit)

	case ND_Set:
		v.MakeMap()
		return def.PopulateSet(v, lit)

	case ND_Map:
		v.MakeMap()
		v.Map.obj_class = lit.tdef
		return def.PopulateMap(v, lit)

	default:
		PostError("bad syntax in initializer for '%s'", def.name)
		return FAILED
	}
}
