// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "os/user"
import "io"
import "fmt"
import "path/filepath"
import "strings"
import "strconv"
import "time"
import "unicode"

import "github.com/peterh/liner"

// the command-line editor for terminal input.
// always available in an interactive shell, must be explicitly enabled
// in scripts.
var editor *liner.State

var ErrQuit   = fmt.Errorf("user quit")
var ErrAbort  = fmt.Errorf("user abort")
var ErrInput  = fmt.Errorf("input error")
var ErrCancel = fmt.Errorf("cancelled")

//----------------------------------------------------------------------

func RunInteractive() (status int) {
	lar_AddARGS([]string{"myrtle"})

	EnableEditor()

	// this will reset the terminal into a usable state should
	// a panic occur somewhere.
	// [ unfortunately there is no way to recover from serious
	//   runtime issues like overflowing the Go stack... ]
	defer editor.Close()

	for {
		err := ProcessUserInput()

		switch {
		case err == ErrQuit:
			return 0

		case err == ErrInput:
			return 1

		case err == ErrAbort:
			return 2
		}
	}
}

func EnableEditor() {
	if editor == nil {
		editor = liner.NewLiner()
		editor.SetCtrlCAborts(true)
	}
}

func ProcessUserInput() error {
	input, err := ReadLine(1)

	if err == io.EOF {
		Print_Err("\nEOF\n")
		return ErrQuit
	}
	if err == liner.ErrPromptAborted {
		Print_Err("Aborted\n")
		return ErrAbort
	}
	if err != nil {
		Print_Err("Input Error: %s\n", err.Error())
		return ErrInput
	}

	// ignore blank lines
	if input == "" {
		return nil
	}

	// see if user wants to quit
	switch input {
	case "quit", "QUIT", "exit", "EXIT":
		return ErrQuit
	}

	var kbd KeyboardReader
	kbd.Begin(input)

	// in interactive mode, we can recover from compilation and
	// runtime errors...

	load_err := lar_LoadLine(&kbd)

	// must do this after LoadLine, in order to capture any additional
	// input lines which may have been requested.
	editor.AppendHistory(kbd.JoinLines())

	if load_err == OK {
		if lar_CompileCode() == OK {
			if ExecuteStatements() == OK {
				return nil  // ok
			}
		}
	}

	ShowErrors()
	lar_ClearErrors()

	// remove any pending do blocks
	for lar_NextStatement() != nil {
	}

	return nil  // ok
}

//----------------------------------------------------------------------

type KeyboardReader struct {
	// current data that has not been read yet
	buffer []byte

	// all the raw input lines
	lines []string
}

func (kbd *KeyboardReader) Begin(s string) {
	kbd.lines = make([]string, 1)
	kbd.lines[0] = s

	// need a newline in the buffer!
	s = s + "\n"

	kbd.buffer = []byte(s)
}

func (kbd *KeyboardReader) Read(p []byte) (n int, err error) {
	if len(p) == 0 {
		return 0, nil
	}

	for len(kbd.buffer) == 0 {
		s, err := ReadLine(2)

		if err == io.EOF {
			// CTRL-D on a continuation line is a cancel
			return 0, ErrCancel
		}

		if err == liner.ErrPromptAborted {
			return 0, ErrAbort
		}

		if err != nil {
			return 0, err
		}

		kbd.lines = append(kbd.lines, s)

		// need a newline!
		s = s + "\n"

		kbd.buffer = []byte(s)
	}

	use_len := len(kbd.buffer)
	if use_len > len(p) {
		use_len = len(p)
	}

	copy(p, kbd.buffer[0:use_len])

	kbd.buffer = kbd.buffer[use_len:]

	return use_len, nil  // ok
}

func (kbd *KeyboardReader) JoinLines() string {
	// we separate each line with a space (NOT a newline).
	// while the liner package has a "Multi Line Mode", it seems
	// quite broken, so this is the best we can do...

	s := ""

	for idx, line := range kbd.lines {
		if idx > 0 {
			s = s + " "
		}
		s = s + line
	}
	return s
}

//----------------------------------------------------------------------

func ReadLine(prompt int) (string, error) {
	s, err := editor.Prompt(GetPrompt(prompt, false))

	// this usually shouldn't happen, as GetPrompt sanitizes the
	// string to be safe for the liner package, but just in case...
	if err == liner.ErrInvalidPrompt {
		s, err = editor.Prompt(GetPrompt(prompt, true))
	}

	return s, err
}

func GetPrompt(prompt int, basic bool) string {
	switch prompt {
	case 1:
		// for the superuser, default prompt gets a `#` hash
		return GetPromptFromVar("PS1", "=\\$ ", basic)

	case 2:
		return GetPromptFromVar("PS2", ".. ", basic)

	case 4:
		return GetPromptFromVar("PS4", "+ ", basic)

	default:
		return "? "
	}
}

func GetPromptFromVar(env_var string, s string, basic bool) string {
	if !basic {
		if Exec_HasVar(env_var) {
			s = Exec_ReadVar(env_var)
		}
	}

	// doing two things here:
	//   1. processing special sequences like `\u`
	//   2. sanitizing the string to be safe for the liner package

	var sb strings.Builder

	r := []rune(s)

	for len(r) > 0 {
		ch := r[0]
		r   = r[1:]

		if ch == '\\' && len(r) > 0 {
			esc := PromptEscape(r[0])
			r = r[1:]

			for _, ch := range esc {
				if ! unicode.Is(unicode.C, ch) {
					sb.WriteRune(ch)
				}
			}
		} else {
			if ch == '\t' || ch == '\n' {
				sb.WriteRune(' ')
			} else if ! unicode.Is(unicode.C, ch) {
				sb.WriteRune(ch)
			}
		}
	}

	return sb.String()
}

func PromptEscape(ch rune) string {
	// this is a subset of what bash and ksh support.
	// in particular, the `\D{format}` for dates is not supported.
	// due to the liner package, things like `\e` cannot be used,
	// similarly we don't handle octal escapes like `\033`.
	// plus things like `\!` don't make much sense in this shell.

	switch ch {
	case '\\':
		return "\\"

	case 'a':
		// bell character, we send it to the terminal now!
		Print_Out("\u0007")
		return ""

	case '$':
		// are we the superuser?
		if os.Geteuid() == 0 {
			return "#"
		}
		return "$"

	case 'n': return " "  // newline
	case 'r': return ""   // carriage return

	case 's': return "myrtle"
	case 'v': return VERSION
	case 'V': return VERSION

	case 'h': return PromptHostname(false)
	case 'H': return PromptHostname(true)

	case 'u': return PromptUsername()
	case 'w': return PromptDirectory(false)
	case 'W': return PromptDirectory(true)

	case 'd': return time.Now().Format("Mon Jan 02")
	case 't': return time.Now().Format("15:04:05")
	case 'T': return time.Now().Format("03:04:05")
	case '@': return time.Now().Format("03:04 PM")
	case 'A': return time.Now().Format("15:04")

	default:
		return "\\" + string(ch)
	}
}

func PromptHostname(full bool) string {
	host, err := os.Hostname()
	if err != nil {
		return "localhost"
	}
	if !full {
		idx := strings.IndexByte(host, byte('.'))
		if idx >= 0 {
			host = host[0:idx]
		}
	}
	if host == "" {
		return "localhost"
	}
	return host
}

func PromptDirectory(base bool) string {
	dir, err := os.Getwd()
	if err != nil {
		return "."
	}
	dir = filepath.Clean(dir)

	// replace home directory with a `~`, where possible
	if Exec_HasVar("HOME") {
		home := Exec_ReadVar("HOME")
		home = filepath.Clean(home)

		if dir == home {
			dir = "~"
		} else if strings.HasPrefix(dir, home + "/") || strings.HasPrefix(dir, home + "\\") {
			dir = "~" + dir[len(home):]
		}
	}

	if base {
		return filepath.Base(dir)
	}
	return dir
}

func PromptUsername() string {
	var info *user.User
	var err  error

	uid := os.Geteuid()

	if uid >= 0 {
		uid_str := strconv.Itoa(uid)
		info, err = user.LookupId(uid_str)
		if err != nil {
			return PromptUserFallback()
		}

	} else {
		// this code handles Windows (I hope)

		info, err = user.Current()
		if err != nil {
			return PromptUserFallback()
		}
	}

	name := info.Username
	if name == "" {
		return "localuser"
	}
	return name
}

func PromptUserFallback() string {
	home, err := os.UserHomeDir()

	if err != nil {
		home = Exec_ReadVar("HOME")
		home = filepath.Clean(home)
	}

	name := filepath.Base(home)
	if name == "." {
		return "localuser"
	}
	return name
}

func GetUserDirectory(name string) string {
	if name == "" {
		// prefer $HOME if it exists
		if Exec_HasVar("HOME") {
			home := Exec_ReadVar("HOME")
			home = filepath.Clean(home)

			if home != "." {
				return home
			}
		}

		home, err := os.UserHomeDir()
		if err == nil && home != "." {
			return home
		}

		return "/home/localuser"
	}

	info, err := user.Lookup(name)
	if err == nil {
		if info.HomeDir != "" {
			return info.HomeDir
		}
	}

	return "/home/" + name
}
