// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "io"
import "bufio"
//import "strings"
//import "fmt"
//import "path/filepath"

var all_filenames []string

const REPL = false

func AddFilename(name string) int {
	// file numbers are (1 + index)

	all_filenames = append(all_filenames, name)
	return len(all_filenames)
}

//----------------------------------------------------------------------

type PendingItem struct {
	// this is really a union, only one is ever non-nil
	p_var  *GlobalDef
	p_data *GlobalDef
	p_func *GlobalDef
	p_meth *Value
	p_stat *Value
	p_cmd  *UserCommand
}

// pending_items contains all the things (vars, funcs, etc) which have
// been "loaded" (and parsed) and are waiting to be compiled.
var pending_items []*PendingItem

func NewPendingItem() *PendingItem {
	it := new(PendingItem)
	pending_items = append(pending_items, it)
	return it
}

func InitPendingItems() {
	ClearPendingItems()
}

func ClearPendingItems() {
	pending_items = make([]*PendingItem, 0)
}

func AddPendingVar(def *GlobalDef) {
	it := NewPendingItem()
	it.p_var = def
}

func AddPendingFunction(def *GlobalDef) {
	it := NewPendingItem()
	it.p_func = def
}

func AddPendingMethod(loc *Value) {
	it := NewPendingItem()
	it.p_meth = loc
}

func AddPendingCommand(cmd *UserCommand) {
	it := NewPendingItem()
	it.p_cmd = cmd
}

func AddPendingStatement(loc *Value) {
	it := NewPendingItem()
	it.p_stat = loc
}

//----------------------------------------------------------------------

func lar_LoadFile(filename string) cmError {
	file_num := AddFilename(filename)

	f, err := os.Open(filename)
	if err != nil {
		Error_SetFile(0)
		PostError("%s", err.Error())
		return FAILED
	}
	defer f.Close()

	reader := bufio.NewReader(f)
	lex    := NewLexer(reader, file_num)

	for {
		t := lex.Scan()

		if t.kind == NL_EOF {
			if have_errors {
				return FAILED
			}
			return OK
		}

		// NOTE: on parse failures we generally skip ahead to the next
		//       known directive, allowing multiple syntax errors to be
		//       found and reported.

		Error_SetPos(t.pos)

		if t.kind == NL_ERROR {
			PostError("%s", t.str)
			lex.SkipAhead()
			continue
		}

		if t.Len() == 0 {
			continue
		}

		if LoadDirective(t) != OK {
			lex.SkipAhead()
			continue
		}
	}
}

func lar_LoadLine(r io.Reader) cmError {
	Error_SetFile(0)

	lex := NewLexer(r, 0)

	for {
		t := lex.Scan()

		if t.kind == NL_ERROR {
			PostError("%s", t.str)
			return FAILED
		}

		if t.kind == NL_EOF {
			return OK
		}

		/// Dump("token:", t, 0)

		if t.Len() > 0 {
			if LoadStatement(t) != OK {
				return FAILED
			}
		}

		// any other lines?
		if ! lex.MoreLines() {
			return OK
		}
	}
}

//----------------------------------------------------------------------

func LoadDirective(t *Node) cmError {
	head := t.children[0]

	if head.Match("type") {
		return LoadTypeDef(t)
	}
	if head.Match("const") {
		return LoadVarDef(t, false)
	}
	if head.Match("var") {
		return LoadVarDef(t, true)
	}
	if head.Match("fun") {
		return LoadFuncDef(t)
	}
	if head.Match("command") {
		return LoadCommandDef(t)
	}

	return LoadStatement(t)
}

func LoadTypeDef(t *Node) cmError {
	if t.Len() < 2 {
		PostError("bad type def: missing name")
		return FAILED
	}

	// reconstitute all tokens
	for _, child := range t.children {
		child.Unglob()
	}

	t_name := t.children[1]

	// check for reserved keywords like "var", "if", etc...
	if ValidateName(t_name, "type", 0) != OK {
		return FAILED
	}

	if t.Len() < 3 {
		PostError("bad type def: missing keyword after name")
		return FAILED
	}
	if t.Len() > 3 {
		PostError("bad type def: extra rubbish at end")
		return FAILED
	}

	t_sub := t.children[2]
	if t_sub.kind != NL_Name {
		PostError("bad type def: expected keyword, got: %s", t_sub.String())
		return FAILED
	}

	if t_sub.Match("class") {
		// ok
	} else if t_sub.kind == NL_Name {
		PostError("bad type def: unknown type kind: %s", t_sub.str)
		return FAILED
	}

	type_def := NewTypeDef(t_name.str)
	_ = type_def

	return OK
}

func LoadVarDef(t *Node, mutable bool) cmError {
	if t.Len() < 2 {
		PostError("bad variable def: missing name")
		return FAILED
	}

	// reconstitute all tokens
	for _, child := range t.children {
		child.Unglob()
	}

	t_name := t.children[1]

	// check for reserved keywords like "var", "if", etc...
	if ValidateName(t_name, "variable", 0) != OK {
		return FAILED
	}

	if t.Len() < 3 {
		PostError("bad variable def: missing '=' after name")
		return FAILED
	}
	if !t.children[2].Match("=") {
		PostError("bad variable def: missing '=' after name")
		return FAILED
	}

	children := t.children[3:]

	if len(children) == 0 {
		PostError("bad variable def: missing value")
		return FAILED
	}

	t_expr := children[0]

	// handle a value consisting of multiple tokens
	if len(children) > 1 {
		t_expr = NewNode(NG_Expr, "", t_expr.pos)
		for _, sub := range children {
			t_expr.Add(sub)
		}
	}

	// create the global now, the value is parsed/compiled later
	def := MakeGlobal(t_name.str, mutable)
	def.expr = t_expr

	AddPendingVar(def)
	return OK
}

func LoadFuncDef(t *Node) cmError {
	if t.Len() < 2 {
		PostError("bad function def: missing name")
		return FAILED
	}
	if t.Len() < 3 {
		PostError("bad function def: missing params")
		return FAILED
	}
	if t.Len() < 4 {
		PostError("bad function def: missing body")
		return FAILED
	}

	t_name  := t.children[1]
	t_pars  := t.children[2]
	t_block := t.children[3]

	// reconstitute function and parameter names
	t_name.Unglob()

	for _, child := range t_pars.children {
		child.Unglob()
	}

	name := t_name.str

	var method_class *TypeDef

	if t_name.IsMethod() {
		method_class = ParseMethodInParams(t_pars)
		if method_class == nil {
			return FAILED
		}

		// methods normally cannot be redefined, but the REPL allows it
		if method_class.HasMethod(name) && !REPL {
			PostError("method '%s' already defined in class %s", name, method_class.name)
			return FAILED
		}

	} else {
		// check for reserved keywords like "fun", "if", etc...
		if ValidateName(t_name, "function", 0) != OK {
			return FAILED
		}
	}

	if t_block.kind != NG_Block {
		PostError("bad function def: body must be a block in {}")
		return FAILED
	}
	if t.Len() > 4 {
		PostError("bad function def: extra rubbish after body")
		return FAILED
	}

	// give methods a helpful debugging name
	debug_name := name
	if method_class != nil {
		debug_name = method_class.name + ":" + name
	}

	cl := NewClosure(debug_name)
	cl.is_function = true
	cl.uncompiled_pars = t_pars
	cl.uncompiled_body = t_block

	if ParseParameters(t_pars, cl) != OK {
		return FAILED
	}

	if method_class != nil {
		v := method_class.AddMethod(name, cl)
		AddPendingMethod(v)
	} else {
		def := MakeGlobal(name, false)
		def.loc.MakeFunction(cl)
		AddPendingFunction(def)
	}
	return OK
}

func LoadCommandDef(t *Node) cmError {
	if t.Len() < 2 {
		PostError("bad command def: missing name")
		return FAILED
	}
	if t.Len() < 3 {
		PostError("bad command def: missing body")
		return FAILED
	}

	t_name  := t.children[1]
	t_block := t.children[2]

	// NOTE: we deliberately don't Unglob the name
	// [ since words on a command line are not Unglob'd ]

	// TODO this check will disallow some valid things  (and perhaps allow some invalid ones)
	if t_name.kind != NL_Name {
		PostError("bad name for user-defined command")
		return FAILED
	}

	// check for reserved keywords like "fun", "if", etc...
	if ValidateName(t_name, "command", 0) != OK {
		return FAILED
	}

	name := t_name.str

	if t_block.kind != NG_Block {
		PostError("bad command def: body must be a block in {}")
		return FAILED
	}
	if t.Len() > 4 {
		PostError("bad command def: extra rubbish after body")
		return FAILED
	}

	cmd := NewUserCommand(name)

	cl := cmd.loc.Clos
	cl.uncompiled_pars = NewNode(NG_Expr, "", t.pos)
	cl.uncompiled_body = t_block

	AddUserCommand(cmd)
	AddPendingCommand(cmd)
	return OK
}

func LoadDoBlock(t *Node) cmError {
	if t.Len() < 2 {
		PostError("missing block after do")
		return FAILED
	}
	if t.Len() > 2 {
		PostError("too many elements for do block")
		return FAILED
	}

	t_block := t.children[1]

	if t_block.kind != NG_Block {
		PostError("expected block after do, got: %s", t_block.String())
		return FAILED
	}

Dump("do block:", t_block, 0)
	EnqueueDoBlock(t_block)
	return OK
}

func LoadStatement(t *Node) cmError {
	head := t.children[0]

	if head.Match("do") {
		return LoadDoBlock(t)
	}
	if  head.Match("let") ||
		head.Match("skip") ||
		head.Match("junction") {

		PostError("cannot use %s outside of a function", head.str)
		return FAILED
	}

	t_block := NewNode(NG_Block, "", head.pos)
	t_block.Add(t)

	// enqueue the block for later compilation and execution
	EnqueueDoBlock(t_block)
	return OK
}

func EnqueueDoBlock(t_block *Node) {
	cl := NewClosure("do")
	cl.is_statement = true
	cl.uncompiled_pars = NewNode(NG_Expr, "", t_block.pos)
	cl.uncompiled_body = t_block

	loc := new(Value)
	loc.MakeFunction(cl)

	AddPendingStatement(loc)
}

//----------------------------------------------------------------------

func lar_CompileCode() cmError {
	err_count := 0

	if CompileAllGlobals() != OK {
		goto done
	}

	for _, it := range pending_items {
		if it.p_func != nil {
			if TryCompileFunction(it.p_func.loc) != OK {
				err_count += 1
			}
		}

		if it.p_meth != nil {
			if TryCompileFunction(it.p_meth) != OK {
				err_count += 1
			}
		}

		if it.p_cmd != nil {
			if TryCompileFunction(it.p_cmd.loc) != OK {
				err_count += 1
			}
		}

		if it.p_stat != nil {
			loc := it.p_stat

			if TryCompileFunction(loc) == OK {
				AddStatement(loc)
			} else {
				err_count += 1
			}
		}
	}

done:
	ClearPendingItems()

	if err_count > 0 || have_errors {
		return FAILED
	}
	return OK
}

func TryCompileFunction(loc *Value) cmError {
	cl := loc.Clos

	if CompileClosure(cl, nil) == OK {
		return OK
	}

	cl.failed = true

	// for the REPL, don't leave an uncompiled func
	if REPL {
		loc.MakeNIL()
	}
	return FAILED
}
