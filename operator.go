// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "math"
//import "strconv"

type OperatorInfo struct {
	name   string
	code   OpCode

	unary  bool
	prec   int
	right  bool  // right associative
}

var  unary_operators map[string]*OperatorInfo
var binary_operators map[string]*OperatorInfo

func SetupOperators() {
	 unary_operators = make(map[string]*OperatorInfo)
	binary_operators = make(map[string]*OperatorInfo)

	/* Binary operators */

	// NOTE: these two are special since they are short-circuiting,
	// which is achieved by some special code in the compiler.
	RegisterOperator(1, "and", OP_NOP)
	RegisterOperator(2, "or",  OP_NOP)

	RegisterOperator(3, "==", OP_EQ)
	RegisterOperator(3, "!=", OP_NE)
	RegisterOperator(3, "<",  OP_LT)
	RegisterOperator(3, ">",  OP_GT)
	RegisterOperator(3, "<=", OP_LE)
	RegisterOperator(3, ">=", OP_GE)

	RegisterOperator(4, "+",  OP_ADD)
	RegisterOperator(4, "-",  OP_SUB)
	RegisterOperator(5, "*",  OP_MUL)
	RegisterOperator(5, "/",  OP_DIV)
	RegisterOperator(5, "%",  OP_REM)

	RegisterOperator(4, "|",  OP_OR)
	RegisterOperator(4, "~",  OP_XOR)
	RegisterOperator(5, "&",  OP_AND)

	RegisterOperator(6, "<<", OP_SHIFT_L)
	RegisterOperator(6, ">>", OP_SHIFT_R)
	RegisterOperator(6, "**", OP_POW)

	/* Unary operators */

	RegisterOperator(0, "not", OP_NOT)
	RegisterOperator(0, "-",   OP_NEG)
	RegisterOperator(0, "~",   OP_FLIP)
}

func RegisterOperator(prec int, sym string, code OpCode) {
	info := new(OperatorInfo)

	info.name  = sym
	info.code  = code
	info.prec  = prec
	info.right = (sym == "**")

	if prec == 0 {
		unary_operators[sym] = info
	} else {
		binary_operators[sym] = info
	}
}

func IsOperatorName(name string) bool {
	_, exist1 :=  unary_operators[name]
	_, exist2 := binary_operators[name]

	return exist1 || exist2
}

func IsBinaryOperator(t *Node) bool {
	if ! (t.kind == NL_Name || t.kind == NL_DirSym) {
		return false
	}
	_, exist := binary_operators[t.str]
	return exist
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) OperatorExpand(children []*Node) cmError {
	// breaks the input into "complex" terms, separated by *binary*
	// operators.  a complex term consists of one or more tokens.
	// a multi-token term can be further decomposed when it begins
	// with a *unary* operator, otherwise it is consider a "form",
	// which includes function calls and some special things.
	//
	// single terms may be identifiers, literal values, an expression
	// in `()` parentheses, data o access in `[]` brackets, etc....
	// most of these are handled by code elsewhere.
	//
	// the input should never be empty.  the `out` field has the result.
	// on error, FAILED is returned.  for const exprs, HIT_UNEVAL will
	// be returned if a global constant is used but not yet evaluated.

	if len(children) == 0 {
		panic("empty expression")
	}

	// hack to allow "naked" lambdas
	if children[0].Match("lam") && ctx.fu != nil {
		return ctx.ParseForm(children)
	}

	// this list becomes: TERM { bin-op TERM }
	expr := make([]*Node, 0)

	num_ops := 0

	for len(children) > 0 {
		// find the next binary operator.
		// [ first element should never be one ]
		pos := 1

		for pos < len(children) {
			if IsBinaryOperator(children[pos]) {
				break
			}
			pos += 1
		}

		err := ctx.MultiTokenTerm(children[0:pos])
		if err != OK {
			return err
		}

		expr = append(expr, ctx.out)
		children = children[pos:]

		// do we have an operator?
		if len(children) > 0 {
			expr = append(expr, children[0])
			children = children[1:]
			num_ops += 1
		}
	}

	if num_ops > 0 {
		return ctx.ShuntingYard(expr)
	}

	ctx.out = expr[0]
	return OK
}

func (ctx *ParsingCtx) MultiTokenTerm(children []*Node) cmError {
	head := children[0]

	Error_SetPos(head.pos)

	if head.kind == NL_Name {
		switch head.str {
		case "type", "macro", "const", "var", "let", "do", "command":
			PostError("cannot use '%s' in that context", head.str)
			return FAILED
		}
	}

	if head.kind == NL_Name || head.kind == NL_DirSym {
		// a unary operator?
		op := unary_operators[head.str]

		if op != nil {
			return ctx.Term_Unary(op, children)
		}
	}

	if head.Match("fun") {
		return ctx.Term_Fun(children)
	}

	if len(children) > 1 {
		if ctx.fu != nil {
			return ctx.ParseForm(children)
		}

		if head.IsMethod() {
			PostError("cannot call methods in const exprs")
		} else if head.kind == NL_Name && ! LEX_Number(head.str) {
			PostError("cannot call functions in const exprs")
		} else {
			PostError("multiple values in const expr")
		}
		return FAILED
	}

	if head.kind == NL_Name && ! LEX_Number(head.str) {
		// local variables always shadow a global
		if ctx.LookupLocal(head.str) != nil {
			return ctx.ParseTerm(head)
		}

		// allow a single-name func call (for a known global)
		def := LookupGlobal(head.str)

		if def != nil {
			if !def.mutable && def.loc.kind == TYP_Function {
				if ctx.fu != nil {
					return ctx.ParseForm(children)
				}

				PostError("cannot call functions in const exprs")
				return FAILED
			}
		}
	}

	return ctx.ParseTerm(head)
}

func (ctx *ParsingCtx) Term_Unary(op *OperatorInfo, children []*Node) cmError {
	if len(children) < 2 {
		PostError("bad expression: missing term after %s operator", op.name)
		return FAILED
	}

	err := ctx.MultiTokenTerm(children[1:])
	if err != OK {
		return err
	}

	if ctx.out.IsValue() {
		return ctx.EV_UnaryOp(op.name, ctx.out)
	}

	t_unary := NewNode(NX_Unary, op.name, children[0].pos)
	t_unary.Add(ctx.out)

	ctx.out = t_unary
	return OK
}

func (ctx *ParsingCtx) Term_Fun(children []*Node) cmError {
	if len(children) < 2 {
		PostError("missing function name after fun")
		return FAILED
	}
	if len(children) > 2 {
		PostError("extra rubbish in fun form")
		return FAILED
	}

	t_name := children[1]

	if t_name.kind != NL_Name {
		PostError("expected function name, got: %s", t_name.String())
		return FAILED
	}

	def := LookupGlobal(t_name.str)

	if def == nil {
		if IsLanguageKeyword(t_name.str) {
			PostError("expected function name, got keyword: %s", t_name.str)
		} else {
			PostError("no such function: %s", t_name.str)
		}
		return FAILED
	}

	ctx.out = NewNode(NX_Const, t_name.str, t_name.pos)
	ctx.out.val = new(Value)
	*ctx.out.val = *def.loc
	return OK
}

func (ctx *ParsingCtx) ShuntingYard(children []*Node) cmError {
	/* this is Dijkstra's shunting-yard algorithm */

	op_stack   := make([]*OperatorInfo, 0)
	term_stack := make([]*Node, 0)

	shunt := func() cmError {
		// the op_stack is never empty when this is called
		op := op_stack[len(op_stack)-1]
		op_stack = op_stack[0 : len(op_stack)-1]

		if len(term_stack) < 2 {
			panic("FAILURE AT THE SHUNTING YARD")
		}

		L := term_stack[len(term_stack)-2]
		R := term_stack[len(term_stack)-1]

		var term *Node

		// if both sides are a constant, result is a constant
		if L.IsValue() && R.IsValue() {
			err := ctx.EV_BinaryOp(op.name, L, R)
			if err != OK {
				return err
			}
			term = ctx.out

		} else {
			term = NewNode(NX_Binary, op.name, L.pos)
			term.Add(L)
			term.Add(R)
		}

		term_stack = term_stack[0 : len(term_stack)-2]
		term_stack = append(term_stack, term)
		return OK
	}

	// the children list must be: TERM { bin-op TERM }

	for idx, t := range children {
		if (idx % 2) == 0 {
			// we have a term
			term_stack = append(term_stack, t)
			continue
		}

		op := binary_operators[t.str]

		// shunt existing operators if they have greater precedence
		for len(op_stack) > 0 {
			top := op_stack[len(op_stack)-1]

			right_assoc := op.right

			if top.prec > op.prec || (top.prec == op.prec && !right_assoc) {
				err := shunt()
				if err != OK {
					return err
				}
				continue
			}
			break
		}

		op_stack = append(op_stack, op)
	}

	// handle the remaining operators on stack
	for len(op_stack) > 0 {
		err := shunt()
		if err != OK {
			return err
		}
	}

	if len(term_stack) != 1 {
		panic("FAILURE AT THE SHUNTING YARD")
	}

	ctx.out = term_stack[0]
	return OK
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) EV_UnaryOp(op_name string, L *Node) cmError {
	LV := L.val

	ctx.ev_pos = L.pos

	switch LV.kind {
	case TYP_Bool:
		return ctx.EV_UnaryBool(op_name, LV)
	case TYP_Number:
		return ctx.EV_UnaryNumber(op_name, LV)
	default:
		PostError("operator %s cannot be used with %s", op_name,
			LV.kind.String())
		return FAILED
	}
}

func (ctx *ParsingCtx) EV_UnaryBool(op_name string, LV *Value) cmError {
	LB := LV.Num > 0

	switch op_name {
	case "not": return ctx.EV_MakeBool(! LB)
	}

	PostError("operator %s cannot be used with bool", op_name)
	return FAILED
}

func (ctx *ParsingCtx) EV_UnaryNumber(op_name string, LV *Value) cmError {
	if InfinityOrNan(LV.Num) {
		PostError("detected an infinity or NaN in const expression")
		return FAILED
	}

	switch op_name {
	case "-": return ctx.EV_MakeNumber(- LV.Num)
	case "~": return ctx.EV_MakeNumber(util_bit_flip(LV.Num))
	}

	PostError("operator %s cannot be used with a number", op_name)
	return FAILED
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) EV_BinaryOp(op_name string, L, R *Node) cmError {
	LV := L.val
	RV := R.val

	ctx.ev_pos = L.pos

	// check types are compatible
	if LV.kind != RV.kind {
		PostError("type mismatch in args of %s operator", op_name)
		return FAILED
	}

	switch LV.kind {
	case TYP_Bool:
		return ctx.EV_BinaryBool(op_name, LV, RV)

	case TYP_Char:
		return ctx.EV_BinaryChar(op_name, LV, RV)

	case TYP_Number:
		return ctx.EV_BinaryNumber(op_name, LV, RV)

	case TYP_String:
		return ctx.EV_BinaryString(op_name, LV, RV)

	default:
		PostError("operator %s cannot be used with %s", op_name,
			LV.kind.String())
		return FAILED
	}
}

func (ctx *ParsingCtx) EV_BinaryBool(op_name string, LV, RV *Value) cmError {
	LB := LV.Num > 0
	RB := RV.Num > 0

	switch op_name {
	case "and": return ctx.EV_MakeBool( LB && RB )
	case "or":  return ctx.EV_MakeBool( LB || RB )
	case "==":  return ctx.EV_MakeBool( LB == RB )
	case "!=":  return ctx.EV_MakeBool( LB != RB )

	case "<":   return ctx.EV_MakeBool( LV.Num <  RV.Num )
	case "<=":  return ctx.EV_MakeBool( LV.Num <= RV.Num )
	case ">":   return ctx.EV_MakeBool( LV.Num >  RV.Num )
	case ">=":  return ctx.EV_MakeBool( LV.Num >= RV.Num )
	}

	PostError("operator %s cannot be used with a bool", op_name)
	return FAILED
}

func (ctx *ParsingCtx) EV_BinaryChar(op_name string, LV, RV *Value) cmError {
	switch op_name {
	case "==": return ctx.EV_MakeBool(LV.Num == RV.Num)
	case "!=": return ctx.EV_MakeBool(LV.Num != RV.Num)
	case "<":  return ctx.EV_MakeBool(LV.Num <  RV.Num)
	case "<=": return ctx.EV_MakeBool(LV.Num <= RV.Num)
	case ">":  return ctx.EV_MakeBool(LV.Num >  RV.Num)
	case ">=": return ctx.EV_MakeBool(LV.Num >= RV.Num)
	}

	PostError("operator %s cannot be used with a char", op_name)
	return FAILED
}

func (ctx *ParsingCtx) EV_BinaryNumber(op_name string, LV, RV *Value) cmError {
	if InfinityOrNan(LV.Num) || InfinityOrNan(RV.Num) {
		PostError("detected an infinity or NaN in const expression")
		return FAILED
	}

	// check for division by zero
	if op_name == "/" || op_name == "%" {
		if RV.Num == 0 {
			PostError("division by zero in const expression")
			return FAILED
		}
	}

	switch op_name {
	case "+":  return ctx.EV_MakeNumber(LV.Num + RV.Num)
	case "-":  return ctx.EV_MakeNumber(LV.Num - RV.Num)
	case "*":  return ctx.EV_MakeNumber(LV.Num * RV.Num)
	case "/":  return ctx.EV_MakeNumber(LV.Num / RV.Num)
	case "%":  return ctx.EV_MakeNumber(util_remainder(LV.Num, RV.Num))
	case "**": return ctx.EV_MakeNumber(util_power(LV.Num, RV.Num))

	case "<<": return ctx.EV_MakeNumber(util_shift_left (LV.Num, RV.Num))
	case ">>": return ctx.EV_MakeNumber(util_shift_right(LV.Num, RV.Num))
	case "&":  return ctx.EV_MakeNumber(util_bit_and(LV.Num, RV.Num))
	case "|":  return ctx.EV_MakeNumber(util_bit_or (LV.Num, RV.Num))
	case "~":  return ctx.EV_MakeNumber(util_bit_xor(LV.Num, RV.Num))

	case "==": return ctx.EV_MakeBool(LV.Num == RV.Num)
	case "!=": return ctx.EV_MakeBool(LV.Num != RV.Num)
	case "<":  return ctx.EV_MakeBool(LV.Num <  RV.Num)
	case "<=": return ctx.EV_MakeBool(LV.Num <= RV.Num)
	case ">":  return ctx.EV_MakeBool(LV.Num >  RV.Num)
	case ">=": return ctx.EV_MakeBool(LV.Num >= RV.Num)
	}

	PostError("operator %s cannot be used with a number", op_name)
	return FAILED
}

func (ctx *ParsingCtx) EV_BinaryString(op_name string, LV, RV *Value) cmError {
	switch op_name {
	case "+":  return ctx.EV_MakeString(LV.Str + RV.Str)

	case "==": return ctx.EV_MakeBool(LV.Str == RV.Str)
	case "!=": return ctx.EV_MakeBool(LV.Str != RV.Str)
	case "<":  return ctx.EV_MakeBool(LV.Str <  RV.Str)
	case "<=": return ctx.EV_MakeBool(LV.Str <= RV.Str)
	case ">":  return ctx.EV_MakeBool(LV.Str >  RV.Str)
	case ">=": return ctx.EV_MakeBool(LV.Str >= RV.Str)
	}

	PostError("operator %s cannot be used with a string", op_name)
	return FAILED
}

func (ctx *ParsingCtx) EV_MakeBool(b bool) cmError {
	ctx.out = NewNode(NX_Value, "", ctx.ev_pos)
	ctx.out.val = new(Value)
	ctx.out.val.MakeBool(b)
	return OK
}

func (ctx *ParsingCtx) EV_MakeNumber(n float64) cmError {
	if InfinityOrNan(n) {
		PostError("detected an infinity or NaN in const expression")
		return FAILED
	}
	ctx.out = NewNode(NX_Value, "", ctx.ev_pos)
	ctx.out.val = new(Value)
	ctx.out.val.MakeNumber(n)
	return OK
}

func (ctx *ParsingCtx) EV_MakeString(s string) cmError {
	ctx.out = NewNode(NX_Value, "", ctx.ev_pos)
	ctx.out.val = new(Value)
	ctx.out.val.MakeString(s)
	return OK
}

func InfinityOrNan(n float64) bool {
	return math.IsInf(n, 0) || math.IsNaN(n)
}
