// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"

type Closure struct {
	// when this is non-nil, this is a built-in function, and ALL the
	// other fields in this struct are meaningless.
	builtin *Builtin

	is_command   bool
	is_statement bool
	is_function  bool  // includes methods

	// the uncompiled parameters (a NG_Expr)
	uncompiled_pars *Node

	// the uncompiled block of code (a NG_Block)
	uncompiled_body *Node

	// the compiled form is a sequence of operations
	vm_ops []Operation

	// the parameter names of the function
	parameters []string

	// all the constant values used in the function
	constants []Value

	// the parent function which contains this one.  Will be nil for
	// global functions, and non-nil for anonymous lambda functions.
	parent *Closure

	// the variables which belong to a parent function.
	// these are created when a `lam` form is executed (instantiation of
	// the template closure).
	// OP_UPV_READ/WRITE operations access this array.
	upvars []*Upvar

	// the variables captured by a `lam` lambda function.
	// it can include variables which are not directly used in a lambda
	// but *are* used in a sub-lambda (or sub-sub-lambda, etc).
	captures []*LocalVar

	// did the function or method fail to parse/compile?
	failed bool

	// this is used while compiling, and is the highest data stack
	// offset used so far by the current function.  Starts off at zero,
	// which always contains the function object itself, and that is
	// followed by a slot for each parameter.  it naturally increases
	// and decreases as variable scopes (blocks) come and go.
	high_water    int32
	highest_water int32

	saved_hw []int32

	block_scopes []*Scope

	// name shown in stack traces
	debug_name string

	num_lambdas int

	// used to set line number in VM instructions, and to get
	// the filename for run-time errors.
	cur_pos Position
}

var dump_code bool = false

// DUMMY_PC is used temporarily in jump instructions whose target
// code does not exist yet.
const DUMMY_PC = -999

//----------------------------------------------------------------------

func NewClosure(debug_name string) *Closure {
	cl := new(Closure)
	cl.debug_name = debug_name

	cl.parameters   = make([]string, 0)
	cl.vm_ops       = make([]Operation, 0)
	cl.upvars       = make([]*Upvar, 0)
	cl.captures     = make([]*LocalVar, 0)
	cl.saved_hw     = make([]int32, 0)
	cl.block_scopes = make([]*Scope, 0)

	return cl
}

func CompileClosure(cl *Closure, parent_scope *Scope) cmError {
	var ctx ParsingCtx
	ctx.fu = cl
	ctx.scope = parent_scope

	// need a scope for the parameters
	ctx.PushScope()

	// compile the parameters
	t_pars := cl.uncompiled_pars

	cl.cur_pos = t_pars.pos
	cl.parameters = make([]string, 0)

	for _, p := range t_pars.children {
		cl.parameters = append(cl.parameters, p.str)
		ctx.AddLocal(p.str, true)  // mutable
	}

	// parse the function body
	body := cl.uncompiled_body

	if ctx.ParseBlock(body, "") != OK {
		return FAILED
	}
	body = ctx.out

	// a debugging aid...
	if dump_code {
		Dump("parse tree for " + cl.debug_name, body, 0)
	}

	Error_SetPos(body.pos)
	cl.cur_pos = body.pos

	// allocate stack slots for parameters.
	// if any parameters were captured, "hoist" them to the heap.
	cl.AllocateLocals(ctx.scope, true)

	// compile the function body
	if cl.CompileBlock(body, true /* tail */) != OK {
		return FAILED
	}

	cl.Emit1(OP_RETURN)

	ctx.PopScope()

	// a debugging aid...
	if dump_code {
		DumpOps(cl)
	}

	return OK
}

func (cl *Closure) CompileBlock(t *Node, tail bool) cmError {
	Error_SetPos(t.pos)
	cl.cur_pos = t.pos

	if t.Len() == 0 {
		// an empty block is always NIL
		cl.Emit1(OP_MAKE_NIL)
		return OK
	}

	// prepare for skip statements
	cl.PushBlockScope(t.scope)

	cl.SaveHighWater()
	cl.AllocateLocals(t.scope, false)

	// compile each child expression.
	// the last one will provide the final "result" without having
	// to do anything special.

	err_count := 0

	for idx, child := range t.children {
		is_last := (idx == t.Len()-1)

		if cl.CompileStatement(child, tail && is_last) != OK {
			err_count += 1
		}
	}

	cl.RestoreHighWater()
	cl.PopBlockScope()

	if err_count > 0 {
		return FAILED
	}
	return OK
}

func (cl *Closure) CompileStatement(t *Node, tail bool) cmError {
	Error_SetPos(t.pos)
	cl.cur_pos = t.pos

	switch t.kind {
	case NS_Return:
		return cl.CompileReturn(t)

	case NS_Skip:
		return cl.CompileSkip(t)

	case NS_Label:
		return cl.CompileLabel(t)

	case NS_Continue:
		return cl.CompileContinueOrBreak(t, "NEXT")

	case NS_Break:
		return cl.CompileContinueOrBreak(t, "OUT")

	case NS_Let:
		return cl.CompileLet(t)

	case NS_Assign:
		return cl.CompileAssign(t)

	case NS_If:
		return cl.CompileIf(t, tail)

	case NS_Loop:
		return cl.CompileLoop(t)

	case NS_For:
		return cl.CompileForRange(t)

	case NS_ForEach:
		return cl.CompileForEach(t)

	case NC_Command:
		return cl.CompileCommand(t, false, nil, nil)

	case NC_Backgnd:
		t = t.children[0]
		return cl.CompileCommand(t, true, nil, nil)

	case NC_Pipe:
		return cl.CompilePipe(t, nil)
	}

	return cl.CompileExpr(t, tail)
}

func (cl *Closure) CompileReturn(t *Node) cmError {
	if t.Len() == 0 {
		cl.Emit1(OP_MAKE_NIL)
	} else {
		// NOTE: return expressions are always in tail position
		if cl.CompileExpr(t.children[0], true) != OK {
			return FAILED
		}
	}

	cl.Emit1(OP_RETURN)
	return OK
}

func (cl *Closure) CompileSkip(t *Node) cmError {
	if t.Len() == 0 {
		cl.Emit1(OP_MAKE_NIL)
	} else {
		if cl.CompileExpr(t.children[0], true) != OK {
			return FAILED
		}
	}

	scope := cl.FindScopeForLabel(t.str)
	if scope == nil {
		PostError("no such label: %s", t.str)
		return FAILED
	}

	skip_pc := cl.CurrentPC()
	cl.Emit3(OP_JUMP, NO_PART, DUMMY_PC)

	scope.skip_pcs = append(scope.skip_pcs, skip_pc)
	return OK
}

func (cl *Closure) CompileContinueOrBreak(t *Node, label string) cmError {
	scope := cl.FindScopeForLabel(label)

	// this should never happen...
	if scope == nil {
		PostError("no such label: %s", label)
		return FAILED
	}

	// handle the condition
	if_pc := int32(-1)

	if t.Len() > 0 {
		if cl.CompileExpr(t.children[0], false) != OK {
			return FAILED
		}

		if_pc = cl.CurrentPC()

		if (t.flags & EF_NEGATE) != 0 {
			cl.Emit3(OP_IF_TRUE,  NO_PART, DUMMY_PC)
		} else {
			cl.Emit3(OP_IF_FALSE, NO_PART, DUMMY_PC)
		}
	}

	// this part is like `skip` with an implicit NIL
	cl.Emit1(OP_MAKE_NIL)

	skip_pc := cl.CurrentPC()
	cl.Emit3(OP_JUMP, NO_PART, DUMMY_PC)

	scope.skip_pcs = append(scope.skip_pcs, skip_pc)

	if if_pc >= 0 {
		// this is like the else clause of an `if` statement
		cl.JumpHere(if_pc)
		cl.Emit1(OP_MAKE_NIL)
	}
	return OK
}

func (cl *Closure) CompileLabel(t *Node) cmError {
	// prevent the child expression from finding the label.
	// since a label is always at the end of a block, all previous
	// skips to this label have been compiled.

	scope := cl.block_scopes[len(cl.block_scopes)-1]
	scope.end_label = ""

	if t.Len() == 0 {
		cl.Emit1(OP_MAKE_NIL)
	} else {
		if cl.CompileExpr(t.children[0], true) != OK {
			return FAILED
		}
	}
	return OK
}

func (cl *Closure) CompileIf(t *Node, tail bool) cmError {
	t_cond := t.children[0]

	if cl.CompileTerm(t_cond) != OK {
		return FAILED
	}

	if_pc := cl.CurrentPC()
	cl.Emit3(OP_IF_FALSE, NO_PART, DUMMY_PC)

	// compile the body
	if cl.CompileBlock(t.children[1], tail) != OK {
		return FAILED
	}

	else_pc := cl.CurrentPC()
	cl.Emit3(OP_JUMP, NO_PART, DUMMY_PC)

	cl.JumpHere(if_pc)

	// compile else clause, or produce `NIL` if none
	if t.Len() < 3 {
		cl.Emit1(OP_MAKE_NIL)
	} else {
		// this node is either NG_Block or NS_If
		if cl.CompileStatement(t.children[2], tail) != OK {
			return FAILED
		}
	}

	cl.JumpHere(else_pc)
	return OK
}

func (cl *Closure) CompileLoop(t *Node) cmError {
	have_cond := (t.Len() > 1)

	start_pc := cl.CurrentPC()
	if_pc    := int32(-1)

	// compile the condition
	if have_cond {
		if cl.CompileTerm(t.children[1]) != OK {
			return FAILED
		}

		// jump to end unless value is true
		if_pc = cl.CurrentPC()

		if (t.flags & EF_NEGATE) != 0 {
			cl.Emit3(OP_IF_TRUE, NO_PART, DUMMY_PC)
		} else {
			cl.Emit3(OP_IF_FALSE, NO_PART, DUMMY_PC)
		}
	}

	// compile the body
	if cl.CompileBlock(t.children[0], false) != OK {
		return FAILED
	}

	// loop!
	cl.Emit3(OP_JUMP, NO_PART, start_pc)

	if have_cond {
		cl.JumpHere(if_pc)
	}

	// ensure the result is always NIL
	cl.Emit1(OP_MAKE_NIL)
	return OK
}

func (cl *Closure) CompileForRange(t *Node) cmError {
	cl.SaveHighWater()

	t_var  := t.children[0]
	t_body := t.children[3]

	// the start/end range and optional step
	t_begin := t.children[1]
	t_end   := t.children[2]

	var t_step *Node
	if t.Len() >= 5 {
		t_step = t.children[4]
	}

	// there are three stack slots (see comment in ParseForRange)
	base := t_var.lvar.offset

	if cl.CompileTerm(t_begin) != OK {
		return FAILED
	}
	cl.Emit3(OP_STORE_A, NO_PART, base+0)

	if cl.CompileTerm(t_end) != OK {
		return FAILED
	}
	cl.Emit3(OP_STORE_A, NO_PART, base+1)

	// if we don't have an explicit step, OP_FOR_BEGIN will compute one
	need_calc := int32(0)

	if t_step == nil {
		need_calc = 1
	} else {
		if cl.CompileTerm(t_step) != OK {
			return FAILED
		}
		cl.Emit3(OP_STORE_A, NO_PART, base+2)
	}

	// validate the start/end/step values
	cl.Emit3(OP_FOR_BEGIN, base, need_calc)

	// if we don't have an explicit step, the body will always run at
	// least once, otherwise need to check for no iterations.
	check_pc := int32(-1)

	if need_calc == 0 {
		cl.Emit3(OP_FOR_PAST, base, NO_PART)

		check_pc = cl.CurrentPC()
		cl.Emit3(OP_IF_TRUE, NO_PART, DUMMY_PC)
	}

	loop_pc := cl.CurrentPC()

	// compile the body
	if cl.CompileBlock(t_body, false) != OK {
		return FAILED
	}

	// perform the step, loop back if not finished
	cl.Emit2(OP_FOR_STEP, base)
	cl.Emit2(OP_FOR_PAST, base)
	cl.Emit3(OP_IF_FALSE, NO_PART, loop_pc)

	if need_calc == 0 {
		cl.JumpHere(check_pc)
	}

	// ensure the result is always NIL
	cl.Emit1(OP_MAKE_NIL)

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileForEach(t *Node) cmError {
	cl.SaveHighWater()

	// the two variables
	t_idx  := t.children[0]
	t_var  := t.children[1] ; _ = t_var

	t_obj  := t.children[2]
	t_body := t.children[3]

	// the object to iterate over
	if cl.CompileTerm(t_obj) != OK {
		return FAILED
	}

	// there are four stack slots (see comment in ParseForEach)
	base := t_idx.lvar.offset

	cl.Emit2(OP_EACH_BEGIN, base)

	start_pc := cl.CurrentPC()

	// set vars, jump to end if no more elements
	cl.Emit3(OP_EACH_STEP, base, DUMMY_PC)

	// compile the body
	if cl.CompileBlock(t_body, false) != OK {
		return FAILED
	}

	// loop the iteration
	cl.Emit3(OP_JUMP, NO_PART, start_pc)

	cl.JumpHere(start_pc)

	// ensure the result is always NIL
	cl.Emit1(OP_MAKE_NIL)

	cl.RestoreHighWater()
	return OK
}

//----------------------------------------------------------------------

func (cl *Closure) CompileLet(t *Node) cmError {
	lvar   := t.children[0].lvar
	t_expr := t.children[1]

	if cl.CompileTerm(t_expr) != OK {
		return FAILED
	}

	// store initial value
	if lvar.external {
		cl.Emit3(OP_EXVAR_WRITE, NO_PART, lvar.offset)
	} else {
		cl.Emit3(OP_STORE_A, NO_PART, lvar.offset)
	}

	cl.Emit1(OP_MAKE_NIL)
	return OK
}

func (cl *Closure) CompileAssign(t *Node) cmError {
	t_var  := t.children[0]
	t_expr := t.children[1]

	if t_var.kind == NX_Access {
		return cl.CompileWriteAccess(t_var, t_expr)
	}

	// compute the value
	if cl.CompileTerm(t_expr) != OK {
		return FAILED
	}

	name := t_var.str

	if t_var.kind == NX_Global {
		g_idx := global_lookup[name]
		cl.Emit3(OP_GLOB_WRITE, NO_PART, int32(g_idx))
		cl.Emit1(OP_MAKE_NIL)
		return OK
	}

	if t_var.kind == NX_EnvVar {
		cl.Emit3(OP_ENV_WRITE, NO_PART, cl.AddString(t_var.str))
		return OK
	}

	lvar := t_var.lvar

	if lvar.owner == cl {
		// var is local to this function
		if lvar.external {
			cl.Emit3(OP_EXVAR_WRITE, NO_PART, lvar.offset)
		} else {
			cl.Emit3(OP_STORE_A, NO_PART, lvar.offset)
		}

	} else {
		// variable belongs to a parent -- an upvar
		upv_idx := cl.CaptureLocal(lvar)
		cl.Emit3(OP_UPV_WRITE, NO_PART, upv_idx)
	}

	// ensure the result is always NIL
	cl.Emit1(OP_MAKE_NIL)
	return OK
}

func (cl *Closure) CompileReadAccess(t *Node) cmError {
	cl.SaveHighWater()

	t_obj   := t.children[0]
	obj_ofs := cl.NewTemporary()

	if cl.CompileTerm(t_obj) != OK {
		return FAILED
	}
	if cl.CompileIndexors(t.children[1:], obj_ofs) != OK {
		return FAILED
	}

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileWriteAccess(t_access, t_expr *Node) cmError {
	cl.SaveHighWater()

	t_obj := t_access.children[0]

	// we need two temporaries
	obj_ofs := cl.NewTemporary()
	key_ofs := cl.NewTemporary()

	if cl.CompileTerm(t_obj) != OK {
		return FAILED
	}

	// when there are multiple indexors, all but the last are reads
	if t_access.Len() > 2 {
		indexors := t_access.children[1:t_access.Len()-1]

		if cl.CompileIndexors(indexors, obj_ofs) != OK {
			return FAILED
		}
	}

	// the final index or key
	t_key := t_access.children[t_access.Len()-1]

	// we optimize when the key and/or value are a constant:
	// -  when both are constant,  can omit save/restore of the object.
	// -  when either is constant, can omit save/restore of the key.

	key_lit := t_key .IsValue()
	val_lit := t_expr.IsValue()

	// save the object
	if !(key_lit && val_lit) {
		cl.Emit3(OP_STORE_A, NO_PART, obj_ofs)
	} else {
		cl.Emit1(OP_A_TO_B)
	}

	// compute the key, save if needed
	if !key_lit {
		if cl.CompileTerm(t_key) != OK {
			return FAILED
		}
		if val_lit {
			cl.Emit1(OP_A_TO_C)
		} else {
			cl.Emit3(OP_STORE_A, NO_PART, key_ofs)
		}
	}

	// compute the value
	if cl.CompileTerm(t_expr) != OK {
		return FAILED
	}

	if key_lit {
		cl.Emit2(OP_LOAD_C, cl.AddConstant(t_key.val))
	} else if val_lit {
		// key already in C
	} else {
		cl.Emit2(OP_LOAD_C, key_ofs)
	}

	if !(key_lit && val_lit) {
		cl.Emit2(OP_LOAD_B, obj_ofs)
	}
	cl.Emit1(OP_ELEM_WRITE)

	// the result of an assignment is always NIL
	cl.Emit1(OP_MAKE_NIL)

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileIndexors(indexors []*Node, obj_ofs int32) cmError {
	// on entry, A register contains the object.
	// on exit,  A register has the value read.

	for i := range indexors {
		t_key := indexors[i]

		if t_key.IsValue() {
			cl.Emit1(OP_A_TO_B)
			cl.Emit2(OP_LOAD_A, cl.AddConstant(t_key.val))

		} else {
			cl.Emit3(OP_STORE_A, NO_PART, obj_ofs)

			if cl.CompileTerm(t_key) != OK {
				return FAILED
			}
			cl.Emit2(OP_LOAD_B, obj_ofs)
		}

		cl.Emit1(OP_ELEM_READ)
	}

	return OK
}

//----------------------------------------------------------------------

func (cl *Closure) CompileTerm(t *Node) cmError {
	return cl.CompileExpr(t, false)
}

func (cl *Closure) CompileExpr(t *Node, tail bool) cmError {
	Error_SetPos(t.pos)
	cl.cur_pos = t.pos

	switch t.kind {
	case NL_Name:
		panic("got a raw name in CompileExpr")

	case NG_Block:
		return cl.CompileBlock(t, tail)

	case NX_Value, NX_Const:
		cl.CompileValue(t.val)
		return OK

	case NX_Global:
		return cl.CompileGlobalVar(t)

	case NX_Local:
		return cl.CompileLocalVar(t.lvar)

	case NX_EnvVar:
		return cl.CompileEnvVar(t)

	case NX_ArgParm:
		return cl.CompileArgParm(t)

	case NX_UserDir:
		return cl.CompileUserDir(t)

	case NX_Unary:
		return cl.CompileUnaryOp(t)

	case NX_Binary:
		switch t.str {
		case "and", "or":
			return cl.CompileShortCircuitingOp(t)
		default:
			return cl.CompileBinaryOp(t)
		}

	case NX_Call:
		return cl.CompileFunCall(t, false, tail)

	case NX_Method:
		return cl.CompileFunCall(t, true, tail)

	case NX_Access:
		return cl.CompileReadAccess(t)

	case NX_Lambda:
		return cl.CompileLambda(t)

	case NX_Supports:
		return cl.CompileSupports(t)

	case NX_Matches:
		return cl.CompileMatches(t)

	case NX_Min:
		return cl.CompileMinMax(t, false)

	case NX_Max:
		return cl.CompileMinMax(t, true)

	case ND_Array:
		return cl.CompileArray(t)

	case ND_Map:
		return cl.CompileMap(t)

	case ND_Set:
		return cl.CompileSet(t)

	case ND_StrBuild:
		return cl.CompileStrBuilder(t)

	case NC_GlobMatch:
		return cl.CompileGlobMatch(t)

	case NC_Ok:
		cl.Emit1(OP_CALC_OK)
		return OK

	case NC_Failed:
		cl.Emit1(OP_CALC_OK)
		cl.Emit1(OP_NOT)
		return OK

	case NC_ArgNum:
		cl.Emit1(OP_ARG_NUM)
		return OK

	default:
		PostError("weird expression, got: %s", t.String())
		return FAILED
	}
}

func (cl *Closure) CompileValue(v *Value) {
	switch v.kind {
	case TYP_Void:
		cl.Emit1(OP_MAKE_NIL)
		return

	case TYP_Bool:
		if v.Num > 0 {
			cl.Emit1(OP_MAKE_TRUE)
		} else {
			cl.Emit1(OP_MAKE_FALSE)
		}
		return

	case TYP_Char:
		cl.Emit2(OP_MAKE_CHAR, int32(v.Num))
		return

	case TYP_Number:
		ival := util_to_int(v.Num)

		if ival > -0x7FFFFFFF && ival < 0x7FFFFFFF {
			fval := float64(ival)

			if fval == v.Num {
				cl.Emit2(OP_MAKE_NUMBER, int32(ival))
				return
			}
		}
	}

	cl.Emit2(OP_LOAD_A, cl.AddConstant(v))
}

func (cl *Closure) CompileLocalVar(lvar *LocalVar) cmError {
	if lvar.owner != cl {
		// variable belongs to a parent function -- an upvar
		upv_idx := cl.CaptureLocal(lvar)
		cl.Emit2(OP_UPV_READ, upv_idx)
		return OK
	}

	if lvar.external {
		cl.Emit2(OP_EXVAR_READ, lvar.offset)
	} else {
		cl.Emit2(OP_LOAD_A, lvar.offset)
	}
	return OK
}

func (cl *Closure) CompileGlobalVar(t *Node) cmError {
	name := t.str

	g_idx, exist := global_lookup[name]
	if !exist {
		panic("unknown global: " + name)
	}

	cl.Emit2(OP_GLOB_READ, int32(g_idx))
	return OK
}

func (cl *Closure) CompileEnvVar(t *Node) cmError {
	cl.Emit2(OP_ENV_READ, cl.AddString(t.str))
	return OK
}

func (cl *Closure) CompileArgParm(t *Node) cmError {
	// this should never fail
	num, _ := strconv.ParseInt(t.str, 10, 32)

	cl.Emit2(OP_ARG_READ, int32(num))
	cl.Emit1(OP_TO_STR)

	return OK
}

func (cl *Closure) CompileUserDir(t *Node) cmError {
	cl.Emit2(OP_USER_DIR, cl.AddString(t.str))
	return OK
}

//----------------------------------------------------------------------

func (cl *Closure) CompileArray(t *Node) cmError {
	cl.SaveHighWater()

	// create a new empty array
	arr_ofs := cl.NewTemporary()

	cl.Emit1(OP_MAKE_ARRAY)
	cl.Emit3(OP_STORE_A, NO_PART, arr_ofs)

	// append each element in turn...
	for _, t_elem := range t.children {
		if cl.CompileTerm(t_elem) != OK {
			return FAILED
		}

		cl.Emit2(OP_LOAD_B, arr_ofs)
		cl.Emit1(OP_APPEND)
	}

	cl.Emit2(OP_LOAD_A, arr_ofs)

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileMap(t *Node) cmError {
	cl.SaveHighWater()

	map_ofs := cl.NewTemporary()
	key_ofs := cl.NewTemporary()

	// create a new empty map
	cl.Emit1(OP_MAKE_MAP)
	cl.Emit3(OP_STORE_A, NO_PART, map_ofs)

	// handle classes
	if t.tdef != nil {
		cl.Emit2(OP_SET_CLASS, int32(t.tdef.index))
	}

	// insert each element in turn...
	for _, pair := range t.children {
		t_key := pair.children[0]
		t_val := pair.children[1]

		// produce more optimal code when key is a constant
		key_lit := t_key.IsValue()

		if !key_lit {
			if cl.CompileTerm(t_key) != OK {
				return FAILED
			}
			cl.Emit3(OP_STORE_A, NO_PART, key_ofs)
		}

		if cl.CompileTerm(t_val) != OK {
			return FAILED
		}

		if key_lit {
			cl.Emit2(OP_LOAD_C, cl.AddConstant(t_key.val))
		} else {
			cl.Emit2(OP_LOAD_C, key_ofs)
		}

		cl.Emit2(OP_LOAD_B, map_ofs)
		cl.Emit1(OP_ELEM_WRITE)
	}

	cl.Emit2(OP_LOAD_A, map_ofs)

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileSet(t *Node) cmError {
	cl.SaveHighWater()

	set_ofs := cl.NewTemporary()

	// create a new empty map
	cl.Emit1(OP_MAKE_MAP)
	cl.Emit3(OP_STORE_A, NO_PART, set_ofs)

	// insert each element in turn...
	for _, t_key := range t.children {
		if cl.CompileTerm(t_key) != OK {
			return FAILED
		}
		cl.Emit1(OP_A_TO_C)
		cl.Emit1(OP_MAKE_TRUE)

		cl.Emit2(OP_LOAD_B, set_ofs)
		cl.Emit1(OP_ELEM_WRITE)
	}

	cl.Emit2(OP_LOAD_A, set_ofs)

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileStrBuilder(t *Node) cmError {
	cl.SaveHighWater()

	// begin with an empty string
	str_ofs := cl.NewTemporary()

	cl.Emit3(OP_COPY, cl.AddString(""), str_ofs)

	// append each element in turn...
	for _, t_elem := range t.children {
		is_string := false

		if t_elem.IsValue() {
			if t_elem.val.kind == TYP_String {
				is_string = true
			}
		}

		if cl.CompileTerm(t_elem) != OK {
			return FAILED
		}
		if !is_string {
			cl.Emit1(OP_TO_STR)
		}

		cl.Emit2(OP_LOAD_B, str_ofs)
		cl.Emit1(OP_ADD)
		cl.Emit3(OP_STORE_A, NO_PART, str_ofs)
	}

	if t.Len() == 0 {
		cl.Emit2(OP_LOAD_A, str_ofs)
	}

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileFmtString(t *Node) cmError {
	PostError("fmt$ is not implemented yet")
	return FAILED
}

//----------------------------------------------------------------------

func (cl *Closure) CompileFunCall(t *Node, is_method, tail bool) cmError {
	cl.SaveHighWater()

	t_func  := t.children[0]
	params  := t.children[1:]
	p_start := 0

	// stack frame looks like:
	//    base + 0 : function to call
	//    base + 1 : first parameter
	//    base + 2 : second parameter
	//    etc...

	cl.high_water += 1
	base := cl.high_water

	// ensure temporaries used to compute params are ABOVE the parameter area
	cl.high_water += int32(len(params))

	if cl.high_water > cl.highest_water {
		cl.highest_water = cl.high_water
	}

	if is_method {
		// compile the receiver first
		t_recv := params[0]
		p_start = 1

		if cl.CompileTerm(t_recv) != OK {
			return FAILED
		}
		cl.Emit3(OP_STORE_A, NO_PART, base+1)

		cl.Emit2(OP_GET_METHOD, cl.AddString(t_func.str))

	} else {
		if cl.CompileTerm(t_func) != OK {
			return FAILED
		}
	}

	cl.Emit3(OP_STORE_A, NO_PART, base+0)

	// compute the parameters...
	for idx := p_start; idx < len(params); idx++ {
		t_par := params[idx]

		if cl.CompileTerm(t_par) != OK {
			return FAILED
		}
		cl.Emit3(OP_STORE_A, NO_PART, base+int32(idx+1))
	}

	cl.cur_pos = t_func.pos

	// the function and its parameters are on the data stack,
	// so now we can call it.

	if tail {
		cl.Emit3(OP_TAIL_CALL, base, int32(len(params)))
	} else {
		cl.Emit3(OP_FUN_CALL, base, int32(len(params)))
	}

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileUnaryOp(t *Node) cmError {
	if cl.CompileTerm(t.children[0]) != OK {
		return FAILED
	}

	op := unary_operators[t.str]

	cl.cur_pos = t.pos
	cl.Emit1(op.code)

	return OK
}

func (cl *Closure) CompileBinaryOp(t *Node) cmError {
	cl.SaveHighWater()

	op := binary_operators[t.str]

	lhs := t.children[0]
	rhs := t.children[1]

	// use more optimal code for certain situations

	if rhs.IsValue() || rhs.kind == NX_Global {
		if cl.CompileTerm(lhs) != OK {
			return FAILED
		}
		cl.Emit1(OP_A_TO_B)

		if cl.CompileTerm(rhs) != OK {
			return FAILED
		}

	} else if lhs.IsValue() {
		if cl.CompileTerm(rhs) != OK {
			return FAILED
		}
		cl.Emit2(OP_LOAD_B, cl.AddConstant(lhs.val))

	} else {
		/* normal logic */

		// compile LHS
		if cl.CompileTerm(lhs) != OK {
			return FAILED
		}

		// save LHS result in a temporary
		temp_ofs := cl.NewTemporary()

		cl.Emit3(OP_STORE_A, NO_PART, temp_ofs)

		// compile RHS
		if cl.CompileTerm(rhs) != OK {
			return FAILED
		}

		// restore LHS result
		cl.Emit2(OP_LOAD_B, temp_ofs)
	}

	cl.cur_pos = t.pos
	cl.Emit1(op.code)

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileShortCircuitingOp(t *Node) cmError {
	// the `and` operator is equivalent to:
	//     if LHS { RHS } else { FALSE }
	//
	// the `or` operator is equivalent to:
	//     if LHS { TRUE } else { RHS }

	lhs := t.children[0]
	rhs := t.children[1]

	if cl.CompileTerm(lhs) != OK {
		return FAILED
	}

	check_pc := cl.CurrentPC()

	if t.str == "and" {
		cl.Emit3(OP_IF_FALSE, NO_PART, DUMMY_PC)
	} else {
		cl.Emit3(OP_IF_TRUE,  NO_PART, DUMMY_PC)
	}

	if cl.CompileTerm(rhs) != OK {
		return FAILED
	}

	cl.JumpHere(check_pc)
	return OK
}

func (cl *Closure) CompileSupports(t *Node) cmError {
	t_obj   := t.children[0]
	methods := t.children[1:]

	if cl.CompileTerm(t_obj) != OK {
		return FAILED
	}
	cl.Emit1(OP_A_TO_B)

	check_pcs := make([]int32, 0)

	// handle each method, the first match terminates early
	for i, t_meth := range methods {
		is_last := (i == len(methods)-1)

		cl.Emit2(OP_HAS_METHOD, cl.AddString(t_meth.str))

		if !is_last {
			pc := cl.CurrentPC()
			check_pcs = append(check_pcs, pc)
			cl.Emit3(OP_IF_FALSE, NO_PART, DUMMY_PC)
		}
	}

	for _, pc := range check_pcs {
		cl.JumpHere(pc)
	}

	return OK
}

func (cl *Closure) CompileMatches(t *Node) cmError {
	t_main   := t.children[0]
	children := t.children[1:]

	// check if all values except first are constants.
	// we can optimize that case by keeping main value in B register.
	all_lit := true

	for _, t_val := range children {
		all_lit = all_lit && t_val.IsValue()
	}

	cl.SaveHighWater()

	main_ofs := cl.NewTemporary()

	// compile the primary value
	if cl.CompileTerm(t_main) != OK {
		return FAILED
	}

	if all_lit {
		cl.Emit1(OP_A_TO_B)
	} else {
		cl.Emit3(OP_STORE_A, NO_PART, main_ofs)
	}

	check_pcs := make([]int32, 0)

	// handle each value, the first match terminates early
	for i, t_val := range children {
		is_last := (i == len(children)-1)

		if cl.CompileTerm(t_val) != OK {
			return FAILED
		}

		if !all_lit {
			cl.Emit2(OP_LOAD_B, main_ofs)
		}

		// compare main value and current one
		cl.Emit1(OP_EQ)

		if !is_last {
			pc := cl.CurrentPC()
			check_pcs = append(check_pcs, pc)
			cl.Emit3(OP_IF_TRUE, NO_PART, DUMMY_PC)
		}
	}

	for _, pc := range check_pcs {
		cl.JumpHere(pc)
	}

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileMinMax(t *Node, is_max bool) cmError {
	t_main   := t.children[0]
	children := t.children[1:]

	// check if all values except first are constants.
	// that allows us to omit save/restore of current value.
	all_lit := true

	for _, t_val := range children {
		all_lit = all_lit && t_val.IsValue()
	}

	cl.SaveHighWater()

	save_ofs := cl.NewTemporary()

	// compile the primary value
	if cl.CompileTerm(t_main) != OK {
		return FAILED
	}

	// handle each value
	for _, t_val := range children {
		if all_lit {
			cl.Emit2(OP_LOAD_B, cl.AddConstant(t_val.val))
		} else {
			cl.Emit3(OP_STORE_A, NO_PART, save_ofs)

			if cl.CompileTerm(t_val) != OK {
				return FAILED
			}
			cl.Emit2(OP_LOAD_B, save_ofs)
		}

		if is_max {
			cl.Emit1(OP_MAX)
		} else {
			cl.Emit1(OP_MIN)
		}
	}

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileLambda(t *Node) cmError {
	template := t.val.Clos

	// this operation creates the "real" closure when run
	cl.Emit2(OP_MAKE_FUNC, cl.AddConstant(t.val))

	// capture variables for the new closure
	for _, cvar := range template.captures {
		if cvar.owner != cl {
			// this variable is "far up", i.e. belongs not to the immediate
			// parent of the template but a function further up, so we need
			// to ensure any intermediate lambdas also capture the local.

			offset := cl.CaptureLocal(cvar)
			cl.Emit2(OP_COPY_UPVAR, offset)

		} else if cvar.external {
			cl.Emit2(OP_NEW_UPVAR, cvar.offset)
		} else {
			cl.Emit2(OP_HOIST_VAL, cvar.offset)
		}
	}

	return OK
}

//----------------------------------------------------------------------

func (cl *Closure) CompileCommand(t *Node, backgnd bool, in_conn, out_conn *PipeConnection) cmError {
	// TODO backgrounding

	cl.SaveHighWater()

	cmd_ofs := cl.NewTemporary()

	cl.Emit2(OP_EXEC_BEGIN, cmd_ofs)

	env_vars  := t.children[0]
	redirects := t.children[1]
	arg_list  := t.children[2:]

	// add command name and each argument to the CommandBuilder.
	// OP_EXEC_ARG will handle anything which is not a string.
	for _, arg := range arg_list {
		if arg.kind == NC_ArgGroup {
			if cl.CompileArgGroup(arg, cmd_ofs) != OK {
				return FAILED
			}
		} else {
			if cl.CompileTerm(arg) != OK {
				return FAILED
			}
			cl.Emit2(OP_EXEC_ARG, cmd_ofs)
		}
	}

	// handle environment_vars
	for _, pair := range env_vars.children {
		t_var  := pair.children[0]
		t_expr := pair.children[1]

		if cl.CompileTerm(t_expr) != OK {
			return FAILED
		}
		cl.Emit3(OP_EXEC_ENV, cmd_ofs, cl.AddString(t_var.str))
	}

	// handle redirects
	redir_files := make([]int32, 4)

	for _, redir := range redirects.children {
		if cl.CompileRedirect(redir, cmd_ofs, redir_files) != OK {
			return FAILED
		}
	}

	// handle pipes
	if in_conn != nil {
		cl.Emit2(OP_MAKE_NUMBER, 0)
		cl.Emit3(OP_EXEC_REDIR, cmd_ofs, in_conn.offset)
	}
	if out_conn != nil {
		if out_conn.stdout {
			cl.Emit2(OP_MAKE_NUMBER, 1)
			cl.Emit3(OP_EXEC_REDIR, cmd_ofs, out_conn.offset)
		}
		if out_conn.stderr {
			cl.Emit2(OP_MAKE_NUMBER, 2)
			cl.Emit3(OP_EXEC_REDIR, cmd_ofs, out_conn.offset)
		}
	}

	cl.Emit2(OP_EXEC_RUN, cmd_ofs)

	// close all the files opened for redirects
	for k := 0; k < 4; k++ {
		file_ofs := redir_files[k]
		if file_ofs != 0 {
			cl.Emit2(OP_FILE_CLOSE, file_ofs)
		}
	}

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileRedirect(t *Node, cmd_ofs int32, redir_files []int32) cmError {
	symbol := t.str
	target := t.children[0]

	to_var    := (symbol[len(symbol)-1] == '=')
	from_expr := (symbol == "<<")

	if to_var {
		PostError("to-var redirects NYI")
		return FAILED
	}

	file_ofs := cl.NewTemporary()

	// add offset to redir_files  [ UGH, HACKY ]
	for k := 0; k < 4; k++ {
		if redir_files[k] == 0 {
			redir_files[k] = file_ofs
			break
		}
	}

	if symbol[0] == '<' {
		if from_expr {
			PostError("<< redirect is NYI")
			return FAILED
		}

		// grab the filename
		if cl.CompileTerm(target) != OK {
			return FAILED
		}

		// open the file for reading
		cl.Emit3(OP_FILE_OPEN, file_ofs, 0)

		cl.Emit2(OP_MAKE_NUMBER, 0)
		cl.Emit3(OP_EXEC_REDIR, cmd_ofs, file_ofs)
		return OK
	}

	to_stdout := (symbol[0] == '>' || symbol[0] == '&')
	to_stderr := (symbol[0] == '!' || symbol[0] == '&')

	if symbol[0] != '>' {
		symbol = symbol[1:]
	}

	is_append := false
	if len(symbol) >= 2 {
		is_append = (symbol[0] == '>' && symbol[1] == '>')
	}

	// grab the filename
	if cl.CompileTerm(target) != OK {
		return FAILED
	}

	// open the file for writing
	if is_append {
		cl.Emit3(OP_FILE_OPEN, file_ofs, 2)
	} else {
		cl.Emit3(OP_FILE_OPEN, file_ofs, 1)
	}

	if to_stdout {
		cl.Emit2(OP_MAKE_NUMBER, 1)
		cl.Emit3(OP_EXEC_REDIR, cmd_ofs, file_ofs)
	}
	if to_stderr {
		cl.Emit2(OP_MAKE_NUMBER, 2)
		cl.Emit3(OP_EXEC_REDIR, cmd_ofs, file_ofs)
	}

	return OK
}

func (cl *Closure) CompilePipe(t *Node, out_conn *PipeConnection) cmError {
	cl.SaveHighWater()

	lhs  := t.children[0]
	rhs  := t.children[1]
	pipe := t.str

	// create the file used for communication
	conn_ofs := cl.NewTemporary()

	cl.Emit2(OP_FILE_TEMP, conn_ofs)

	conn := new(PipeConnection)
	conn.offset = conn_ofs
	conn.stdout = (pipe == "|"  || pipe == "||")
	conn.stderr = (pipe == "|!" || pipe == "||")

	if lhs.kind == NC_Pipe {
		if cl.CompilePipe(lhs, conn) != OK {
			return FAILED
		}
	} else {
		if cl.CompileCommand(lhs, false, nil, conn) != OK {
			return FAILED
		}
	}

	// close communication file for writing, re-open for reading
	cl.Emit2(OP_FILE_REOPEN, conn_ofs)

	if cl.CompileCommand(rhs, false, conn, out_conn) != OK {
		return FAILED
	}

	cl.Emit2(OP_FILE_CLOSE, conn_ofs)

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileArgGroup(t *Node, cmd_ofs int32) cmError {
	cl.SaveHighWater()

	count_ofs := cl.NewTemporary()

	cl.Emit2(OP_EXEC_COUNT, cmd_ofs)
	cl.Emit3(OP_STORE_A, NO_PART, count_ofs)

	for _, arg := range t.children {
		if cl.CompileTerm(arg) != OK {
			return FAILED
		}
		cl.Emit2(OP_EXEC_ARG, cmd_ofs)
	}

	cl.Emit2(OP_EXEC_COUNT, cmd_ofs)
	cl.Emit2(OP_LOAD_B, count_ofs)
	cl.Emit1(OP_EQ)

	check_pc := cl.CurrentPC()
	cl.Emit3(OP_IF_FALSE, NO_PART, DUMMY_PC)

	// WISH: show the glob(s)
	msg := "no matching files"
	if (t.flags & EF_HAS_GLOB) == 0 {
		msg = "expression(s) produced no arguments"
	}
	cl.Emit2(OP_LOAD_A, cl.AddString(msg))
	cl.Emit1(OP_ERROR)

	cl.JumpHere(check_pc)

	cl.RestoreHighWater()
	return OK
}

func (cl *Closure) CompileGlobMatch(t *Node) cmError {
	// each element is either a NW_Glob or something which will
	// evaluate to a string (which the parser ensures).

	cl.SaveHighWater()

	glob_ofs := cl.NewTemporary()

	cl.Emit3(OP_PAT_BEGIN, glob_ofs, 0 /* case sensitive */)

	for _, child := range t.children {
		if child.kind == NW_Glob {
			cl.Emit3(OP_PAT_GLOB, glob_ofs, cl.AddString(child.str))
		} else {
			if cl.CompileTerm(child) != OK {
				return FAILED
			}
			cl.Emit2(OP_PAT_TEXT, glob_ofs)
		}
	}

	cl.Emit1(OP_MAKE_ARRAY)
	cl.Emit2(OP_PAT_RUN, glob_ofs)

	cl.RestoreHighWater()
	return OK
}

//----------------------------------------------------------------------

func (cl *Closure) AllocateLocals(scope *Scope, parameters bool) {
	for _, lvar := range scope.vars {
		cl.high_water += 1
		lvar.offset = cl.high_water

		if lvar.external {
			if parameters {
				cl.Emit3(OP_LOAD_A, lvar.offset, NO_PART)
			}

			cl.Emit3(OP_NEW_EXVAR, NO_PART, lvar.offset)

			if parameters {
				cl.Emit3(OP_EXVAR_WRITE, NO_PART, lvar.offset)
			}
		}
	}

	if cl.high_water > cl.highest_water {
		cl.highest_water = cl.high_water
	}
}

func (cl *Closure) NewTemporary() int32 {
	cl.high_water += 1
	if cl.high_water > cl.highest_water {
		cl.highest_water = cl.high_water
	}
	return cl.high_water
}

func (cl *Closure) SaveHighWater() {
	cl.saved_hw = append(cl.saved_hw, cl.high_water)
}

func (cl *Closure) RestoreHighWater() {
	total := len(cl.saved_hw)
	if total == 0 {
		panic("RestoreHighWater with empty stack")
	}

	cl.high_water = cl.saved_hw[  total-1]
	cl.saved_hw   = cl.saved_hw[0:total-1]
}

func (cl *Closure) PushBlockScope(scope *Scope) {
	scope.skip_pcs = make([]int32, 0)

	cl.block_scopes = append(cl.block_scopes, scope)
}

func (cl *Closure) PopBlockScope() {
	scope :=          cl.block_scopes[  len(cl.block_scopes)-1]
	cl.block_scopes = cl.block_scopes[0:len(cl.block_scopes)-1]

	// fix jumps of skip statements
	for _, pc := range scope.skip_pcs {
		cl.JumpHere(pc)
	}
}

func (cl *Closure) FindScopeForLabel(label string) *Scope {
	for i := len(cl.block_scopes)-1; i >= 0; i-- {
		scope := cl.block_scopes[i]
		if scope.end_label == label {
			return scope
		}
	}
	return nil  // not found
}

func (cl *Closure) AddString(s string) int32 {
	var v Value
	v.MakeString(s)
	return cl.AddConstant(&v)
}

func (cl *Closure) AddConstant(v *Value) int32 {
	if cl.constants == nil {
		cl.constants = make([]Value, 0)
	}

	// see if we can re-use an existing constant
	for idx, other := range cl.constants {
		if v.BasicEqual(&other) {
			return int32(-1 - idx)
		}
	}

	// nope, so add a new one
	res := int32(-1 - len(cl.constants))
	cl.constants = append(cl.constants, *v)
	return res
}

func (cl *Closure) CaptureLocal(lvar *LocalVar) int32 {
	// returns an index into `upvars` (of the running closure).

	for idx, cvar := range cl.captures {
		if cvar == lvar {
			return int32(idx)
		}
	}

	idx := len(cl.captures)
	cl.captures = append(cl.captures, lvar)

	return int32(idx)
}

func (cl *Closure) Emit1(T OpCode) {
	cl.Emit3(T, NO_PART, NO_PART)
}

func (cl *Closure) Emit2(T OpCode, S int32) {
	cl.Emit3(T, S, NO_PART)
}

func (cl *Closure) Emit3(T OpCode, S, D int32) {
	var oper Operation

	oper.kind = T
	oper.line = int32(cl.cur_pos.line)
	oper.S    = S
	oper.D    = D

	cl.vm_ops = append(cl.vm_ops, oper)
}

func (cl *Closure) CurrentPC() int32 {
	return int32(len(cl.vm_ops))
}

func (cl *Closure) JumpHere(jump_pc int32) {
	cl.vm_ops[jump_pc].D = cl.CurrentPC()
}
